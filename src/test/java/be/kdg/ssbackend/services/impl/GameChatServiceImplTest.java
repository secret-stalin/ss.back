package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.*;
import be.kdg.ssbackend.exceptions.GameNotFoundException;
import be.kdg.ssbackend.exceptions.ObjectNotFoundException;
import be.kdg.ssbackend.model.chat.Message;
import be.kdg.ssbackend.model.game.Game;
import be.kdg.ssbackend.services.api.GameChatService;
import be.kdg.ssbackend.services.api.GameService;
import be.kdg.ssbackend.services.api.LobbyService;
import be.kdg.ssbackend.services.api.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.*;

@ActiveProfiles("test")
@SpringBootTest
@RunWith(SpringRunner.class)
public class GameChatServiceImplTest {

    @Autowired
    private UserService userService;
    @Autowired
    private GameService gameService;
    @Autowired
    private LobbyService lobbyService;
    @Autowired
    private GameChatService gameChatService;

    private int gameId;

    private String validEmail = "tset@tsetmai1l.com";
    private String InValidEmail = "tsedsfsdfdsfdsfst@tsetmai1l.com";

    @Before
    public void setUp() throws Exception {
        UserDTO userDTO = new UserDTO(this.validEmail, "testUser100", "jwtpass");
        userService.createUserAccount(userDTO, false);
        Game game = gameService.createGame();
        LobbyDTO lobbyDTO = lobbyService.createLobby(game.getGameId(), new GameSettingsDTO(game.getGameId(), game.getName(), 1, false, 10, 5, new ArrayList<>()), userDTO.getEmail());
        gameId = lobbyService.startLobby(lobbyDTO.getLobbyId(), userDTO.getEmail());
    }

    @Test
    @Transactional
    public void getGameChatsValidUser() {
        List<GameChatDTO> gameChatDTOS = gameChatService.getChatsFromGame(validEmail, gameId);
        assertTrue(gameChatDTOS.size() == 1 || gameChatDTOS.size() == 2);
        if (gameChatDTOS.size() > 1) {
            assertNotNull(gameChatDTOS.stream().filter(g -> g.name.toLowerCase().equals("stalin")));
            assertNotNull(gameChatDTOS.stream().filter(g -> g.name.toLowerCase().equals("general")));
        }
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void getGameChatsInValidUser() {
        gameChatService.getChatsFromGame(this.InValidEmail, gameId);
    }

    @Test(expected = GameNotFoundException.class)
    @Transactional
    public void getGameChatsInValidGame() {
        gameChatService.getChatsFromGame(this.validEmail, 46545465);
    }

    @Test
    @Transactional
    public void sendMessageValid() {
        String testMess = "test message";
        List<GameChatDTO> gameChatDTOS = gameChatService.getChatsFromGame(this.validEmail, gameId);
        GameChatDTO gameChat = gameChatDTOS.get(0);
        int previousSize = gameChat.messages.size();
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setText(testMess);
        gameChatService.sendMessage(gameChat.getId(), messageDTO, this.validEmail);
        List<Message> messages = new ArrayList<>(gameChatService.getMessagesFromChat(gameChat.getId()));
        messages.sort(Comparator.comparing(Message::getTime));
        assertEquals(previousSize + 1, messages.size());
        assertEquals(testMess, messages.get(messages.size() - 1).getText());

    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void sendMessageInValid() {
        String testMess = "test message";
        List<GameChatDTO> gameChatDTOS = gameChatService.getChatsFromGame(this.InValidEmail, gameId);
        GameChatDTO gameChat = gameChatDTOS.get(0);
        int previousSize = gameChat.messages.size();
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setText(testMess);
        gameChatService.sendMessage(gameChat.id, messageDTO, this.validEmail);

    }
}
