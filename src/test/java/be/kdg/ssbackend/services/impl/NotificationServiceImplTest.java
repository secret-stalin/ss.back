package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.model.user.Notification;
import be.kdg.ssbackend.model.user.NotificationType;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.NotificationRepository;
import be.kdg.ssbackend.services.api.NotificationService;
import be.kdg.ssbackend.services.api.NotificationsSettingsService;
import be.kdg.ssbackend.services.api.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@SpringBootTest
@RunWith(SpringRunner.class)
@Transactional
public class NotificationServiceImplTest {

    @Mock
    NotificationRepository notificationRepository;
    @Mock
    UserService userService;
    @Mock
    PushNotificationService pushNotificationService;
    @Autowired
    SimpMessagingTemplate template;
    @Mock
    NotificationsSettingsService settingsService;


    private NotificationService notificationService;

    @Before
    public void setUp() throws Exception {
        User user = new User("testinguser@m.com", "pass");
        user.setUserId(1);

        Notification notification1 = new Notification(1, "test", false, NotificationType.FRIEND_REQUEST, user);
        notification1.setNotificationId(1);
        Notification notification2 = new Notification(1, "test", false, NotificationType.ENDPHASE, user);
        notification2.setNotificationId(2);
        Notification notification3 = new Notification(1, "test", false, NotificationType.INVITE, user);
        notification3.setNotificationId(3);
        Notification notification4 = new Notification(1, "test", false, NotificationType.YOUR_TURN, user);
        notification4.setNotificationId(4);
        List<Notification> notifications = new ArrayList<>();
        notifications.add(notification1);
        notifications.add(notification2);
        notifications.add(notification3);
        notifications.add(notification4);
        Map<Integer, Notification> notificationMap = new HashMap<>();
        for (Notification notification : notifications) {
            notificationMap.put(notification.getNotificationId(), notification);
        }
        User user2 = new User("invalidtestinguser@m.com", "pass");
        user2.setUserId(2);
        user2.setNotifications(new HashMap<>());

        user.setNotifications(notificationMap);
        when(userService.findByEmail("testinguser@m.com")).thenReturn(user);
        when(userService.findByEmail("invalidtestinguser@m.com")).thenReturn(user2);
        when(notificationRepository.findByNotificationId(1)).thenReturn(notification1);
        when(notificationRepository.findNotificationsByUser_UserId(1)).thenReturn(notifications);
        this.notificationService = new NotificationServiceImpl(notificationRepository, userService, template, pushNotificationService, settingsService);
    }

    @Test(expected = NullPointerException.class)
    public void readNotificationNoAccess() {
        notificationService.readNotification("invalidtestinguser@m.com", 1);
    }

    @Test
    public void readNotification() {
        assertEquals(notificationService.getUnreadNotificationCount("testinguser@m.com"), 4);
        notificationService.readNotification("testinguser@m.com", 1);
        assertEquals(notificationService.getUnreadNotificationCount("testinguser@m.com"), 3);
    }
}
