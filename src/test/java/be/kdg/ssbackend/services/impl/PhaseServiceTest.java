package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.VoteResultDTO;
import be.kdg.ssbackend.exceptions.ParticipationNotFoundException;
import be.kdg.ssbackend.model.game.*;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.ActionRepository;
import be.kdg.ssbackend.repositories.api.CardRepository;
import be.kdg.ssbackend.repositories.api.ParticipationRepository;
import be.kdg.ssbackend.repositories.api.PhaseRepository;
import be.kdg.ssbackend.services.api.GameService;
import be.kdg.ssbackend.services.api.NotificationService;
import be.kdg.ssbackend.services.api.PhaseService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class PhaseServiceTest {

    @Mock
    PhaseRepository phaseRepository;

    @Mock
    ParticipationRepository participationRepository;

    @Mock
    ActionRepository actionRepository;

    @Mock
    GameService gameService;

    @Mock
    NotificationService notificationService;

    @Mock
    CardRepository cardRepository;

    @MockBean
    SimpMessagingTemplate simpMessagingTemplate;

    PhaseService phaseService;

    @Test
    @Transactional
    public void StartFirstPhaseOfAGameWithoutExtraRolesTest() {
        Game stubGame = new Game();
        stubGame.setGameId(1);
        stubGame.setDayDuration(Duration.ofSeconds(60));
        stubGame.setNightDuration(Duration.ofSeconds(60));
        when(gameService.getGame(1)).thenReturn(stubGame);

        when(gameService.saveGame(stubGame)).thenAnswer(i -> i.getArgument(0));

        phaseService = new PhaseServiceImpl(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService);
        phaseService.addPhase(1);
        Game game = gameService.getGame(1);
        Phase result = game.getPhases().get(0);

        Assert.assertEquals(RoleType.SLAV, result.getRoleTypeVoter());
        Assert.assertEquals(0, result.getPhaseNumber());
        Assert.assertEquals(State.DAY, result.getState());
    }

    @Test
    @Transactional
    public void StartSecondPhaseOfAGameWithoutExtraRolesTest() {
        Game stubGame = new Game();
        stubGame.setGameId(1);
        stubGame.setDayDuration(Duration.ofSeconds(60));
        stubGame.setNightDuration(Duration.ofSeconds(60));

        Phase stubPhase = new Phase();
        stubPhase.setState(State.DAY);
        stubPhase.setRoleTypeVoter(RoleType.SLAV);
        stubPhase.setPhaseNumber(0);

        stubGame.getPhases().add(stubPhase);

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);
        Card card2 = new Card(RoleType.STALIN, "stalin", "", true);

        Role role1 = new Role();
        role1.setCard(card1);
        Role role2 = new Role();
        role2.setCard(card2);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);
        roles1.add(role2);

        Set<Role> roles2 = new HashSet<>();
        roles2.add(role1);

        Participation participation1 = new Participation();
        participation1.setRoles(roles1);

        Participation participation2 = new Participation();
        participation2.setRoles(roles2);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);
        participations.add(participation2);

        stubGame.setParticipations(participations);

        when(gameService.getGame(1)).thenReturn(stubGame);
        when(phaseRepository.findTopByGameOrderByPhaseNumberDesc(stubGame)).thenReturn(stubPhase);
        when(gameService.saveGame(stubGame)).thenAnswer(i -> i.getArgument(0));

        phaseService = new PhaseServiceImpl(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService);
        phaseService.addPhase(1);
        Game game = gameService.getGame(1);
        Phase result = game.getPhases().get(1);

        Assert.assertEquals(RoleType.STALIN, result.getRoleTypeVoter());
        Assert.assertEquals(1, result.getPhaseNumber());
        Assert.assertEquals(State.NIGHT, result.getState());
    }

    @Test
    @Transactional
    public void VoteKillRegisterTest() {
        Game stubGame = new Game();
        stubGame.setGameId(1);

        Phase stubPhase = new Phase();
        stubPhase.setRoleTypeVoter(RoleType.SLAV);
        stubPhase.setPhaseNumber(0);

        stubGame.getPhases().add(stubPhase);

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);
        Card card2 = new Card(RoleType.STALIN, "stalin", "", true);

        Role role1 = new Role();
        role1.setCard(card1);
        Role role2 = new Role();
        role2.setCard(card2);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);
        roles1.add(role2);

        Set<Role> roles2 = new HashSet<>();
        roles2.add(role1);

        Participation participation1 = new Participation();
        participation1.setParticipationId(1);
        participation1.setRoles(roles1);
        participation1.setGame(stubGame);

        User user1 = new User();
        user1.getParticipations().add(participation1);

        Participation participation2 = new Participation();
        participation2.setParticipationId(2);
        participation2.setRoles(roles2);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);
        participations.add(participation2);

        stubGame.setParticipations(participations);

        when(gameService.getGame(1)).thenReturn(stubGame);
        when(phaseRepository.findTopByGameOrderByPhaseNumberDesc(stubGame)).thenReturn(stubPhase);
        when(participationRepository.findByParticipationId(participation2.getParticipationId())).thenReturn(participation2);
        when(actionRepository.save(any(Action.class))).thenAnswer(i -> i.getArgument(0));

        phaseService = new PhaseServiceImpl(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService);
        phaseService.addAction(1, user1, 2, RoleType.SLAV);
        ArgumentCaptor<Action> actionArgumentCaptor = ArgumentCaptor.forClass(Action.class);
        verify(actionRepository).save(actionArgumentCaptor.capture());
        Action result = actionArgumentCaptor.getValue();

        Assert.assertEquals(participation1, result.getExecutor());

        Assert.assertEquals(ActionType.VOTE, result.getActionType());
        Assert.assertEquals(participation2, result.getAffected());
    }

    @Test(expected = ParticipationNotFoundException.class)
    @Transactional
    public void VoteKillRegisterWithNonExistingAffectedParticipationTest() {
        Game stubGame = new Game();
        stubGame.setGameId(1);

        Phase stubPhase = new Phase();
        stubPhase.setRoleTypeVoter(RoleType.SLAV);
        stubPhase.setPhaseNumber(0);

        stubGame.getPhases().add(stubPhase);

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);
        Card card2 = new Card(RoleType.STALIN, "stalin", "", true);

        Role role1 = new Role();
        role1.setCard(card1);
        Role role2 = new Role();
        role2.setCard(card2);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);
        roles1.add(role2);

        Participation participation1 = new Participation();
        participation1.setParticipationId(1);
        participation1.setRoles(roles1);
        participation1.setGame(stubGame);

        User user1 = new User();
        user1.getParticipations().add(participation1);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);

        stubGame.setParticipations(participations);

        when(gameService.getGame(1)).thenReturn(stubGame);
        when(phaseRepository.findTopByGameOrderByPhaseNumberDesc(stubGame)).thenReturn(stubPhase);
        when(actionRepository.save(any(Action.class))).thenAnswer(i -> i.getArgument(0));

        phaseService = new PhaseServiceImpl(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService);
        phaseService.addAction(1, user1, 2, RoleType.SLAV);
    }

    @Test
    @Transactional
    public void EndPhaseWithAKillAndNoWinnersTest() {
        Game stubGame = new Game();
        stubGame.setGameId(1);
        stubGame.setDayDuration(Duration.ofSeconds(60));
        stubGame.setNightDuration(Duration.ofSeconds(60));

        Phase stubPhase = new Phase();
        stubPhase.setState(State.DAY);
        stubPhase.setRoleTypeVoter(RoleType.SLAV);
        stubPhase.setPhaseNumber(0);

        stubGame.getPhases().add(stubPhase);

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);
        Card card2 = new Card(RoleType.STALIN, "stalin", "", true);

        Role role1 = new Role();
        role1.setCard(card1);
        Role role2 = new Role();
        role2.setCard(card2);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);
        roles1.add(role2);

        Set<Role> roles2 = new HashSet<>();
        roles2.add(role1);


        Participation participation1 = new Participation();
        participation1.setParticipationId(1);
        participation1.setRoles(roles1);

        User user1 = new User();
        user1.setUsername("user1");
        user1.getParticipations().add(participation1);
        participation1.setUser(user1);

        Participation participation2 = new Participation();
        participation2.setParticipationId(2);
        participation2.setRoles(roles2);

        User user2 = new User();
        user2.setUsername("user2");
        user2.getParticipations().add(participation2);
        participation2.setUser(user2);

        Participation participation3 = new Participation();
        participation3.setParticipationId(3);
        participation3.setRoles(roles2);

        User user3 = new User();
        user3.setUsername("user3");
        participation3.setUser(user3);


        Action action1 = new Action();
        action1.setAffected(participation3);
        action1.setPhase(stubPhase);
        action1.setActionType(ActionType.VOTE);
        action1.setTime(LocalDateTime.now());

        Action action2 = new Action();
        action2.setAffected(participation2);
        action2.setPhase(stubPhase);
        action2.setActionType(ActionType.VOTE);
        action2.setTime(LocalDateTime.now().plusSeconds(60));

        participation1.getActionsDone().add(action1);
        participation1.getActionsDone().add(action2);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);
        participations.add(participation2);
        participations.add(participation3);

        stubGame.setParticipations(participations);

        when(gameService.getGame(1)).thenReturn(stubGame);
        when(phaseRepository.findTopByGameOrderByPhaseNumberDesc(stubGame)).thenReturn(stubPhase);

        phaseService = new PhaseServiceImpl(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService);
        phaseService.endPhase(1);
        ArgumentCaptor<Game> gameArgumentCaptor = ArgumentCaptor.forClass(Game.class);
        verify(gameService, times(2)).saveGame(gameArgumentCaptor.capture());
        Game game = gameArgumentCaptor.getAllValues().get(0);
        Participation part = game.getParticipations().stream().filter(p -> p.getParticipationId() == participation2.getParticipationId()).findFirst().get();

        Assert.assertFalse(part.isAlive());
        Assert.assertTrue(game.isActive());
    }

    @Test
    @Transactional
    public void EndPhaseWithAKillAndWinnersTest() {
        Game stubGame = new Game();
        stubGame.setGameId(1);
        stubGame.setDayDuration(Duration.ofSeconds(60));
        stubGame.setNightDuration(Duration.ofSeconds(60));

        Phase stubPhase = new Phase();
        stubPhase.setState(State.DAY);
        stubPhase.setRoleTypeVoter(RoleType.SLAV);
        stubPhase.setPhaseNumber(0);

        stubGame.getPhases().add(stubPhase);

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);
        Card card2 = new Card(RoleType.STALIN, "stalin", "", true);

        Role role1 = new Role();
        role1.setCard(card1);
        Role role2 = new Role();
        role2.setCard(card2);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);
        roles1.add(role2);

        Set<Role> roles2 = new HashSet<>();
        roles2.add(role1);

        Participation participation1 = new Participation();
        participation1.setParticipationId(1);
        participation1.setRoles(roles1);

        User user1 = new User();
        user1.setUsername("user1");
        user1.getParticipations().add(participation1);
        participation1.setUser(user1);

        Participation participation2 = new Participation();
        participation2.setParticipationId(2);
        participation2.setRoles(roles2);

        User user2 = new User();
        user2.setUsername("user2");
        user2.getParticipations().add(participation2);
        participation2.setUser(user2);

        Action action1 = new Action();
        action1.setAffected(participation2);
        action1.setPhase(stubPhase);
        action1.setActionType(ActionType.VOTE);
        action1.setTime(LocalDateTime.now());

        participation1.getActionsDone().add(action1);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);
        participations.add(participation2);

        stubGame.setParticipations(participations);

        when(gameService.getGame(1)).thenReturn(stubGame);
        when(phaseRepository.findTopByGameOrderByPhaseNumberDesc(stubGame)).thenReturn(stubPhase);

        phaseService = new PhaseServiceImpl(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService);
        phaseService.endPhase(1);
        ArgumentCaptor<Game> gameArgumentCaptor = ArgumentCaptor.forClass(Game.class);
        verify(gameService).saveGame(gameArgumentCaptor.capture());
        Game game = gameArgumentCaptor.getValue();
        Participation part = game.getParticipations().stream().filter(p -> p.getParticipationId() == participation2.getParticipationId()).findFirst().get();

        Assert.assertFalse(part.isAlive());
        Assert.assertFalse(game.isActive());
    }

    @Test
    @Transactional
    public void EndPhaseWithTwoOrMoreVoteCountWinnersTest() {
        Game stubGame = new Game();
        stubGame.setGameId(1);
        stubGame.setDayDuration(Duration.ofSeconds(60));
        stubGame.setNightDuration(Duration.ofSeconds(60));

        Phase stubPhase = new Phase();
        stubPhase.setState(State.DAY);
        stubPhase.setRoleTypeVoter(RoleType.SLAV);
        stubPhase.setPhaseNumber(0);

        stubGame.getPhases().add(stubPhase);

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);
        Card card2 = new Card(RoleType.STALIN, "stalin", "", true);

        Role role1 = new Role();
        role1.setCard(card1);
        Role role2 = new Role();
        role2.setCard(card2);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);
        roles1.add(role2);

        Set<Role> roles2 = new HashSet<>();
        roles2.add(role1);

        Participation participation1 = new Participation();
        participation1.setParticipationId(1);
        participation1.setRoles(roles1);

        User user1 = new User();
        user1.setUsername("user1");
        user1.getParticipations().add(participation1);
        participation1.setUser(user1);

        Participation participation2 = new Participation();
        participation2.setParticipationId(2);
        participation2.setRoles(roles2);

        User user2 = new User();
        user2.setUsername("user2");
        user2.getParticipations().add(participation2);
        participation2.setUser(user2);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);
        participations.add(participation2);

        stubGame.setParticipations(participations);

        when(gameService.getGame(1)).thenReturn(stubGame);
        when(phaseRepository.findTopByGameOrderByPhaseNumberDesc(stubGame)).thenReturn(stubPhase);

        phaseService = new PhaseServiceImpl(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService);
        phaseService.endPhase(1);
        ArgumentCaptor<Game> gameArgumentCaptor = ArgumentCaptor.forClass(Game.class);
        verify(gameService, times(2)).saveGame(gameArgumentCaptor.capture());
        Game game = gameArgumentCaptor.getAllValues().get(0);

        for (Participation part : game.getParticipations()) {
            Assert.assertTrue(part.isAlive());
        }
        Assert.assertTrue(game.isActive());
    }

    @Test
    @Transactional
    public void VoteResultsTest() {
        Game stubGame = new Game();
        stubGame.setGameId(1);

        Phase stubPhase = new Phase();
        stubPhase.setRoleTypeVoter(RoleType.SLAV);
        stubPhase.setPhaseNumber(0);

        stubGame.getPhases().add(stubPhase);

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);
        Card card2 = new Card(RoleType.STALIN, "stalin", "", true);

        Role role1 = new Role();
        role1.setCard(card1);
        Role role2 = new Role();
        role2.setCard(card2);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);
        roles1.add(role2);

        Set<Role> roles2 = new HashSet<>();
        roles2.add(role1);

        Participation participation1 = new Participation();
        participation1.setParticipationId(1);
        participation1.setRoles(roles1);
        participation1.setGame(stubGame);

        User user1 = new User();
        user1.setUsername("user1");
        user1.getParticipations().add(participation1);
        participation1.setUser(user1);

        Participation participation2 = new Participation();
        participation2.setParticipationId(2);
        participation2.setRoles(roles2);
        participation2.setGame(stubGame);

        User user2 = new User();
        user2.setUsername("user2");
        user2.getParticipations().add(participation2);
        participation2.setUser(user2);

        Action action1 = new Action();
        action1.setAffected(participation2);
        action1.setPhase(stubPhase);
        action1.setActionType(ActionType.VOTE);
        action1.setTime(LocalDateTime.now());

        participation1.getActionsDone().add(action1);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);
        participations.add(participation2);

        stubGame.setParticipations(participations);

        when(gameService.getGame(1)).thenReturn(stubGame);
        when(phaseRepository.findTopByGameOrderByPhaseNumberDesc(stubGame)).thenReturn(stubPhase);

        phaseService = new PhaseServiceImpl(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService);
        List<VoteResultDTO> voteResultDTOs = phaseService.getVoteResults(user2, 1);

        Assert.assertEquals("user1", voteResultDTOs.get(0).getUsername());
        Assert.assertEquals(0, voteResultDTOs.get(0).getVoteCount());
        Assert.assertEquals("user2", voteResultDTOs.get(1).getUsername());
        Assert.assertEquals(1, voteResultDTOs.get(1).getVoteCount());
    }

    @Test
    @Transactional
    public void userCanVoteTest() {
        Game stubGame = new Game();
        stubGame.setGameId(1);

        Phase stubPhase = new Phase();
        stubPhase.setRoleTypeVoter(RoleType.STALIN);
        stubPhase.setPhaseNumber(0);

        stubGame.getPhases().add(stubPhase);

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);
        Card card2 = new Card(RoleType.STALIN, "stalin", "", true);

        Role role1 = new Role();
        role1.setCard(card1);
        Role role2 = new Role();
        role2.setCard(card2);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);
        roles1.add(role2);

        Participation participation1 = new Participation();
        participation1.setParticipationId(1);
        participation1.setRoles(roles1);
        participation1.setGame(stubGame);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);

        stubGame.setParticipations(participations);

        when(gameService.getGame(1)).thenReturn(stubGame);
        when(phaseRepository.findTopByGameOrderByPhaseNumberDesc(stubGame)).thenReturn(stubPhase);

        boolean result = false;
        try {
            Method method = PhaseServiceImpl.class.getDeclaredMethod("canUserVote", Game.class, Participation.class);
            method.setAccessible(true);
            Object value = method.invoke(PhaseServiceImpl.class.getConstructors()[0]
                    .newInstance(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService), stubGame, participation1);
            result = (Boolean) value;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }

        Assert.assertTrue(result);
    }

    @Test
    @Transactional
    public void userCannotVoteTest() {
        Game stubGame = new Game();
        stubGame.setGameId(1);

        Phase stubPhase = new Phase();
        stubPhase.setRoleTypeVoter(RoleType.STALIN);
        stubPhase.setPhaseNumber(0);

        stubGame.getPhases().add(stubPhase);

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);

        Role role1 = new Role();
        role1.setCard(card1);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);

        Participation participation1 = new Participation();
        participation1.setParticipationId(1);
        participation1.setRoles(roles1);
        participation1.setGame(stubGame);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);

        stubGame.setParticipations(participations);

        when(gameService.getGame(1)).thenReturn(stubGame);
        when(phaseRepository.findTopByGameOrderByPhaseNumberDesc(stubGame)).thenReturn(stubPhase);

        boolean result = true;
        try {
            Method method = PhaseServiceImpl.class.getDeclaredMethod("canUserVote", Game.class, Participation.class);
            method.setAccessible(true);
            Object value = method.invoke(PhaseServiceImpl.class.getConstructors()[0]
                    .newInstance(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService), stubGame, participation1);
            result = (Boolean) value;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }

        Assert.assertFalse(result);
    }

    @Test(expected = ParticipationNotFoundException.class)
    @Transactional
    public void userCannotVoteBecauseParticipationExecutorNotFoundTest() throws Throwable {
        Game stubGame = new Game();
        stubGame.setGameId(1);

        try {
            Method method = PhaseServiceImpl.class.getDeclaredMethod("canUserVote", Game.class, Participation.class);
            method.setAccessible(true);
            Object value = method.invoke(PhaseServiceImpl.class.getConstructors()[0]
                    .newInstance(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService), stubGame, null);
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        }
    }

    @Test
    @Transactional
    public void checkWhichRoleTypesAreInTheGameTest() {
        Game stubGame = new Game();
        stubGame.setGameId(1);
        stubGame.setDayDuration(Duration.ofSeconds(60));
        stubGame.setNightDuration(Duration.ofSeconds(60));

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);
        Card card2 = new Card(RoleType.STALIN, "stalin", "", true);

        Role role1 = new Role();
        role1.setCard(card1);
        Role role2 = new Role();
        role2.setCard(card2);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);
        roles1.add(role2);

        Set<Role> roles2 = new HashSet<>();
        roles2.add(role1);


        Participation participation1 = new Participation();
        participation1.setParticipationId(1);
        participation1.setRoles(roles1);

        Participation participation2 = new Participation();
        participation2.setParticipationId(2);
        participation2.setRoles(roles2);

        Participation participation3 = new Participation();
        participation3.setParticipationId(3);
        participation3.setRoles(roles2);
        participation3.setAlive(false);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);
        participations.add(participation2);
        participations.add(participation3);

        stubGame.setParticipations(participations);

        try {
            Method method = PhaseServiceImpl.class.getDeclaredMethod("getRoleTypesAliveInGame", Game.class);
            method.setAccessible(true);
            Object value = method.invoke(PhaseServiceImpl.class.getConstructors()[0]
                    .newInstance(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService), stubGame);
            List<RoleType> roleTypesResult = (ArrayList<RoleType>) value;
            roleTypesResult.sort(Enum::compareTo);
            Assert.assertEquals(RoleType.SLAV, roleTypesResult.get(0));
            Assert.assertEquals(RoleType.STALIN, roleTypesResult.get(1));
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Transactional
    public void mayorKilledWithSuccesor() {
        Game stubGame = new Game();
        stubGame.setGameId(1);
        stubGame.setDayDuration(Duration.ofSeconds(60));
        stubGame.setNightDuration(Duration.ofSeconds(60));

        Phase stubPhase = new Phase();
        stubPhase.setState(State.DAY);
        stubPhase.setRoleTypeVoter(RoleType.SLAV);
        stubPhase.setPhaseNumber(0);

        stubGame.getPhases().add(stubPhase);

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);
        Card card2 = new Card(RoleType.STALIN, "stalin", "", true);
        Card card3 = new Card(RoleType.MAYOR, "mayor", "", false);

        Role role1 = new Role();
        role1.setCard(card1);
        Role role2 = new Role();
        role2.setCard(card2);
        Role role3 = new Role();
        role3.setCard(card3);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);
        roles1.add(role2);

        Set<Role> roles2 = new HashSet<>();
        roles2.add(role1);

        Set<Role> roles3 = new HashSet<>();
        roles3.add(role3);


        Participation participation1 = new Participation();
        participation1.setParticipationId(1);
        participation1.setRoles(roles1);

        User user1 = new User();
        user1.setUsername("user1");
        user1.getParticipations().add(participation1);
        participation1.setUser(user1);

        Participation participation2 = new Participation();
        participation2.setParticipationId(2);
        participation2.setRoles(roles2);

        User user2 = new User();
        user2.setUsername("user2");
        user2.getParticipations().add(participation2);
        participation2.setUser(user2);

        Participation participation3 = new Participation();
        participation3.setParticipationId(3);
        participation3.setRoles(roles3);

        User user3 = new User();
        user3.setUsername("user3");
        participation3.setUser(user3);


        Action action1 = new Action();
        action1.setAffected(participation3);
        action1.setPhase(stubPhase);
        action1.setActionType(ActionType.VOTE);
        action1.setTime(LocalDateTime.now());

        Action action2 = new Action();
        action2.setAffected(participation3);
        action2.setPhase(stubPhase);
        action2.setActionType(ActionType.VOTE);
        action2.setTime(LocalDateTime.now().plusSeconds(60));

        Action action3 = new Action();
        action3.setAffected(participation2);
        action3.setPhase(stubPhase);
        action3.setActionType(ActionType.MAYOR_CHOICE);
        action3.setTime(LocalDateTime.now().plusSeconds(40));

        participation1.getActionsDone().add(action1);
        participation1.getActionsDone().add(action2);
        participation3.getActionsDone().add(action3);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);
        participations.add(participation2);
        participations.add(participation3);

        stubGame.setParticipations(participations);

        when(gameService.getGame(1)).thenReturn(stubGame);
        when(phaseRepository.findTopByGameOrderByPhaseNumberDesc(stubGame)).thenReturn(stubPhase);
        when(participationRepository.findByParticipationId(2)).thenReturn(participation2);
        when(cardRepository.findByRoleType(RoleType.MAYOR)).thenReturn(card3);

        phaseService = new PhaseServiceImpl(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService);
        phaseService.endPhase(1);
        ArgumentCaptor<Game> gameArgumentCaptor = ArgumentCaptor.forClass(Game.class);
        verify(gameService, times(2)).saveGame(gameArgumentCaptor.capture());
        Game game = gameArgumentCaptor.getAllValues().get(0);
        Participation part2 = game.getParticipations().stream().filter(p -> p.getParticipationId() == participation2.getParticipationId()).findFirst().get();
        Participation part3 = game.getParticipations().stream().filter(p -> p.getParticipationId() == participation3.getParticipationId()).findFirst().get();

        Assert.assertTrue(part2.isAlive());
        Assert.assertTrue(part2.getRoles().stream().anyMatch(r -> r.getCard().getRoleType().equals(RoleType.MAYOR)));

        Assert.assertFalse(part3.isAlive());
        Assert.assertTrue(part3.getRoles().stream().anyMatch(r -> r.getCard().getRoleType().equals(RoleType.MAYOR)));
        Assert.assertTrue(game.isActive());
    }

    @Test
    @Transactional
    public void hunterKilledWithTarget() {
        Game stubGame = new Game();
        stubGame.setGameId(1);
        stubGame.setDayDuration(Duration.ofSeconds(60));
        stubGame.setNightDuration(Duration.ofSeconds(60));

        Phase stubPhase = new Phase();
        stubPhase.setState(State.DAY);
        stubPhase.setRoleTypeVoter(RoleType.SLAV);
        stubPhase.setPhaseNumber(0);

        stubGame.getPhases().add(stubPhase);

        Card card1 = new Card(RoleType.SLAV, "slav", "", false);
        Card card2 = new Card(RoleType.STALIN, "stalin", "", true);
        Card card3 = new Card(RoleType.HUNTER, "hunter", "", false);

        Role role1 = new Role();
        role1.setCard(card1);
        Role role2 = new Role();
        role2.setCard(card2);
        Role role3 = new Role();
        role3.setCard(card3);

        Set<Role> roles1 = new HashSet<>();
        roles1.add(role1);
        roles1.add(role2);

        Set<Role> roles2 = new HashSet<>();
        roles2.add(role1);

        Set<Role> roles3 = new HashSet<>();
        roles3.add(role3);


        Participation participation1 = new Participation();
        participation1.setParticipationId(1);
        participation1.setRoles(roles1);

        User user1 = new User();
        user1.setUsername("user1");
        user1.getParticipations().add(participation1);
        participation1.setUser(user1);

        Participation participation2 = new Participation();
        participation2.setParticipationId(2);
        participation2.setRoles(roles2);

        User user2 = new User();
        user2.setUsername("user2");
        user2.getParticipations().add(participation2);
        participation2.setUser(user2);

        Participation participation3 = new Participation();
        participation3.setParticipationId(3);
        participation3.setRoles(roles3);

        User user3 = new User();
        user3.setUsername("user3");
        participation3.setUser(user3);


        Action action1 = new Action();
        action1.setAffected(participation3);
        action1.setPhase(stubPhase);
        action1.setActionType(ActionType.VOTE);
        action1.setTime(LocalDateTime.now());

        Action action2 = new Action();
        action2.setAffected(participation3);
        action2.setPhase(stubPhase);
        action2.setActionType(ActionType.VOTE);
        action2.setTime(LocalDateTime.now().plusSeconds(60));

        Action action3 = new Action();
        action3.setAffected(participation2);
        action3.setPhase(stubPhase);
        action3.setActionType(ActionType.HUNTER_TARGET);
        action3.setTime(LocalDateTime.now().plusSeconds(40));

        participation1.getActionsDone().add(action1);
        participation1.getActionsDone().add(action2);
        participation3.getActionsDone().add(action3);

        Set<Participation> participations = new HashSet<>();
        participations.add(participation1);
        participations.add(participation2);
        participations.add(participation3);

        stubGame.setParticipations(participations);

        when(gameService.getGame(1)).thenReturn(stubGame);
        when(phaseRepository.findTopByGameOrderByPhaseNumberDesc(stubGame)).thenReturn(stubPhase);
        when(participationRepository.findByParticipationId(2)).thenReturn(participation2);
        when(cardRepository.findByRoleType(RoleType.MAYOR)).thenReturn(card3);

        phaseService = new PhaseServiceImpl(phaseRepository, simpMessagingTemplate, participationRepository, cardRepository, gameService, actionRepository, notificationService);
        phaseService.endPhase(1);
        ArgumentCaptor<Game> gameArgumentCaptor = ArgumentCaptor.forClass(Game.class);
        verify(gameService).saveGame(gameArgumentCaptor.capture());
        Game game = gameArgumentCaptor.getAllValues().get(0);
        Participation part2 = game.getParticipations().stream().filter(p -> p.getParticipationId() == participation2.getParticipationId()).findFirst().get();
        Participation part3 = game.getParticipations().stream().filter(p -> p.getParticipationId() == participation3.getParticipationId()).findFirst().get();

        Assert.assertFalse(part2.isAlive());

        Assert.assertFalse(part3.isAlive());
        Assert.assertTrue(part3.getRoles().stream().anyMatch(r -> r.getCard().getRoleType().equals(RoleType.HUNTER)));
    }
}
