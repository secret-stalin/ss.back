package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.UserDTO;
import be.kdg.ssbackend.dto.UserStatDTO;
import be.kdg.ssbackend.exceptions.EmailFormatException;
import be.kdg.ssbackend.exceptions.UserAlreadyExistException;
import be.kdg.ssbackend.model.game.*;
import be.kdg.ssbackend.model.user.Notification;
import be.kdg.ssbackend.model.user.NotificationType;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.UserRepository;
import be.kdg.ssbackend.repositories.api.VerificationTokenRepository;
import be.kdg.ssbackend.services.api.ChatService;
import be.kdg.ssbackend.services.api.NotificationService;
import be.kdg.ssbackend.services.api.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class UserServiceImplTest {

    UserService userService;

    @Mock
    UserRepository userRepository;
    @Mock
    VerificationTokenRepository tokenRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    JavaMailSender mailSender;
    @Mock
    NotificationService notificationService;

    @Mock
    ChatService chatService;

    private User user1;
    private User user2;
    private User user3;
    private Notification notification;

    @Autowired
    private UserService autowiredUserService;
    @Autowired
    private VerificationTokenRepository autowiredVerificationTokenRepository;

    @Before
    public void setUp() throws Exception {
        userService = new UserServiceImpl(userRepository, tokenRepository, notificationService, bCryptPasswordEncoder, chatService, mailSender);
    }

    public void initializeFriendTestMocks() {
        List<User> userList = new ArrayList<>();
        this.user1 = new User("user1@mail.com", "pass");
        user1.setUserId(1);
        user1.setUsername("user1");
        this.user2 = new User("user2@mail.com", "pass");
        user2.setUserId(2);
        user2.setUsername("user2");
        this.user3 = new User("user3@mail.com", "pass");
        user3.setUserId(3);
        user3.setUsername("user3");

        user1.getFriends().add(user2);
        user2.getFriends().add(user1);

        //User 3 has a friend request from user 2
        this.notification = new Notification(2, "test", false, NotificationType.FRIEND_REQUEST, user3);
        this.notification.setNotificationId(1);
        user3.getNotifications().put(this.notification.getNotificationId(), this.notification);

        userList.add(user1);
        userList.add(user2);
        userList.add(user3);

        when(userRepository.findByUsername(any(String.class))).thenAnswer(i -> userList.stream().filter(u -> u.getUsername().equals(i.getArgument(0))).findFirst().get());
        when(userRepository.findByEmail(any(String.class))).thenAnswer(i -> userList.stream().filter(u -> u.getEmail().equals(i.getArgument(0))).findFirst().get());
    }

    public void initializeGameStatMocks(RoleType winningRole, RoleType playedAs, boolean PlayerWin) {
        User userStub = new User();
        userStub.setUsername("GameStatTestUser");
        userStub.setEmail("GameStatTestUser@mmmm.com");
        userStub.setUserId(1);

        Set<Participation> participations = new HashSet<>();

        Card cardRoleUser = new Card();
        cardRoleUser.setRoleType(playedAs);
        cardRoleUser.setCardId(1);

        Role role1 = new Role();
        role1.setCard(cardRoleUser);

        Participation participationStub1 = new Participation();
        participationStub1.setAlive(PlayerWin);
        Set<Role> roles = new HashSet<>();
        roles.add(role1);
        participationStub1.setRoles(roles);

        Game gameStub1 = new Game();
        gameStub1.setWinningRole(winningRole);
        gameStub1.setActive(false);
        gameStub1.setGameId(1);
        participationStub1.setGame(gameStub1);
        participations.add(participationStub1);
        userStub.setParticipations(new ArrayList<>(participations));
        gameStub1.setParticipations(participations);

        when(userRepository.findByUsername(userStub.getUsername())).thenReturn(userStub);
    }

    @Test
    @Transactional
    public void createUserWithUserName() {
        UserDTO userDTO = new UserDTO("secretstalingame@gmail.com", "RobbyD", "password");
        userService.createUserAccount(userDTO, false);
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        User createdUser = userArgumentCaptor.getValue();
        assertFalse(createdUser.getUsername().matches("Player[0-9]*"));
    }

    @Test(expected = UserAlreadyExistException.class)
    @Transactional
    public void createUserEmailExists() {
        UserDTO existingUserDTO = new UserDTO("secretstalingame@gmail.com", "ExistingUser", "password");
        userService.createUserAccount(existingUserDTO, false);

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        User createdUser = userArgumentCaptor.getValue();
        when(userRepository.findByEmail(createdUser.getEmail())).thenReturn(createdUser);
        UserDTO userDTO = new UserDTO("secretstalingame@gmail.com", "diffUser", "password");
        userService.createUserAccount(userDTO, false);
    }

    @Test(expected = UserAlreadyExistException.class)
    @Transactional
    public void createUserNameExists() {
        UserDTO existingUserDTO = new UserDTO("secretstalingame@gmail.com", "ExistingUser", "password");
        userService.createUserAccount(existingUserDTO, false);

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        User createdUser = userArgumentCaptor.getValue();
        when(userRepository.findByUsername(createdUser.getUsername())).thenReturn(createdUser);

        UserDTO userDTO = new UserDTO("123diffEmail@554495545485545488874email.com", "ExistingUser", "password");
        userService.createUserAccount(userDTO, false);
    }

    @Test()
    @Transactional
    public void createUserNoUserName() {
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("secretstalingame@gmail.com");
        userDTO.setNewPassword("pass");

        userService.createUserAccount(userDTO, false);

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        User userResult = userArgumentCaptor.getValue();

        assertNotNull(userResult);
        assertTrue(userResult.getUsername().matches("Player[0-9]*"));
    }

    @Test()
    @Transactional
    public void checkPasswordEncoding() {
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("secretstalingame@gmail.com");
        userDTO.setNewPassword("pass");
        userService.createUserAccount(userDTO, false);

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        User userResult = userArgumentCaptor.getValue();

        assertNotNull(userResult);
        assertTrue(userResult.getPassword().matches("\\A\\$2a?\\$\\d\\d\\$[./0-9A-Za-z]{53}"));
    }

    @Test(expected = EmailFormatException.class)
    @Transactional
    public void checkWrongEmail() {
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("passwordteail.com");
        userDTO.setNewPassword("pass");
        userService.createUserAccount(userDTO, false);
    }

    @Test
    @Transactional
    public void testEnableUser() {
        UserDTO existingUserDTO = new UserDTO("secretstalingame@gmail.com", "ExistingUser", "password");
        autowiredUserService.createUserAccount(existingUserDTO, true);
        User user = autowiredUserService.findByUsername("ExistingUser");
        autowiredUserService.checkToken(autowiredVerificationTokenRepository.findByUser(user).getToken());
        assertTrue(user.isEnabled());
    }

    @Test
    @Transactional
    public void testNotEnabledUser() {
        UserDTO existingUserDTO = new UserDTO("secretstalingame@gmail.com", "ExistingUser", "password");
        autowiredUserService.createUserAccount(existingUserDTO, true);
        User user = autowiredUserService.findByUsername("ExistingUser");
        assertFalse(user.isEnabled());
    }

    @Test
    public void testIsFriendsFrom() {
        this.initializeFriendTestMocks();
        assertTrue(userService.isFriendFrom(user1.getEmail(), user2.getUsername()));
        assertTrue(userService.isFriendFrom(user2.getEmail(), user1.getUsername()));
        assertFalse(userService.isFriendFrom(user1.getEmail(), user3.getUsername()));
        assertFalse(userService.isFriendFrom(user3.getEmail(), user1.getUsername()));

    }

    @Test
    public void testHasFriendRequestFromUser() {
        this.initializeFriendTestMocks();

        assertTrue(userService.hasFriendRequestFromUser(user3.getEmail(), user2.getUsername(), true));
        assertTrue(userService.hasFriendRequestFromUser(user2.getEmail(), user3.getUsername(), false));
        assertFalse(userService.hasFriendRequestFromUser(user3.getEmail(), user2.getUsername(), false));
        assertFalse(userService.hasFriendRequestFromUser(user2.getEmail(), user3.getUsername(), true));


        assertFalse(userService.hasFriendRequestFromUser(user1.getEmail(), user3.getUsername(), true));
        assertFalse(userService.hasFriendRequestFromUser(user3.getEmail(), user1.getUsername(), false));
        assertFalse(userService.hasFriendRequestFromUser(user1.getEmail(), user3.getUsername(), false));
        assertFalse(userService.hasFriendRequestFromUser(user3.getEmail(), user1.getUsername(), true));
    }

    @Test
    public void testAcceptFriendRequest() {
        this.initializeFriendTestMocks();
        userService.acceptFriendRequest(user3.getEmail(), user2.getUsername());

        User user3Result = userRepository.findByEmail(user3.getEmail());
        User user2Result = userRepository.findByEmail(user2.getEmail());

        assertTrue(user3Result.getFriends().stream().anyMatch(u -> u.getEmail().equals(user2.getEmail())));
        assertTrue(user2Result.getFriends().stream().anyMatch(u -> u.getEmail().equals(user3.getEmail())));
        assertFalse(user2Result.getFriends().stream().anyMatch(u -> u.getEmail().equals(user2.getEmail())));
        assertFalse(user3Result.getFriends().stream().anyMatch(u -> u.getEmail().equals(user3.getEmail())));

        assertNull(user3Result.getNotifications().get(this.notification.getNotificationId()));
    }

    @Test
    public void testDeclineFriendRequest() {
        this.initializeFriendTestMocks();
        userService.declineFriendRequest(user3.getEmail(), user2.getUsername());

        User user3Result = userRepository.findByEmail(user3.getEmail());
        User user2Result = userRepository.findByEmail(user2.getEmail());

        assertFalse(user3Result.getFriends().stream().anyMatch(u -> u.getEmail().equals(user2.getEmail())));
        assertFalse(user2Result.getFriends().stream().anyMatch(u -> u.getEmail().equals(user3.getEmail())));
        assertFalse(user2Result.getFriends().stream().anyMatch(u -> u.getEmail().equals(user2.getEmail())));
        assertFalse(user3Result.getFriends().stream().anyMatch(u -> u.getEmail().equals(user3.getEmail())));

        assertNull(user3Result.getNotifications().get(this.notification.getNotificationId()));
    }

    @Test
    public void testSendFriendRequest() {
        this.initializeFriendTestMocks();

        userService.sendFriendRequest(user1.getEmail(), user3.getUsername());

        User user3Result = userRepository.findByEmail(user3.getEmail());

        assertNotNull(user3Result.getNotifications().values().stream().filter(n -> n.getTargetId() == user1.getUserId()));
    }

    @Test
    public void testRemoveAsFriend() {
        this.initializeFriendTestMocks();

        userService.removeFriend(user1.getEmail(), user2.getUsername());

        User user1Result = userRepository.findByEmail(user1.getEmail());
        User user2Result = userRepository.findByEmail(user2.getEmail());

        assertFalse(user1Result.getFriends().stream().anyMatch(u -> u.getEmail().equals(user2.getEmail())));
        assertFalse(user2Result.getFriends().stream().anyMatch(u -> u.getEmail().equals(user1.getEmail())));
    }

    @Test
    public void testRemoveNotificationFromUser() {
        this.initializeFriendTestMocks();
        assertNotNull(user3.getNotifications().get(this.notification.getNotificationId()));

        userService.removeFriendRequestNotificationFromUser(user3, notification.getTargetId());

        User user3Result = userRepository.findByEmail(user3.getEmail());
        assertNull(user3Result.getNotifications().get(this.notification.getNotificationId()));
    }

    @Test
    public void testGameWonAsStalin() {
        initializeGameStatMocks(RoleType.STALIN, RoleType.STALIN, true);

        UserStatDTO statsDTO = userService.getUserGameStats("GameStatTestUser");

        assertEquals(1, statsDTO.getGamePlayedAsStalin());
        assertEquals(1, statsDTO.getGamesWonAsStalin());
        assertEquals(0, statsDTO.getGamesPlayedAsSlav());
        assertEquals(0, statsDTO.getGameWonAsSlav());
    }

    @Test
    public void testGameWonAsSlav() {
        initializeGameStatMocks(RoleType.SLAV, RoleType.SLAV, true);

        UserStatDTO statsDTO = userService.getUserGameStats("GameStatTestUser");

        assertEquals(0, statsDTO.getGamePlayedAsStalin());
        assertEquals(0, statsDTO.getGamesWonAsStalin());
        assertEquals(1, statsDTO.getGamesPlayedAsSlav());
        assertEquals(1, statsDTO.getGameWonAsSlav());
    }

    @Test
    public void testGameLost() {
        initializeGameStatMocks(RoleType.STALIN, RoleType.SLAV, false);

        UserStatDTO statsDTO = userService.getUserGameStats("GameStatTestUser");

        assertEquals(0, statsDTO.getGamePlayedAsStalin());
        assertEquals(0, statsDTO.getGamesWonAsStalin());
        assertEquals(1, statsDTO.getGamesPlayedAsSlav());
        assertEquals(0, statsDTO.getGameWonAsSlav());
    }
}
