package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.model.game.Game;
import be.kdg.ssbackend.repositories.api.CardRepository;
import be.kdg.ssbackend.repositories.api.ChatRepository;
import be.kdg.ssbackend.repositories.api.GameRepository;
import be.kdg.ssbackend.services.api.GameService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class GameServiceTest {

    @Autowired
    GameService gameService;

    @Mock
    GameRepository gameRepository;

    @Mock
    CardRepository cardRepository;

    @Mock
    ChatRepository chatRepository;

       @Test
    @Transactional
    public void GameCreationTest(){
        Random rand = new Random();
        when(gameRepository.save(any(Game.class))).thenAnswer(i -> {
            Game g = i.getArgument(0);
            g.setGameId(rand.nextInt());
            return g;
        });
        Game actual = gameService.createGame();
        Assert.assertNotEquals(0, actual.getGameId());
    }



}
