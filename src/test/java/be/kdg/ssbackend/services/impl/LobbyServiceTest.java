package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.GameSettingsDTO;
import be.kdg.ssbackend.dto.LobbyDTO;
import be.kdg.ssbackend.dto.UserDTO;
import be.kdg.ssbackend.exceptions.LobbyNotFoundException;
import be.kdg.ssbackend.model.chat.GameChat;
import be.kdg.ssbackend.model.game.*;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.CardRepository;
import be.kdg.ssbackend.repositories.api.ChatRepository;
import be.kdg.ssbackend.repositories.api.InvitationRepository;
import be.kdg.ssbackend.repositories.api.LobbyRepository;
import be.kdg.ssbackend.services.api.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class LobbyServiceTest {

    @Mock
    CardRepository cardRepository;

    @Mock
    LobbyRepository lobbyRepository;

    @Mock
    UserService userService;

    @Mock
    GameService gameService;

    @Mock
    PhaseService phaseService;

    @Mock
    NotificationService notificationService;

    @Mock
    ChatRepository chatRepository;

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @Mock
    InvitationRepository invitationRepository;

    @Mock
    JavaMailSender mailSender;

    LobbyService lobbyService;


    @Test
    @Transactional
    public void LobbyCreationTest() {

        Game game = new Game();
        game.setGameId(1);
        when(gameService.getGame(1)).thenReturn(game);

        List<RoleType> rts = new ArrayList<>();
        List<Card> cards = new ArrayList<>();
        rts.add(RoleType.SLAV);
        cards.add(new Card(RoleType.SLAV, "slav", "", false));
        rts.add(RoleType.STALIN);
        cards.add(new Card(RoleType.STALIN, "stalin", "", true));
        when(cardRepository.findAllByRoleTypeIn(rts)).thenReturn(cards);
        when(lobbyRepository.save(any(Lobby.class))).thenAnswer(i -> i.getArgument(0));
        when(chatRepository.save(any(GameChat.class))).thenAnswer(i -> i.getArgument(0));

        UserDTO host = new UserDTO();
        host.setUsername("test");
        host.setEmail("test@mail.com");
        User stubbedUser = new User();
        stubbedUser.setUsername("test");
        stubbedUser.setEmail("test@mail.com");
        stubbedUser.setUserId(1);
        when(userService.findByUsername(host.getUsername())).thenReturn(stubbedUser);
        List<String> roles = new ArrayList<>();
        roles.add("SLAV");
        roles.add("STALIN");
        GameSettingsDTO settings = new GameSettingsDTO(1, "test", 6, true, 60, 60, roles);

        lobbyService = new LobbyServiceImpl(gameService, cardRepository, lobbyRepository, userService, invitationRepository, simpMessagingTemplate, phaseService, notificationService);
        LobbyDTO result = lobbyService.createLobby(1, settings, "test@mail.com");

        Assert.assertEquals("test", result.getGameSettings().get_gameName());
        Assert.assertEquals(2, result.getGameSettings().get_roles().size());
        Assert.assertNotNull(result.getUsers());
        Assert.assertEquals(0, result.getLobbyId());
        Assert.assertEquals(60, result.getGameSettings().get_dayTime());
        Assert.assertEquals(60, result.getGameSettings().get_nightTime());
        Assert.assertEquals(6, result.getGameSettings().get_maxPlayers());
        Assert.assertEquals(1, result.getGameSettings().get_gameId());
    }

    @Test
    @Transactional
    public void fetchExistingLobbyTest() {
        Lobby output = new Lobby();
        output.setLobbyId(4);
        when(lobbyRepository.findById(4)).thenReturn(Optional.of(output));
        lobbyService = new LobbyServiceImpl(null, null, lobbyRepository, null,  null, null, phaseService, notificationService);
        LobbyDTO actual = lobbyService.getLobby(4);
        Assert.assertEquals(4, actual.getLobbyId());
    }


    @Test(expected = LobbyNotFoundException.class)
    @Transactional
    public void fetchNonExistingLobbyTest() {
        when(lobbyRepository.getOne(any(Integer.TYPE))).thenReturn(null);
        lobbyService = new LobbyServiceImpl(null, null, lobbyRepository, null,  null, null, phaseService, notificationService);

        lobbyService.getLobby(1);
    }

    @Test
    @Transactional
    public void startLobbyTest() {
        Game gameStub = new Game();
        gameStub.setGameId(1);
        Set<User> userStubs = new HashSet<>();
        for (int i = 0; i < 4; i++) {
            User userStub = new User();
            userStub.setEmail(String.format("test%d@mail.com", i));
            userStub.setUserId(i);
            userStub.setUsername(String.format("user%d", i));
            userStubs.add(userStub);
        }

        User host = new User();
        host.setEmail("test5@mail.com");
        host.setUserId(5);
        host.setUsername("user5");
        userStubs.add(host);

        Lobby lobbyStub = new Lobby();
        lobbyStub.setCreatedBy(host);
        lobbyStub.setLobbyId(1);
        lobbyStub.setGame(gameStub);
        lobbyStub.setJoinedUsers(userStubs);

        when(lobbyRepository.findById(1)).thenReturn(Optional.of(lobbyStub));
        when(cardRepository.findByRoleType(any(RoleType.class))).thenAnswer(i -> {
            Card cardStub = new Card();
            cardStub.setRoleType(i.getArgument(0));
            return cardStub;
        });
        when(gameService.saveGame(any(Game.class))).thenAnswer(i -> i.getArgument(0));


        lobbyService = new LobbyServiceImpl(gameService, cardRepository, lobbyRepository, userService, null, simpMessagingTemplate, phaseService, notificationService);
        int result = lobbyService.startLobby(1, "test5@mail.com");


        ArgumentCaptor<Game> gameArgumentCaptor = ArgumentCaptor.forClass(Game.class);
        verify(gameService).saveGame(gameArgumentCaptor.capture());
        Game startedGame = gameArgumentCaptor.getValue();
        Assert.assertEquals(1, result);
        Assert.assertEquals(2, startedGame.getGameChats().size());
        Assert.assertEquals(5, startedGame.getParticipations().size());


    }

    @Test
    @Transactional
    public void inviteTest() {
        when(userService.findByEmail(any(String.class))).thenAnswer(i -> {
            User user = new User();
            user.setEmail(i.getArgument(0));
            user.setUsername("test");
            user.setUserId(1);
            return user;
        });
        when(lobbyRepository.findById(any(Integer.class))).thenAnswer(i -> {
            Lobby lobby = new Lobby();
            lobby.setLobbyId(i.getArgument(0));
            return Optional.of(lobby);
        });


        lobbyService = new LobbyServiceImpl(null, null, lobbyRepository, userService, invitationRepository, null, phaseService, notificationService);
        ((LobbyServiceImpl) lobbyService).setMailSender(mailSender);
        lobbyService.createInvitation(1, "test@mail.com");

        ArgumentCaptor<Invitation> invitationArgumentCaptor = ArgumentCaptor.forClass(Invitation.class);
        verify(invitationRepository).save(invitationArgumentCaptor.capture());

        Invitation passedInvitation = invitationArgumentCaptor.getValue();
        Assert.assertEquals("test@mail.com", passedInvitation.getInvitee().getEmail());
        Assert.assertEquals(1, passedInvitation.getLobby().getLobbyId());
        Assert.assertFalse(passedInvitation.isAccepted());
        Assert.assertFalse(passedInvitation.isDeclined());

        ArgumentCaptor<SimpleMailMessage> simpleMailMessageArgumentCaptor = ArgumentCaptor.forClass(SimpleMailMessage.class);
        verify(mailSender).send(simpleMailMessageArgumentCaptor.capture());
        SimpleMailMessage passedMessage = simpleMailMessageArgumentCaptor.getValue();

        Assert.assertNotNull(passedMessage.getTo());
        Assert.assertEquals("test@mail.com", passedMessage.getTo()[0]);
    }


}

