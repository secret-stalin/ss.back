package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.EventDTO;
import be.kdg.ssbackend.dto.ReplayDTO;
import be.kdg.ssbackend.exceptions.NoAccessException;
import be.kdg.ssbackend.model.chat.GameChat;
import be.kdg.ssbackend.model.chat.Message;
import be.kdg.ssbackend.model.game.*;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.PhaseRepository;
import be.kdg.ssbackend.services.api.GameChatService;
import be.kdg.ssbackend.services.api.GameService;
import be.kdg.ssbackend.services.api.ReplayService;
import be.kdg.ssbackend.services.api.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ReplayServiceTest {

    @Mock
    private UserService userService;

    @Mock
    private GameService gameService;

    @Mock
    private GameChatService gameChatService;

    @Mock
    private PhaseRepository phaseRepo;

    private ReplayService replayService;

    @Before
    public void setup() {


        when(userService.findByEmail(any(String.class))).thenAnswer(i -> {
            User result = new User();
            result.setUsername("test");
            result.setEmail(i.getArgument(0));
            result.setUserId(i.getArgument(0).equals("test1@mail.com") ? 1 : 404);
            return result;
        });

        Set<Participation> participations = new HashSet<>();
        for (int j = 1; j < 3; j++) {
            User user = new User();
            user.setUserId(j);
            user.setEmail(String.format("test%d@mail.com", j));
            user.setUsername(String.format("test%d", j));

            Participation participation = new Participation();
            participation.setColorCode(String.format("#133769420%d", j));
            participation.setActionsDone(new ArrayList<>());
            participation.setActionsDoneTo(new ArrayList<>());
            participation.setParticipationId(j);
            participation.setUser(user);

            participations.add(participation);
        }

        Set<GameChat> gameChats = new HashSet<>();
        for (int j = 1; j < 3; j++) {
            GameChat chat = new GameChat();
            chat.setName(String.format("test%d", j));
            chat.setAccess(j == 1 ? RoleType.SLAV : RoleType.STALIN);
            gameChats.add(chat);
        }


        when(gameService.getGame(any(Integer.class))).thenAnswer(i -> {
            Game game = new Game();
            game.setGameId(i.getArgument(0));
            game.setActive(true);
            participations.forEach(p -> p.setGame(game));
            game.setParticipations(participations);

            gameChats.forEach(gc -> gc.setGame(game));
            game.setGameChats(gameChats);
            return game;
        });

        when(gameChatService.getMessagesFromChat(any(Integer.class))).thenAnswer( i -> {
            List<Message> messages = new ArrayList<>();
            for (int j = 1; j < 3; j++) {
                Message message = new Message();
                message.setMessageId(j * (Integer) i.getArgument(0));
                if (gameChats.stream().anyMatch(gc -> gc.getChatId() == (Integer) i.getArgument(0))){
                    message.setChat(gameChats.stream().filter(gc -> gc.getChatId() == (Integer) i.getArgument(0)).findFirst().get());
                }
                message.setSender(participations.stream().findAny().get().getUser());
                message.setText(String.format("Test message %d", j * (Integer) i.getArgument(0)));
                message.setTime(LocalDateTime.now());
                messages.add(message);
            }
            return messages;
        });


        when(phaseRepo.findAllByGame(any(Game.class))).thenAnswer(i -> {
            List<Phase> phases = new ArrayList<>();
            for (int j = 1; j < 3; j++) {
                Phase phase = new Phase();
                phase.setGame(i.getArgument(0));
                phase.setPhaseId(j);
                phase.setPhaseNumber(j);
                phase.setRoleTypeVoter(j == 1 ? RoleType.STALIN : RoleType.SLAV);
                phase.setState(j == 1 ? State.DAY : State.NIGHT);
                List<Action> actions = new ArrayList<>();
                for (int k = 0; k < 3; k++) {
                    Action action = new Action();
                    action.setActionId(k);
                    action.setActionType(phase.getState() == State.DAY ? ActionType.VOTE : ActionType.KILL);
                    action.setAffected(participations.stream().findAny().get());
                    action.setExecutor(participations.stream().filter(p -> !p.equals(action.getAffected())).findAny().get());
                    action.setPhase(phase);
                    action.setTime(LocalDateTime.now());
                    actions.add(action);
                }
                phase.setActions(actions);
                phases.add(phase);
            }
            return phases;
        });

        this.replayService = new ReplayServiceImpl(userService,gameService,gameChatService,phaseRepo);
    }


    @Test
    public void GetReplayOfGame() {
        ReplayDTO result = replayService.getReplay(1 , "test1@mail.com");
        Assert.assertNotNull(result.getEvents());
        Assert.assertNotNull(result.getParticipations());
        for (EventDTO event: result.getEvents()) {
            Assert.assertNotNull(event.getTimestamp());
            if (event.getEventType() == EventType.CHAT){
                Assert.assertNotNull(event.getMessage());
                Assert.assertEquals(0, event.getAffectedPlayerId());
                Assert.assertEquals(0, event.getExecutorId());
                Assert.assertThat(event.getChatAccess(), anyOf(is(RoleType.SLAV), is(RoleType.STALIN)) );
            }
            if(event.getEventType()== EventType.VOTE){
                Assert.assertNull(event.getMessage());
                Assert.assertNotEquals(0, event.getExecutorId());
                Assert.assertNotEquals(0, event.getAffectedPlayerId());
            }
            if(event.getEventType() == EventType.KILL){
                Assert.assertNull(event.getMessage());
                Assert.assertEquals(0, event.getExecutorId());
                Assert.assertNotEquals(0, event.getAffectedPlayerId());
            }
        }
    }

    @Test(expected = NoAccessException.class)
    public void getReplayOfGameNoParticipation() {
        replayService.getReplay(1, "noAccess@mail.com");
    }
}
