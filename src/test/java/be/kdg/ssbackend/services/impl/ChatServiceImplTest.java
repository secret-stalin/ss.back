package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.MessageDTO;
import be.kdg.ssbackend.exceptions.NoChatAccessException;
import be.kdg.ssbackend.model.chat.Chat;
import be.kdg.ssbackend.model.chat.Message;
import be.kdg.ssbackend.model.chat.PrivateChat;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.ChatRepository;
import be.kdg.ssbackend.repositories.api.MessageRepository;
import be.kdg.ssbackend.services.api.ChatService;
import be.kdg.ssbackend.services.api.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class ChatServiceImplTest {

    @Autowired
    ChatRepository chatRepository;
    @Autowired
    ChatService chatService;
    @Autowired
    UserService userService;
    @Autowired
    MessageRepository messageRepository;

    private User userWithNoAccess;
    private User userWithAccess;
    private Chat chat;
    private MessageDTO messageDto;

    @Before
    public void setUp() throws Exception {
        userWithAccess = new User();
        userWithAccess.setPassword("test");
        userWithAccess.setEmail("test@mailiemail.com");
        userWithAccess.setUsername("testName");
        userWithAccess.setEnabled(true);
        PrivateChat chat = new PrivateChat();

        List<User> users = new ArrayList<>();
        users.add(userWithAccess);
        chat.setUsers(users);

        userWithAccess.getChats().add(chat);
        userService.save(userWithAccess);

        userWithNoAccess = new User();
        userWithNoAccess.setPassword("test2");
        userWithNoAccess.setEmail("test2@mailiemail.com");
        userWithNoAccess.setUsername("test2Name");
        userWithNoAccess.setEnabled(true);
        userService.save(userWithNoAccess);

        this.chat = userService.getChatsFromUser(userWithAccess.getEmail()).get(0);
        messageDto = new MessageDTO();
        messageDto.setText("test");
        messageDto.setTimestamp("dede");
    }

    @Test
    @Transactional
    public void SendMessageValidUser() {
        int chatId = chat.getChatId();
        chatService.sendMessage(userWithAccess.getEmail(), messageDto, chatId);
        List<MessageDTO> chatMessages = chatService.getMessagesFromChat(userWithAccess.getEmail(), chatId);
        assertTrue(chatMessages.size() > 0);

    }

    @Test(expected = NoChatAccessException.class)
    @Transactional
    public void SendMessageInValidUser() {
        int chatId = chat.getChatId();
        try {
            chatService.sendMessage(userWithNoAccess.getEmail(), messageDto, chat.getChatId());
        } finally {
            //Check if message hasn't been added to the chat
            List<Message> chatMessages = chatService.getMessagesFromChat(chatId);
            assertEquals(0, chatMessages.size());
        }
    }

    @Test
    @Transactional
    public void GetMessagesValidUser() {
        List<MessageDTO> messagesbefore = chatService.getMessagesFromChat(userWithAccess.getEmail(), chat.getChatId());
        chatService.addMessage(chat.getChatId(), messageDto, userWithAccess.getEmail());
        List<MessageDTO> messages = chatService.getMessagesFromChat(userWithAccess.getEmail(), chat.getChatId());
        assertTrue(messages.size() > messagesbefore.size());

    }

    @Test(expected = NoChatAccessException.class)
    @Transactional
    public void GetMessagesInValidUser() {
        chatService.getMessagesFromChat(userWithNoAccess.getEmail(), chat.getChatId());
    }


}
