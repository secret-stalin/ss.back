package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.UserDTO;
import be.kdg.ssbackend.model.chat.Chat;
import be.kdg.ssbackend.model.chat.PrivateChat;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.ChatRepository;
import be.kdg.ssbackend.services.api.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class ChatControllerTest {

    private MockMvc mvc;
    private ObjectMapper mapper;
    private int chatId;

    @Autowired
    ChatController chatController;

    @Autowired
    WebApplicationContext wac;

    @Autowired
    UserService userService;
    @Autowired
    ChatRepository chatRepository;

    @Before
    public void setup() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(wac).build();
        this.mapper = new ObjectMapper();
        this.mapper.configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS,false);
        this.mapper.registerModule(new JSR310Module());
        UserDTO userDTO = new UserDTO("tset@tsetmai1l.com", "testUser100", "jwtpass");
        userService.createUserAccount(userDTO, false);
        UserDTO userDTO1 = new UserDTO("tset@tsetmai2l.com", "testUser101", "jwtpass");
        userService.createUserAccount(userDTO1, false);
        List<User> users = new ArrayList<>();
        users.add(userService.findByUsername("testUser100"));
        users.add(userService.findByUsername("testUser101"));
        Chat chat = new PrivateChat(users);
        chat.setMessages(new HashSet<>());
        chatId = chatRepository.save(chat).getChatId();
    }

    @Test
    @Transactional
    public void testValidGetChatsFromUser() throws Exception {
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("tset@tsetmai1l.com");

        mvc.perform(get("/api/chat/getChats")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .principal(mockPrincipal))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void testInvalidGetChatsFromUser() throws Exception {
        mvc.perform(get("/api/chat/getChats")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    public void testValidGetExistingChatMessages() throws Exception {
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("tset@tsetmai1l.com");

        mvc.perform(get("/api/chat/getMessages/{chatId}",chatId)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .principal(mockPrincipal))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void testInvalidExistingChatMessages() throws Exception {
        mvc.perform(get("/api/chat/getMessages/{chatId}",chatId)
                .accept(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isBadRequest());
    }
}
