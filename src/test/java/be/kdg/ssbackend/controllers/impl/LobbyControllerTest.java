package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.GameSettingsDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class LobbyControllerTest {


    private MockMvc mvc;

    @Autowired
    WebApplicationContext wac;

    ObjectMapper mapper;

    @Before
    public void setup() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(wac).build();
        this.mapper = new ObjectMapper();
        this.mapper.configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS, false);
        this.mapper.registerModule(new JSR310Module());
    }


    @Test
    @Transactional
    public void createLobbyTest() throws Exception {

        List<String> rts = new ArrayList<>();
        rts.add("SLAV");
        rts.add("STALIN");

        GameSettingsDTO content = new GameSettingsDTO(1, "test", 5, true, 5, 5, rts);

        Principal mockPrincipal = Mockito.mock(Principal.class);

        Mockito.when(mockPrincipal.getName()).thenReturn("test@mail.com");

        String contentString = mapper.writeValueAsString(content);

        mvc.perform(post("/api/lobby/{id}", 1).accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .principal(mockPrincipal)
                .content(contentString))
                .andDo(print())
                .andExpect(status().isOk());
    }


    @Test
    @Transactional
    public void getExistingLobbyTest() throws Exception {
        mvc.perform(get("/api/lobby/{id}", 4).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void getNonExistingLobbyTest() throws Exception {
        mvc.perform(get("/api/lobby/{id}", 1).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }


}
