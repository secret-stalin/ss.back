package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.UserDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    private MockMvc mvc;
    private ObjectMapper mapper;
    @Mock
    private HttpServletResponse response;

    @Autowired
    WebApplicationContext wac;

    @Autowired
    UserController userController;

    @Before
    public void setup() throws Exception{
        this.mvc = MockMvcBuilders.webAppContextSetup(wac).build();
        this.mapper = new ObjectMapper();
        this.mapper.configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS,false);
        this.mapper.registerModule(new JSR310Module());

    }

    @Test
    @Transactional
    public void testValidRegistration() throws Exception {
        UserDTO userDTO = new UserDTO("testregistration132@m1ail23.com", "registration123", "jwtpass");
        String contentString = mapper.writeValueAsString(userDTO);

        mvc.perform(post("/api/registration")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(contentString)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void testInvalidEmailRegistration() throws Exception {
        UserDTO userDTO = new UserDTO("invalid", "registration123", "jwtpass");
        String contentString = mapper.writeValueAsString(userDTO);

        mvc.perform(post("/api/registration")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(contentString)
        ).andDo(print()).andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void testInvalidUserNameRegistration() throws Exception {
        UserDTO userDTO = new UserDTO("testregistration132@m1ail23.com", "testUser", "jwtpass");
        String contentString = mapper.writeValueAsString(userDTO);

        mvc.perform(post("/api/registration")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(contentString)
        ).andDo(print()).andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void testInvalidGetByUsername() throws Exception {
        mvc.perform(get("/api/user/profile/{username}", "invalidusernameshouldfail")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
