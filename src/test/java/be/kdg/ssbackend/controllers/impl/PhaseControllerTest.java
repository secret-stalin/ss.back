package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.VoteDTO;
import be.kdg.ssbackend.model.game.Game;
import be.kdg.ssbackend.services.api.GameService;
import be.kdg.ssbackend.services.api.PhaseService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.security.Principal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class PhaseControllerTest {

  private MockMvc mvc;

  @Autowired
  WebApplicationContext wac;

  private ObjectMapper mapper;
  private VoteDTO voteDTO;
  private Principal mockPrincipal;
  private Game game;

  @Autowired
  PhaseController phaseController;

  @Autowired
  GameService gameService;

  @Autowired
  PhaseService phaseService;

  @Before
  public void setup() {
    this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    this.mapper = new ObjectMapper();
    this.mapper.configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS, false);
    this.mapper.registerModule(new JSR310Module());

    game = gameService.createGame();
    //phaseService.addPhase(game.getGameId());
    voteDTO = new VoteDTO(game.getGameId(),4);
    voteDTO.setRoleTypeExecutor("SLAV");
    mockPrincipal = Mockito.mock(Principal.class);
    Mockito.when(mockPrincipal.getName()).thenReturn("test@mail.com");

  }

  @Test
  @Transactional
  public void voteTest() throws Exception{
    mvc.perform(post("/api/vote")
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .principal(mockPrincipal)
        .content(mapper.writeValueAsString(voteDTO)))
        .andDo(print())
        .andExpect(status().isOk());
  }



  /*@Test
  @Transactional
  public void getVoteResultsTest() throws Exception {
    //phaseController.vote(mockPrincipal, voteDTO);

    mvc.perform(post("/api/vote")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .principal(mockPrincipal)
            .content(mapper.writeValueAsString(voteDTO)))
            .andDo(print())
            .andExpect(status().isOk());

    mvc.perform(get("/api/voteResults/{id}", game.getGameId())
      .accept(MediaType.APPLICATION_JSON)
      .principal(mockPrincipal))
      .andDo(print())
      .andExpect(status().isOk());
  }*/


}
