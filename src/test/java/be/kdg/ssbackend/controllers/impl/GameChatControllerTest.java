package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.GameSettingsDTO;
import be.kdg.ssbackend.dto.LobbyDTO;
import be.kdg.ssbackend.dto.UserDTO;
import be.kdg.ssbackend.model.game.Game;
import be.kdg.ssbackend.services.api.GameService;
import be.kdg.ssbackend.services.api.LobbyService;
import be.kdg.ssbackend.services.api.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.ArrayList;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class GameChatControllerTest {

    private MockMvc mvc;
    private ObjectMapper mapper;
    private int gameId;

    @Autowired
    WebApplicationContext wac;

    @Autowired
    GameChatController gameChatController;

    @Autowired
    LobbyService lobbyService;
    @Autowired
    GameService gameService;
    @Autowired
    UserService userService;

    @Before
    public void setup() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(wac).build();
        this.mapper = new ObjectMapper();
        this.mapper.configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS, false);
        this.mapper.registerModule(new JSR310Module());
        UserDTO userDTO = new UserDTO("tset@tsetmai1l.com", "testUser100", "jwtpass");
        userService.createUserAccount(userDTO, false);
        Game game = gameService.createGame();
        LobbyDTO lobbyDTO = lobbyService.createLobby(game.getGameId(), new GameSettingsDTO(game.getGameId(), game.getName(), 1, false, 10, 5, new ArrayList<>()), userDTO.getEmail());
        gameId = lobbyService.startLobby(lobbyDTO.getLobbyId(), userDTO.getEmail());
    }

    @Test
    @Transactional
    public void testInvalidGetChatsFromGame() throws Exception {
        mvc.perform(get("/api/game/chats/{gameId}", gameId)
                .accept(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void testValidGetChatsFromGame() throws Exception {
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("tset@tsetmai1l.com");

        mvc.perform(get("/api/game/chats/{gameId}", gameId)
                .accept(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isBadRequest());
    }


}
