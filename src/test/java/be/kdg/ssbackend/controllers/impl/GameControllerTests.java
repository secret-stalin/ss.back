package be.kdg.ssbackend.controllers.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.security.Principal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class GameControllerTests {

    private MockMvc mvc;

    @Autowired
    WebApplicationContext wac;

    @Before
    public void setup(){
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

    }

    @Test
    @Transactional
    public void createGameTest() throws Exception{
        mvc.perform(get("/api/game").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    @Transactional
    public void getGameStatisticWithoutAccess() throws Exception{
        mvc.perform(get("/api/game/stats/1")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    public void getGameStatisticWithInvalidToken() throws Exception{
        Principal mockPrincipal = Mockito.mock(Principal.class);

        Mockito.when(mockPrincipal.getName()).thenReturn("fakeUser@fakeuser.mock");

        mvc.perform(get("/api/game/stats/1")
                .accept(MediaType.APPLICATION_JSON)
                .principal(mockPrincipal))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    public void getGameStatisticInvalidGame() throws Exception{
        Principal mockPrincipal = Mockito.mock(Principal.class);

        Mockito.when(mockPrincipal.getName()).thenReturn("test@mail.com");

        mvc.perform(get("/api/game/stats/365568")
                .accept(MediaType.APPLICATION_JSON)
                .principal(mockPrincipal))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void getGameStatistic() throws Exception {

        Principal mockPrincipal = Mockito.mock(Principal.class);

        Mockito.when(mockPrincipal.getName()).thenReturn("test@mail.com");

        mvc.perform(get("/api/game/stats/")
                .accept(MediaType.APPLICATION_JSON)
                .principal(mockPrincipal))
                .andDo(print())
                //.andExpect(status().isOk());
                // TODO mock game
                .andExpect(status().isBadRequest());
    }
}
