package be.kdg.ssbackend.controllers.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.security.Principal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest
@RunWith(SpringRunner.class)
@Transactional
public class NotificationControllerTest {


    private MockMvc mvc;
    private ObjectMapper mapper;

    @Autowired
    WebApplicationContext wac;

    Principal principal;

    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(wac).build();
        this.mapper = new ObjectMapper();
        this.mapper.configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS, false);
        this.mapper.registerModule(new JSR310Module());
        this.principal = Mockito.mock(Principal.class);
        Mockito.when(principal.getName()).thenReturn("test@mail.com");
    }

    @Test
    public void getNotifications() throws Exception {
        mvc.perform(get("/api/notifications").accept(MediaType.APPLICATION_JSON).principal(principal))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getNotificationCount() throws Exception {
        mvc.perform(get("/api/notifications/count").accept(MediaType.APPLICATION_JSON).principal(principal))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void readNotification() throws Exception {
        mvc.perform(post("/api/notification/{notificationId}", 1).accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .principal(this.principal)
                .content(""))
                .andDo(print())
                .andExpect(status().isOk());
    }


}
