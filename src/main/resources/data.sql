INSERT INTO card VALUES
(1, 'SLAV', FALSE, 'https://cdn.discordapp.com/attachments/389129246259281922/553562829974929428/SlavCardSingle.png',0),
(2, 'STALIN', TRUE, 'https://cdn.discordapp.com/attachments/389129246259281922/553562827646828554/StalinCardSingle.png',1),
(3, 'HUNTER', FALSE, 'https://cdn.discordapp.com/attachments/389129246259281922/556778410983292949/HunterCardSingle.png',2),
(4, 'MAYOR', FALSE, 'https://cdn.discordapp.com/attachments/389129246259281922/556778433846312971/MayorCardSingle.png',3);
INSERT INTO game VALUES (1, 0 ,TRUE, '', 0, null);

INSERT INTO user VALUES (1, null,'test@mail.com', TRUE, '$2a$10$gJrJAj/.Z.abKGX8VfDg9Olc/ENnVbnnijIa4u.R9wcxtJrclDU3S', '', 'testUser');
INSERT INTO user VALUES (2, null,'test2@mail.com', TRUE, '$2a$10$gJrJAj/.Z.abKGX8VfDg9Olc/ENnVbnnijIa4u.R9wcxtJrclDU3S', '', 'RobbyDirix');
INSERT INTO user VALUES (3, null,'test3@mail.com', TRUE, '$2a$10$gJrJAj/.Z.abKGX8VfDg9Olc/ENnVbnnijIa4u.R9wcxtJrclDU3S', '', 'MichielBernaerts');
INSERT INTO user VALUES (4, null,'test4@mail.com', TRUE, '$2a$10$gJrJAj/.Z.abKGX8VfDg9Olc/ENnVbnnijIa4u.R9wcxtJrclDU3S', '', 'JensDeLaet');
INSERT INTO user VALUES (5, null,'test5@mail.com', TRUE, '$2a$10$gJrJAj/.Z.abKGX8VfDg9Olc/ENnVbnnijIa4u.R9wcxtJrclDU3S', '', 'ErikVanHoolst');
INSERT INTO user VALUES (6, null,'test6@mail.com', TRUE, '$2a$10$gJrJAj/.Z.abKGX8VfDg9Olc/ENnVbnnijIa4u.R9wcxtJrclDU3S', '', 'ArendDeGeest');
INSERT INTO user VALUES (7, null,'test7@mail.com', TRUE, '$2a$10$gJrJAj/.Z.abKGX8VfDg9Olc/ENnVbnnijIa4u.R9wcxtJrclDU3S', '', 'KoenLuyten');

INSERT INTO chat VALUES ('PRIVATE_CHAT', 1, null ,'chat1', null);

INSERT INTO user_chat VALUES (1, 1);
INSERT INTO user_chat VALUES (1, 2);

INSERT INTO user_friend VALUES (1, 2);
INSERT INTO user_friend VALUES (2, 1);


INSERT INTO lobby VALUES (4, false, true, 4, 1, 1);

