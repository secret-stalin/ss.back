package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.ChatDTO;
import be.kdg.ssbackend.dto.MessageDTO;
import be.kdg.ssbackend.exceptions.NoChatAccessException;
import be.kdg.ssbackend.model.chat.Chat;
import be.kdg.ssbackend.model.chat.Message;
import be.kdg.ssbackend.model.chat.PrivateChat;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.ChatRepository;
import be.kdg.ssbackend.repositories.api.PrivateChatRepository;
import be.kdg.ssbackend.services.api.ChatService;
import be.kdg.ssbackend.services.api.MessageService;
import be.kdg.ssbackend.services.api.UserService;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ChatServiceImpl implements ChatService {

    private final PrivateChatRepository privateChatRepository;
    private final ChatRepository chatRepository;
    private final UserService userService;
    private final MessageService messageService;
    private final SimpMessagingTemplate template;

    public ChatServiceImpl(ChatRepository chatRepository, UserService userService, MessageService messageService, PrivateChatRepository privateChatRepository, SimpMessagingTemplate template) {
        this.chatRepository = chatRepository;
        this.userService = userService;
        this.messageService = messageService;
        this.privateChatRepository = privateChatRepository;
        this.template = template;
    }

    @Override
    public List<Message> getMessagesFromChat(int chatId) {
        return messageService.getMessagesFromChat(chatId);
    }

    @Override
    public List<String> getEmailsOfParticipants(int chatId) {
        PrivateChat privateChat = privateChatRepository.getPrivateChatByChatId(chatId);
        return privateChat.getUsers().stream().map(User::getEmail).collect(Collectors.toList());
    }

    /**
     * Add message to specific chat
     *
     * @param chatId     -> id from the destination
     * @param messageDTO -> message that needs to be added
     * @param email      -> e-mail from the sender
     */
    @Override
    public void addMessage(int chatId, MessageDTO messageDTO, String email) {
        Chat chat = chatRepository.getChatByChatId(chatId);
        Message message = messageService.DTOToMessage(messageDTO, email, chat);
        messageService.save(message);
    }

    /**
     * Send a message to a user by giving the chatId from the chat with that user.
     *
     * @param chatId      -> id from the destination
     * @param message     -> message that needs to be send (sender will be added)
     * @param emailSender -> e-mail from the sender
     * @throws NoChatAccessException -> If the user doesn't have access to this chat the message won't be send and a NoChatAccessException will be thrown
     */
    @Override
    public void sendMessage(String emailSender, MessageDTO message, int chatId) {
        User user = userService.findByEmail(emailSender);
        List<String> emailsOfUsers = this.getEmailsOfParticipants(chatId);
        if (emailsOfUsers.contains(emailSender)) {
            this.addMessage(chatId, message, emailSender);
            message.setText(user.getUsername() + ": " + message.getText());
            message.setTimestamp(LocalDateTime.now().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
            for (String emailsOfUser : emailsOfUsers) {
                this.template.convertAndSendToUser(emailsOfUser, "/chat/" + chatId, message);
            }
        } else {
            throw new NoChatAccessException("You do not have permission to access this chat");
        }
    }

    /**
     * Load the message from a chat.
     *
     * @param chatId -> id from the chat where you want messages from
     * @param email  -> e-mail for controlling access to the chat
     * @throws NoChatAccessException -> If the user doesn't have access a NoChatAccessException will be thrown
     */
    @Override
    public List<MessageDTO> getMessagesFromChat(String email, int chatId) {
        List<MessageDTO> messagesDTO = new ArrayList<>();
        List<Message> messages;
        if (this.getEmailsOfParticipants(chatId).contains(email)) {
            messages = this.getMessagesFromChat(chatId);
            for (Message message : messages) {
                MessageDTO messageDTO = messageService.MessageToDTO(message, message.getSender().getEmail());
                messagesDTO.add(messageDTO);
            }
        } else {
            throw new NoChatAccessException("Ya sneaky little bastard, trying to spy on innocent people!");
        }

        return messagesDTO;
    }

    /**
     * Loads all the private chats from a user with the last message.
     * If there are no message yet a default message will be added
     *
     * @param email -> e-mail from the user
     */
    @Override
    public List<ChatDTO> getChatsFromUser(String email) {
        List<PrivateChat> privateChats = userService.getChatsFromUser(email);
        List<ChatDTO> chats = new ArrayList<>();

        for (PrivateChat privateChat : privateChats) {
            ChatDTO chatDTO = new ChatDTO();
            chatDTO.setChatId(String.valueOf(privateChat.getChatId()));
            Message lastMessage;
            if (privateChat.getMessages().size() > 0) {
                List<Message> messages = new ArrayList<>(privateChat.getMessages());
                messages.sort(Comparator.comparing(Message::getTime));
                lastMessage = messages.get(messages.size() - 1);
            } else {
                lastMessage = new Message();
                lastMessage.setText("You should say hi to your friend");
                lastMessage.setTime(LocalDateTime.now());
            }
            chatDTO.setLastMessage(lastMessage.getText());
            chatDTO.setLastMessageTime(lastMessage.getTime().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
            Optional<User> user = privateChat.getUsers().stream().filter(u -> !u.getEmail().equals(email)).findFirst();
            user.ifPresent(user1 -> chatDTO.setReceiver(user1.getUsername()));
            chats.add(chatDTO);
        }
        return chats;
    }

    @Override
    public void createPrivateChat(User requestAcceptor, User sender) {
        PrivateChat chat = new PrivateChat();
        chat.getUsers().add(requestAcceptor);
        chat.getUsers().add(sender);
        chatRepository.save(chat);
    }
}
