package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.GameChatDTO;
import be.kdg.ssbackend.dto.MessageDTO;
import be.kdg.ssbackend.exceptions.ObjectNotFoundException;
import be.kdg.ssbackend.model.chat.GameChat;
import be.kdg.ssbackend.model.chat.Message;
import be.kdg.ssbackend.model.game.Game;
import be.kdg.ssbackend.model.game.Participation;
import be.kdg.ssbackend.model.game.Role;
import be.kdg.ssbackend.model.game.RoleType;
import be.kdg.ssbackend.repositories.api.GameChatRepository;
import be.kdg.ssbackend.services.api.GameChatService;
import be.kdg.ssbackend.services.api.GameService;
import be.kdg.ssbackend.services.api.MessageService;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GameChatServiceImpl implements GameChatService {
    private final GameChatRepository gameChatRepository;
    private final MessageService messageService;
    private final GameService gameService;
    private final SimpMessagingTemplate template;

    public GameChatServiceImpl(GameChatRepository gameChatRepository, MessageService messageService, GameService gameService, SimpMessagingTemplate template) {
        this.gameChatRepository = gameChatRepository;
        this.messageService = messageService;
        this.gameService = gameService;
        this.template = template;
    }

    /**
     * Get the different chats from a game. Every chat includes all the messages
     *
     * @param email  -> e-mail from the user willing to get the chats
     * @param gameId -> id from the game the chats are from
     * @throws ObjectNotFoundException -> If a participation can't be found for the user, an ObjectNotFoundException will be thrown
     */
    @Override
    public List<GameChatDTO> getChatsFromGame(String email, int gameId) {
        List<GameChatDTO> chatDTOS = new ArrayList<>();
        Game game = gameService.getGame(gameId);
        List<GameChat> gameChats = gameChatRepository.getGameChatsByGameGameId(gameId);
        Optional<Participation> participation = game.getParticipations().stream().filter(p -> p.getUser().getEmail().equals(email)).findFirst();
        if (participation.isPresent()) {
            Set<Role> roleTypeList = participation.get().getRoles();
            for (Role role : roleTypeList) {
                RoleType roleType = role.getCard().getRoleType();
                for (GameChat gameChat : gameChats) {
                    if (gameChat.getAccess().toString().equals(roleType.toString())) {
                        List<MessageDTO> messageDTOS = new ArrayList<>();
                        Set<Message> messageSet = gameChat.getMessages();
                        List<Message> messages = new ArrayList<>(messageSet);
                        messages.sort(Comparator.comparing(Message::getTime));
                        for (Message message : messages) {
                            messageDTOS.add(messageService.MessageToDTO(message, message.getSender().getEmail()));
                        }
                        chatDTOS.add(new GameChatDTO(gameChat.getChatId(), gameChat.getName(), messageDTOS));
                    }
                }
            }

        } else {
            throw new ObjectNotFoundException("Participation not found");
        }
        return chatDTOS;
    }

    /**
     * Send a message to a specific chat from a game
     *
     * @param chatId  -> id from the destination
     * @param message -> message that needs to be send (sender will be added)
     * @param name    -> name from the sender
     * @throws ObjectNotFoundException -> If a participation can't be found for the user, an ObjectNotFoundException will be thrown
     * @throws ObjectNotFoundException -> If the GameChat can't be found in the database, an ObjectNotFoundException will be thrown
     */
    @Override
    public void sendMessage(int chatId, MessageDTO message, String name) {
        try {
            GameChat gameChat = gameChatRepository.getGameChatByChatId(chatId);
            Game game = gameChat.getGame();
            Optional<Participation> participation = game.getParticipations().stream().filter(p -> p.getUser().getEmail().equals(name)).findFirst();
            if (participation.isPresent()) {
                Set<Role> roleTypeList = participation.get().getRoles();
                for (Role role : roleTypeList) {
                    RoleType roleType = role.getCard().getRoleType();
                    if (gameChat.getAccess().toString().equals(roleType.toString())) {
                        this.addMessageToGameChat(gameChat, message, name);
                        message.setText(participation.get().getUser().getUsername() + ": " + message.getText());
                        this.template.convertAndSend("/gamechat/" + chatId, message);
                    }
                }
            } else {
                throw new ObjectNotFoundException("Participation not found");
            }
        } catch (org.hibernate.ObjectNotFoundException e) {
            throw new ObjectNotFoundException("GameChat not found");
        }
    }

    @Override
    public void addMessageToGameChat(GameChat gameChat, MessageDTO messageDTO, String emailSender) {
        Message message = messageService.DTOToMessage(messageDTO, emailSender, gameChat);
        messageService.save(message);
    }

    @Override
    public List<Message> getMessagesFromChat(int chatId) {
        return messageService.getMessagesFromChat(chatId);
    }
}
