package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.GameSettingsDTO;
import be.kdg.ssbackend.dto.LobbyDTO;
import be.kdg.ssbackend.dto.UserDTO;
import be.kdg.ssbackend.exceptions.GameNotFoundException;
import be.kdg.ssbackend.exceptions.LobbyFullException;
import be.kdg.ssbackend.exceptions.LobbyNotFoundException;
import be.kdg.ssbackend.exceptions.NotLobbyOwnerException;
import be.kdg.ssbackend.model.chat.GameChat;
import be.kdg.ssbackend.model.game.*;
import be.kdg.ssbackend.model.user.NotificationType;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.CardRepository;
import be.kdg.ssbackend.repositories.api.InvitationRepository;
import be.kdg.ssbackend.repositories.api.LobbyRepository;
import be.kdg.ssbackend.services.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class LobbyServiceImpl implements LobbyService {

    private final GameService gameService;
    private final CardRepository cardRepo;
    private final LobbyRepository lobbyRepo;
    private final UserService userService;
    private final InvitationRepository invitationRepo;
    private final SimpMessagingTemplate template;
    private final PhaseService phaseService;
    private final NotificationService notificationService;

    private static final Logger LOGGER = LoggerFactory.getLogger(LobbyServiceImpl.class);

    @Autowired
    private JavaMailSender mailSender;


    @Autowired
    public LobbyServiceImpl(GameService gameService, CardRepository cardRepo, LobbyRepository lobbyRepo, UserService userService,
                            InvitationRepository invitationRepo, SimpMessagingTemplate template,
                            PhaseService phaseService, NotificationService notificationService) {
        this.gameService = gameService;
        this.cardRepo = cardRepo;
        this.lobbyRepo = lobbyRepo;
        this.userService = userService;
        this.invitationRepo = invitationRepo;
        this.template = template;
        this.phaseService = phaseService;
        this.notificationService = notificationService;
    }

    /**
     * creates a lobby with the supplied settings for a given game, saves it and returns a DTO for the created lobby object
     *
     * @param gameId          -> the gameId for which a lobby should be
     * @param gameSettingsDTO -> a DTO object containing the settings for a game
     * @param hostMail        -> the emailadres of the use who created the lobby, this user immediately joins the lobby
     */
    @Override
    public LobbyDTO createLobby(int gameId, GameSettingsDTO gameSettingsDTO, String hostMail) {

        GameSettings settings = new GameSettings();
        settings.setName(gameSettingsDTO.get_gameName());
        settings.setDay(Duration.ofMinutes(gameSettingsDTO.get_dayTime()));

        settings.setNight(Duration.ofMinutes(gameSettingsDTO.get_nightTime()));
        settings.setPrivate(gameSettingsDTO.is_isPrivate());

        settings.setSize(gameSettingsDTO.get_maxPlayers());

        List<RoleType> rolesDTO = new ArrayList<>();
        for (String role : gameSettingsDTO.get_roles()) {
            rolesDTO.add(RoleType.valueOf(role));
        }
        settings.setRoles(rolesDTO);
        Lobby lobby = new Lobby(settings.isPrivate(), settings.getSize());
        List<RoleType> roles = settings.getRoles();
        List<Card> cards = cardRepo.findAllByRoleTypeIn(roles);
        lobby.setRolesIncluded(cards);
        Game game = gameService.getGame(gameId);
        game.setName(settings.getName());
        game.setDayDuration(settings.getDay());
        game.setNightDuration(settings.getNight());
        gameService.saveGame(game);
        lobby.setGame(game);

        User host = userService.findByEmail(hostMail);
        lobby.setCreatedBy(host);
        lobby.getJoinedUsers().add(host);
        Lobby l = lobbyRepo.save(lobby);
        return this.lobbyToDTO(l);

    }

    /**
     * Retrieves a lobby for a given lobbyId, this is a public wrapper method for the private fetchLobby, which does exception handling
     *
     * @param lobbyId -> the id of the lobby which should be retrieved
     */
    @Override
    public LobbyDTO getLobby(int lobbyId) {
        Lobby lobby = this.fetchLobby(lobbyId);
        return this.lobbyToDTO(lobby);
    }


    /**
     * Starts the game of a given lobby, setting up chats and dividing player cards
     *
     * @param lobbyId -> a token that has been send in a registration confirmation e-mail
     * @throws GameNotFoundException -> if there is no game associated to the given lobbyId
     */
    @Override
    public int startLobby(int lobbyId, String user) throws GameNotFoundException {
        Lobby lobby = fetchLobby(lobbyId);
        if (!lobby.isClosed()) {
            if (!lobby.isPrivate() && !lobby.getCreatedBy().getEmail().equalsIgnoreCase(user)) {
                throw new NotLobbyOwnerException(String.format("User :%s is not the owner of lobby %d", user, lobbyId));
            }
            Game game = lobby.getGame();
            List<User> users = new ArrayList<>(lobby.getJoinedUsers());
            game.setParticipations(divideRoles(users, game));
            game.getGameChats().addAll(setupChats(game));
            phaseService.addPhase(game.getGameId());
            gameService.saveGame(game);
            lobby.setClosed(true);
            lobbyRepo.save(lobby);
            startGame(lobbyId, game.getGameId());
            return game.getGameId();
        } else {
            throw new GameNotFoundException("Game has already started");
        }
    }

    /**
     * private getter method for a lobby which does exception handling
     *
     * @param lobbyId -> id of the lobby to retrieve
     * @throws LobbyNotFoundException -> if there is no lobby for the given lobbyId
     */
    private Lobby fetchLobby(int lobbyId) throws LobbyNotFoundException {
        Lobby lobby;
        if (lobbyRepo.findById(lobbyId).isPresent()) {
            lobby = lobbyRepo.findById(lobbyId).get();
        } else {
            throw new LobbyNotFoundException(String.format("Lobby %d not found", lobbyId));
        }
        return lobby;
    }

    /**
     * private helper method for setting up the participations for a given game, also divides the different roles for that game
     *
     * @param users -> list of the users who are going to participate in the game
     * @param game  -> game for which the participations are being setup
     */
    private Set<Participation> divideRoles(List<User> users, Game game) {
        String[] colorCodes = {"#8c2c75", "#c9ab07", "#ab3223", "#36dd9b",
                "#801df2", "#75af36", "#1e6056", "#106307", "#656635", "#5c5005", "#285ad9",
                "#fc7145", "#518b3e", "#f82edf", "#cf4844", "#ff4612"};
        List<String> colorList = new ArrayList<>(Arrays.asList(colorCodes));
        Card slav = cardRepo.findByRoleType(RoleType.SLAV);
        Card stalin = cardRepo.findByRoleType(RoleType.STALIN);
        Card hunter = cardRepo.findByRoleType(RoleType.HUNTER);
        Card mayor = cardRepo.findByRoleType(RoleType.MAYOR);
        Random rand = new Random();
        List<Participation> participations = new ArrayList<>();
        for (User u : users) {
            Participation part = new Participation();

            int listIndex = rand.nextInt(colorList.size());
            part.setColorCode(colorList.get(listIndex));
            colorList.remove(listIndex);

            part.setGame(game);
            part.setUser(u);
            Role role = new Role(part, slav);
            part.getRoles().add(role);
            participations.add(part);
        }
        int numberOfStalins = users.size() / 5 == 0 ? 1 : users.size() / 5;
        List<Integer> chosenStalins = new ArrayList<>();


        do {
            int possibleStalin = rand.nextInt(users.size());
            if (!chosenStalins.contains(possibleStalin)) {
                chosenStalins.add(possibleStalin);
                Participation chosenStalin = participations.get(possibleStalin);
                chosenStalin.getRoles().add(new Role(chosenStalin, stalin));
            }
        }
        while (chosenStalins.size() < numberOfStalins);

        if (users.size() >= 3) {
            boolean hunterFound = false;
            do {
                int chosenHunter = rand.nextInt(users.size());
                if (!chosenStalins.contains(chosenHunter)) {
                    chosenStalins.add(chosenHunter);
                    Participation chosenStalin = participations.get(chosenHunter);
                    chosenStalin.getRoles().add(new Role(chosenStalin, hunter));
                    hunterFound = true;
                }
            } while (!hunterFound);
        }

        if (users.size() >= 4) {
            boolean mayorFound = false;
            do {
                int chosenMayor = rand.nextInt(users.size());
                if (!chosenStalins.contains(chosenMayor)) {
                    mayorFound = true;
                    chosenStalins.add(chosenMayor);
                    Participation chosenStalin = participations.get(chosenMayor);
                    chosenStalin.getRoles().add(new Role(chosenStalin, mayor));
                }
            } while (!mayorFound);
        }
        return new HashSet<>(participations);
    }

    /**
     * private helper method for setting up the chats for a given game
     *
     * @param game -> game for which the chats are being setup
     */
    private List<GameChat> setupChats(Game game) {
        List<GameChat> chats = new ArrayList<>();

        chats.add(new GameChat("general", RoleType.SLAV, game));

        chats.add(new GameChat("stalin", RoleType.STALIN, game));
        return chats;
    }

    /**
     * sends an invitation to a user for the given lobby
     *
     * @param lobbyId -> lobby for which the information is send
     * @param email   -> email adress of the user that is being invited
     */
    @Override
    public void createInvitation(int lobbyId, String email) {
        User user = userService.findByEmail(email);
        Lobby lobby = this.fetchLobby(lobbyId);
        Invitation invitation = new Invitation(lobby, user);
        invitationRepo.save(invitation);
        sendInvitationByMail(invitation);
        notificationService.addNotification(invitation.getInvitationId(), NotificationType.INVITE, "You have been invited to join a lobby.", email);
    }

    /**
     * private helper method to send out an email for a given invitation
     *
     * @param invitation -> invitation fo which an email should be send
     */
    // TODO refactor to util + link out of app prop
    private void sendInvitationByMail(Invitation invitation) {
        User user = invitation.getInvitee();
        String confirmationUrl = "https://secret-stalin-backend.herokuapp.com/api/accepted/" + invitation.getInvitationId();

        String recipientAddress = user.getEmail();
        String subject = "Secret Stalin Invitation";
        String message = "U have been invited to a lobby to accept click the following link: ";

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + " \r\n" + confirmationUrl);
        mailSender.send(email);
    }

    /**
     * method to accept a given invitation, adds the invitee to the joined users of the lobby for which the invitation was send
     *
     * @param invitationId -> id of the invitation which
     */
    @Override
    public Lobby acceptedInvitation(int invitationId) {
        Invitation invitation = invitationRepo.findByInvitationId(invitationId);
        Lobby lobby = invitation.getLobby();
        User user = invitation.getInvitee();

        invitation.setAccepted(true);

        if (!lobby.getJoinedUsers().contains(user)) {
            if (lobby.getSize() >= lobby.getJoinedUsers().size()) {
                lobby.getJoinedUsers().add(user);
                lobbyRepo.save(lobby);
            } else {
                throw new LobbyFullException(String.format("Lobby %d is already full", lobby.getLobbyId()));
            }
        }
        invitationRepo.save(invitation);
        this.updateJoinedUsers(lobby.getLobbyId());

        return lobby;
    }

    /**
     * method to get the list of joined users for a given lobby
     *
     * @param lobbyId -> id of the lobby for which to retrieve the joined users
     */
    @Override
    public List<UserDTO> getJoined(int lobbyId) {
        List<UserDTO> dtos = new ArrayList<>();
        Lobby lobby = this.fetchLobby(lobbyId);
        for (User joinedUser : lobby.getJoinedUsers()) {
            UserDTO userDTO = new UserDTO();
            userDTO.setEmail(joinedUser.getEmail());
            userDTO.setUsername(joinedUser.getUsername());
            dtos.add(userDTO);
        }
        return dtos;
    }


    @Override
    public List<LobbyDTO> getPublicLobbies() {
        List<Lobby> lobbies = lobbyRepo.getAllByIsPrivate(false);
        return lobbies.stream().filter(l -> l.getJoinedUsers().size() < l.getSize()).filter(l -> !l.isClosed()).map(this::lobbyToDTO).collect(Collectors.toList());
    }

    @Override
    public Lobby joinPublicLobby(int lobbyId, String userMail) {
        Lobby lobby = this.fetchLobby(lobbyId);
        User joinedUser = userService.findByEmail(userMail);
        if (lobby.getJoinedUsers().size() < lobby.getSize()) {
            lobby.getJoinedUsers().add(joinedUser);
            updateJoinedUsers(lobbyId);
        } else {
            throw new LobbyFullException(String.format("Lobby %d is already full", lobby.getLobbyId()));
        }
        return lobbyRepo.save(lobby);
    }

    @Override
    public List<LobbyDTO> getActiveLobbies(String userMail) {
        User user = userService.findByEmail(userMail);
        List<Lobby> lobbies = lobbyRepo.getAllByJoinedUsersContains(user);
        return lobbies.stream().filter(l -> !l.isClosed()).map(this::lobbyToDTO).collect(Collectors.toList());
    }

    private void updateJoinedUsers(int lobbyId) {
        List<UserDTO> dtoList = new ArrayList<>();
        if (lobbyRepo.findById(lobbyId).isPresent()) {
            Lobby lobby = this.fetchLobby(lobbyId);
            for (User joinedUser : lobby.getJoinedUsers()) {
                UserDTO userDTO = new UserDTO();
                userDTO.setUsername(joinedUser.getUsername());
                userDTO.setEmail(joinedUser.getEmail());
                dtoList.add(userDTO);
            }
            this.template.convertAndSend("/lobby/" + lobbyId, dtoList);
        } else {
            throw new LobbyNotFoundException(String.format("Lobby %d not found", lobbyId));
        }

    }

    private void startGame(int lobbyId, int gameId) {
        this.template.convertAndSend("/lobby/start/" + lobbyId, gameId);
    }

    /**
     * helper method to easily convert a Lobby into a LobbyDTO
     *
     * @param lobby -> Lobby object to be converted to a LobbyDTO
     */
    public LobbyDTO lobbyToDTO(Lobby lobby) {
        LobbyDTO dto = new LobbyDTO();
        dto.setLobbyId(lobby.getLobbyId());
        if (lobby.getJoinedUsers() != null) {
            dto.setUsers(lobby.getJoinedUsers().stream().map(user -> userService.userToDTO(user)).collect(Collectors.toList()));
        }
        GameSettingsDTO gsDTO = new GameSettingsDTO();
        if (lobby.getGame() != null) {
            Game g = lobby.getGame();
            gsDTO.set_gameId(g.getGameId());
            gsDTO.set_gameName(g.getName());
            gsDTO.set_dayTime((int) g.getDayDuration().toMinutes());
            gsDTO.set_nightTime((int) g.getNightDuration().toMinutes());
        }
        if (lobby.getRolesIncluded() != null) {
            gsDTO.set_roles(lobby.getRolesIncluded().stream().map(c -> c.getRoleType().name()).collect(Collectors.toList()));
        }
        gsDTO.set_isPrivate(lobby.isPrivate());
        gsDTO.set_maxPlayers(lobby.getSize());
        dto.setGameSettings(gsDTO);
        return dto;
    }

    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }
}
