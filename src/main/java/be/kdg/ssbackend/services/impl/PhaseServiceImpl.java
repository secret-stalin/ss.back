package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.ActionDTO;
import be.kdg.ssbackend.dto.VoteResultDTO;
import be.kdg.ssbackend.exceptions.ParticipationNotFoundException;
import be.kdg.ssbackend.model.game.*;
import be.kdg.ssbackend.model.user.NotificationType;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.ActionRepository;
import be.kdg.ssbackend.repositories.api.CardRepository;
import be.kdg.ssbackend.repositories.api.ParticipationRepository;
import be.kdg.ssbackend.repositories.api.PhaseRepository;
import be.kdg.ssbackend.services.api.GameService;
import be.kdg.ssbackend.services.api.NotificationService;
import be.kdg.ssbackend.services.api.PhaseService;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PhaseServiceImpl implements PhaseService {

    private final PhaseRepository phaseRepository;
    private final SimpMessagingTemplate template;
    private final ParticipationRepository participationRepository;
    private final CardRepository cardRepository;
    private final GameService gameService;
    private final ActionRepository actionRepository;
    private final NotificationService notificationService;

    public PhaseServiceImpl(PhaseRepository phaseRepository, SimpMessagingTemplate template, ParticipationRepository participationRepository, CardRepository cardRepository, GameService gameService, ActionRepository actionRepository, NotificationService notificationService) {
        this.phaseRepository = phaseRepository;
        this.template = template;
        this.participationRepository = participationRepository;
        this.cardRepository = cardRepository;
        this.gameService = gameService;
        this.actionRepository = actionRepository;
        this.notificationService = notificationService;
    }

    /**
     * Starts a new phase for a specific game
     * Will check on the previous phase to determine who has to vote in the new phase
     * After the new phase is created a timer with a task will be created to end phase after a period of time
     * If the game is no longer active then no new phase will start
     * If this is the first phase, begin appropriately
     *
     * @param gameId -> id from the game where a new phase should start
     */
    @Override
    public void addPhase(int gameId) {
        Game game = gameService.getGame(gameId);
        if (game.isActive()) {
            RoleType roleToVote = RoleType.SLAV;
            State state = State.DAY;
            Duration duration = game.getDayDuration();
            if (game.getPhases().size() > 0) {
                RoleType prevRoleTypeToVote = phaseRepository.findTopByGameOrderByPhaseNumberDesc(game).getRoleTypeVoter();

                List<RoleType> roleTypesInGame = getRoleTypesAliveInGame(game);

                boolean isInGame = false;
                do {
                    switch (prevRoleTypeToVote) {
                        case SLAV: {
                            roleToVote = RoleType.STALIN;
                            state = State.NIGHT;
                            duration = game.getNightDuration();
                        }
                        break;
                        case STALIN: {
                            roleToVote = RoleType.SLAV;
                            state = State.DAY;
                            duration = game.getDayDuration();
                        }
                        break;
                    }

                    isInGame = roleTypesInGame.contains(roleToVote);
                    if (!isInGame) {
                        prevRoleTypeToVote = roleToVote;
                    }
                } while (!isInGame);
            }

            Phase phase = new Phase();
            phase.setPhaseNumber(game.getPhases().size());
            phase.setRoleTypeVoter(roleToVote);
            phase.setGame(game);
            phase.setState(state);
            game.getPhases().add(phase);
            gameService.saveGame(game);

            Timer timer = new Timer();
            timer.schedule(new EndPhaseTask(this, gameId), Date.from(LocalDateTime.now().atOffset(ZoneOffset.systemDefault().getRules().getOffset(Instant.now())).plusSeconds(duration.getSeconds()).toInstant()));

            template.convertAndSend("/game/votes/" + gameId, true);
        }
    }

    private static class EndPhaseTask extends TimerTask {
        private final PhaseService phaseService;
        private final int gameId;

        private EndPhaseTask(PhaseService phaseService, int gameId) {
            this.phaseService = phaseService;
            this.gameId = gameId;
        }

        @Override
        public void run() {
            phaseService.endPhase(gameId);
        }
    }

    /**
     * Add action to a phase and participants
     * If it is not the executor his turn to vote or the executor is not alive or the affected is not alive, the action will not be persisted
     *
     * @param gameId         -> id of the game where the action needs to be added
     * @param executor       -> user that executes the action
     * @param partIdAffected -> participation id of the participant who is targeted by the executor's action
     */
    @Override
    public void addAction(int gameId, User executor, int partIdAffected, RoleType roleType) throws ParticipationNotFoundException {
        Game game = gameService.getGame(gameId);

        Participation partAffected = participationRepository.findByParticipationId(partIdAffected);
        Participation partExecutor = executor.getParticipations().stream().filter(p -> p.getGame().getGameId() == game.getGameId()).findFirst().orElse(null);
        if (partAffected != null) {
            switch (roleType) {

                case HUNTER:
                    Action actionHunter = new Action();
                    actionHunter.setActionType(ActionType.HUNTER_TARGET);
                    actionHunter.setExecutor(partExecutor);
                    actionHunter.setAffected(partAffected);
                    actionHunter.setTime(LocalDateTime.now());
                    actionHunter.setPhase(phaseRepository.findTopByGameOrderByPhaseNumberDesc(game));
                    actionRepository.save(actionHunter);
                    break;
                case MAYOR:
                    Action actionMayor = new Action();
                    actionMayor.setActionType(ActionType.MAYOR_CHOICE);
                    actionMayor.setExecutor(partExecutor);
                    actionMayor.setAffected(partAffected);
                    actionMayor.setTime(LocalDateTime.now());
                    actionMayor.setPhase(phaseRepository.findTopByGameOrderByPhaseNumberDesc(game));
                    actionRepository.save(actionMayor);
                    break;
                default: {
                    boolean isRoleToVote = canUserVote(game, partExecutor);

                    if (isRoleToVote && partExecutor.isAlive() && partAffected.isAlive()) {
                        Action action = new Action();
                        action.setActionType(ActionType.VOTE);
                        action.setExecutor(partExecutor);
                        action.setAffected(partAffected);
                        action.setTime(LocalDateTime.now());
                        action.setPhase(phaseRepository.findTopByGameOrderByPhaseNumberDesc(game));
                        actionRepository.save(action);

                        template.convertAndSend("/game/votes/" + gameId, true);
                    }
                }
            }
        } else {
            throw new ParticipationNotFoundException(String.format("Participation with id %d not found", partIdAffected));
        }
    }

    /**
     * End a phase accordingly
     * Determine if there is one winner of the vote results in the current phase
     * If there is one winner, then the winning participant will undergo an action, based of what role got to vote this phase
     * If there are multiple winners, then no actions will be done
     * At the end a new phase will be started
     *
     * @param gameId -> id of the game where the phase needs to be ended
     */
    @Override
    public void endPhase(int gameId) {
        Game game = gameService.getGame(gameId);
        RoleType roleTypeToVote = phaseRepository.findTopByGameOrderByPhaseNumberDesc(game).getRoleTypeVoter();

        List<VoteResultDTO> voteMap = voteCount(game);

        int maxVotes = 0;
        String affectedName = "";
        int doubles = 0;
        for (VoteResultDTO dto : voteMap) {
            if (dto.getVoteCount() > maxVotes) {
                maxVotes = dto.getVoteCount();
                affectedName = dto.getUsername();
                doubles = 0;
            } else if (dto.getVoteCount() == maxVotes) {
                doubles += 1;
            }
        }

        if (doubles == 0) {
            String finalAffectedName = affectedName;
            Participation affected = game.getParticipations().stream().filter(p -> p.getUser().getUsername().equals(finalAffectedName)).findFirst().orElse(null);
            Participation participationAffected = participationRepository.findByParticipationId(affected.getParticipationId());
            switch (roleTypeToVote) {
                case SLAV:
                case STALIN: {
                    if (affected != null) {
                        if (affected.getRoles().stream().anyMatch(r -> r.getCard().getRoleType().equals(RoleType.HUNTER))) {
                            List<Action> targets = affected.getActionsDone().stream().filter(a -> a.getActionType().equals(ActionType.HUNTER_TARGET)).sorted(Comparator.comparing(Action::getTime)).collect(Collectors.toList());
                            if (targets.size() > 0) {
                                Participation participationTarget = targets.get(targets.size() - 1).getAffected();
                                participationTarget.setAlive(false);
                                Action killHunter = new Action();
                                killHunter.setAffected(participationTarget);
                                killHunter.setActionType(ActionType.KILL);
                                killHunter.setTime(LocalDateTime.now());
                                killHunter.setPhase(phaseRepository.findTopByGameOrderByPhaseNumberDesc(game));
                                actionRepository.save(killHunter);
                            }
                        }
                        if (affected.getRoles().stream().anyMatch(r -> r.getCard().getRoleType().equals(RoleType.MAYOR))) {
                            List<Action> targets = affected.getActionsDone().stream().filter(a -> a.getActionType().equals(ActionType.MAYOR_CHOICE)).sorted(Comparator.comparing(Action::getTime)).collect(Collectors.toList());
                            if (targets.size() > 0) {
                                Participation participationTarget = targets.get(targets.size() - 1).getAffected();
                                Participation participationTarget2 = game.getParticipations().stream().filter(p -> p.getParticipationId() == participationTarget.getParticipationId()).findFirst().orElse(null);
                                Card mayorCard = cardRepository.findByRoleType(RoleType.MAYOR);
                                Role role = new Role(participationTarget2, mayorCard);
                                participationTarget2.getRoles().add(role);
                            }
                        }
                        affected.setAlive(false);
                        Action kill = new Action();
                        kill.setActionType(ActionType.KILL);
                        kill.setAffected(participationAffected);
                        kill.setTime(LocalDateTime.now());
                        kill.setPhase(phaseRepository.findTopByGameOrderByPhaseNumberDesc(game));
                        actionRepository.save(kill);
                    }
                }
            }
        }

        List<RoleType> roleTypesInGame = getRoleTypesAliveInGame(game);

        ActionDTO actionDTO = new ActionDTO(gameId, true, true);
        game.setActive(false);
        if (!roleTypesInGame.contains(RoleType.SLAV)) {
            actionDTO.setRoleTypeWinner(RoleType.STALIN);
            game.setWinningRole(RoleType.STALIN);
        } else if (!roleTypesInGame.contains(RoleType.STALIN)) {
            actionDTO.setRoleTypeWinner(RoleType.SLAV);
            game.setWinningRole(RoleType.SLAV);
        } else {
            game.setActive(true);
            actionDTO.setWin(false);
        }

        gameService.saveGame(game);

        if (game.isActive()) {
            for (Participation participation : game.getParticipations()) {
                notificationService.addNotification(
                        gameId,
                        NotificationType.ENDPHASE,
                        "A phase of " + game.getName() + " has ended!",
                        participation.getUser().getEmail());
            }
            this.addPhase(gameId);
        }

        template.convertAndSend("/game/" + gameId, actionDTO);
    }

    /**
     * Returns a list of VoteResultDTOs,
     * consisting of all participants of the specific game with the amount of votes against them
     *
     * @param gameId -> id from the game where we need the vote results
     */
    @Override
    public List<VoteResultDTO> getVoteResults(User user, int gameId) {
        List<VoteResultDTO> voteResultDTOs = new ArrayList<>();
        Game game = gameService.getGame(gameId);
        Participation partExecutor = user.getParticipations().stream().filter(p -> p.getGame().getGameId() == gameId).findFirst().orElse(null);

        if (canUserVote(game, partExecutor)) {
            voteResultDTOs = voteCount(game);
        }

        return voteResultDTOs;
    }

    /**
     * Determines if the executor (participant) can vote in the current phase
     *
     * @param game         -> current game
     * @param partExecutor -> executor to check whether the participant can vote
     * @return <code>true</code> if executor may vote
     * <code>false</code> if executor may not vote
     */
    private boolean canUserVote(Game game, Participation partExecutor) throws ParticipationNotFoundException {
        if (partExecutor != null) {
            RoleType roleTypeToVote = phaseRepository.findTopByGameOrderByPhaseNumberDesc(game).getRoleTypeVoter();
            boolean isRoleToVote = false;
            for (Role role : partExecutor.getRoles()) {
                if (role.getCard().getRoleType() == roleTypeToVote) {
                    isRoleToVote = true;
                }
            }

            return isRoleToVote;
        } else {
            throw new ParticipationNotFoundException("Participation of executor not found!");
        }
    }

    private List<RoleType> getRoleTypesAliveInGame(Game game) {
        List<RoleType> roleTypesInGame = new ArrayList<>();
        for (Participation part : game.getParticipations()) {
            if (part.isAlive()) {
                boolean isStalin = part.getRoles().stream()
                        .anyMatch(r -> r.getCard().getRoleType() == RoleType.STALIN);
                for (Role role : part.getRoles()) {
                    if (!(isStalin && role.getCard().getRoleType() == RoleType.SLAV)) {
                        roleTypesInGame.add(role.getCard().getRoleType());
                    }
                }
            }
        }

        return roleTypesInGame;
    }

    private List<VoteResultDTO> voteCount(Game game) {
        List<VoteResultDTO> voteResultDTOs = new ArrayList<>();
        Phase currentPhase = phaseRepository.findTopByGameOrderByPhaseNumberDesc(game);
        List<Participation> participations = new ArrayList<>(game.getParticipations());
        participations.sort(Comparator.comparingInt(Participation::getParticipationId));

        for (Participation participation : participations) {
            voteResultDTOs.add(new VoteResultDTO(participation.getUser().getUsername(), 0));
        }

        for (Participation participation : participations) {
            Action act = participation
                    .getActionsDone()
                    .stream()
                    .filter(a -> a.getPhase().getPhaseId() == currentPhase.getPhaseId())
                    .filter(a -> a.getActionType().equals(ActionType.VOTE))
                    .max(Comparator.comparing(Action::getTime))
                    .orElse(null);
            if (act != null) {
                VoteResultDTO vrDto = voteResultDTOs
                        .stream()
                        .filter(vr -> vr.getUsername().equals(act.getAffected().getUser().getUsername()))
                        .findFirst()
                        .orElse(null);

                if (participation.getRoles().stream().anyMatch(r -> r.getCard().getRoleType().equals(RoleType.MAYOR))) {
                    vrDto.setVoteCount(vrDto.getVoteCount() + 2);
                } else {
                    vrDto.setVoteCount(vrDto.getVoteCount() + 1);
                }
            }
        }
        return voteResultDTOs;
    }

}
