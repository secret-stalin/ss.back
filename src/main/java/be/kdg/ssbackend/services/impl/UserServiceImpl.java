package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.UserDTO;
import be.kdg.ssbackend.dto.UserStatDTO;
import be.kdg.ssbackend.exceptions.*;
import be.kdg.ssbackend.model.chat.PrivateChat;
import be.kdg.ssbackend.model.game.Game;
import be.kdg.ssbackend.model.game.Participation;
import be.kdg.ssbackend.model.game.RoleType;
import be.kdg.ssbackend.model.user.*;
import be.kdg.ssbackend.repositories.api.UserRepository;
import be.kdg.ssbackend.repositories.api.VerificationTokenRepository;
import be.kdg.ssbackend.services.api.ChatService;
import be.kdg.ssbackend.services.api.NotificationService;
import be.kdg.ssbackend.services.api.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final VerificationTokenRepository tokenRepository;
    private final NotificationService notificationService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ChatService chatService;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${url-confirmToken}")
    private String confirmationUrl;

    private final JavaMailSender mailSender;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, VerificationTokenRepository verificationTokenRepository, @Lazy NotificationService notificationService, BCryptPasswordEncoder bCryptPasswordEncoder, @Lazy ChatService chatService, JavaMailSender mailSender) {
        this.userRepository = userRepository;
        this.tokenRepository = verificationTokenRepository;
        this.notificationService = notificationService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.chatService = chatService;
        this.mailSender = mailSender;
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    /**
     * Create an account (default disabled) and send a registration email with confirmation link
     *
     * @param accountDto -> Information to create the account with
     * @throws EmailFormatException      -> If the e-mail doesn't match the regex an EmailFormatException will be thrown
     * @throws UserAlreadyExistException -> If the e-mail or username is already taken a UserAlreadyExistException will be thrown
     */
    @Override
    public void createUserAccount(UserDTO accountDto, boolean sendMail) {
        if (!checkEmailRegex(accountDto.getEmail())) {
            throw new EmailFormatException("Email not valid");
        } else if (userRepository.findByEmail(accountDto.getEmail()) != null) {
            throw new UserAlreadyExistException("Email already in use");
        } else if (userRepository.findByUsername(accountDto.getUsername()) != null) {
            throw new UserAlreadyExistException("Username already in use");
        } else {
            User user = new User();
            user.setPassword(bCryptPasswordEncoder.encode(accountDto.getNewPassword()));
            user.setEmail(accountDto.getEmail());
            if (accountDto.getUsername() == null || accountDto.getUsername().equals("")) {
                user.setUsername(generateUserName());
            } else {
                user.setUsername(accountDto.getUsername());
            }
            this.save(user);
            if (sendMail) {
                sendConfirmationMail(user);
            }
        }
    }

    /**
     * Send registration confirmation e-mail with secretstalingame@gmail.com
     *
     * @param user -> user where the e-mail needs to be send to
     */
    @Override
    public void sendConfirmationMail(User user) {
        String token = UUID.randomUUID().toString();
        VerificationToken verificationToken = createVerificationToken(user, token);

        String recipientAddress = user.getEmail();
        String subject = "Secret Stalin Registration Confirmation";
        String message = "Please confirm email by clicking following url: ";

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + " \r\n" + confirmationUrl + "" + verificationToken.getToken());
        mailSender.send(email);
    }

    /**
     * resend registration confirmation email with secretstalingame@gmail.com.
     * If the email isn't found in the users, no mail will be send
     *
     * @param email -> e-mail to send to
     */
    @Override
    public void resendVerificationMail(String email) {
        if (!checkEmailRegex(email)) {
            throw new EmailFormatException("Email not valid");
        } else if (userRepository.findByEmail(email) != null) {
            sendConfirmationMail(userRepository.findByEmail(email));
        }
    }

    /**
     * Check if the token is valid, then enable the user, allowing login
     *
     * @param verificationToken -> a token that has been send in a registration confirmation e-mail
     * @throws InvalidTokenException -> if the token doesn't exist or has expired an InvalidTokenException
     */
    public void checkToken(String verificationToken) {

        VerificationToken token = getVerificationToken(verificationToken);
        if (token == null) {
            throw new InvalidTokenException("Token doesn't exist!");
        }

        User user = token.getUser();
        Calendar cal = Calendar.getInstance();
        if ((token.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            throw new InvalidTokenException("Token has expired!");
        }
        user.setEnabled(true);
        save(user);
    }

    /**
     * Only used when the user leaves the username field empty. A random username will be generated.
     */
    public String generateUserName() {
        Random random = new Random();
        String username = "Player" + random.nextInt(5000);
        if (userRepository.findByUsername(username) != null) {
            username = generateUserName();
        }
        return username;

    }

    /**
     * Validate an e-mail format.
     *
     * @param email -> the e-mail that need to be validated
     */
    @Override
    public boolean checkEmailRegex(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public UserDTO userToDTO(User user) {
        UserDTO result = new UserDTO();
        result.setEmail(user.getEmail());
        result.setUsername(user.getUsername());
        result.setProfilePicture(user.getProfilePicture());
        return result;
    }

    @Override
    public VerificationToken getVerificationToken(String VerificationToken) {
        return tokenRepository.findByToken(VerificationToken);
    }

    /**
     * A verification token will be created. If the user already had a verification token the expiration date will be updated
     *
     * @param user  -> user where the e-mail needs to be send to
     * @param token -> a generated token that will be user in the VerificationToken
     */
    @Override
    public VerificationToken createVerificationToken(User user, String token) {
        VerificationToken t = tokenRepository.findByUser(user);
        if (t == null) {
            VerificationToken myToken = new VerificationToken(token, user);
            return tokenRepository.save(myToken);
        } else {
            t.setExpiryDate(t.calculateExpiryDate(VerificationToken.EXPIRATION));
            return tokenRepository.save(t);
        }
    }

    @Override
    public List<PrivateChat> getChatsFromUser(String email) {
        return userRepository.findByEmail(email).getChats();
    }

    @Override
    public List<User> findFriendsFromUser(String email) {
        User user = userRepository.findByEmail(email);
        return user.getFriends();
    }

    @Override
    public boolean isFriendFrom(String requestUserEmail, String username) {
        User requestUser = userRepository.findByEmail(requestUserEmail);
        for (User friend : requestUser.getFriends()) {
            if (friend.getUsername().toLowerCase().equals(username.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the user has a friend request from request sender if the flag inOwnNotifications is true.
     * If the flag is false there will be checked if you send a friend request to requestAcceptor (caller from api).
     * In case the notification doesn't exist false will be returned.
     *
     * @param requestAcceptorUserEmail -> Email from the user willing to accept a friend request
     * @param requestSenderUserName    -> username from the sender from the friend request
     * @param inOwnNotifications       -> flag that marks if there need to be checked in the notifications form the sender or the acceptor
     */
    @Override
    public boolean hasFriendRequestFromUser(String requestAcceptorUserEmail, String requestSenderUserName, boolean inOwnNotifications) {
        User requestSender = userRepository.findByEmail(requestAcceptorUserEmail);
        User requestAcceptor = userRepository.findByUsername(requestSenderUserName);
        if (inOwnNotifications) {
            for (Notification notification : getNotificationsFromUserOfType(requestSender, NotificationType.FRIEND_REQUEST)) {
                //Check if you got a friend request from acceptor
                if (notification.getTargetId() == requestAcceptor.getUserId()) {
                    return true;
                }
            }
        } else {
            for (Notification notification : getNotificationsFromUserOfType(requestAcceptor, NotificationType.FRIEND_REQUEST)) {
                //Check if you have sent an invitiation to acceptor
                if (notification.getTargetId() == requestSender.getUserId()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Send a friend request to a specific user.
     *
     * @param emailRequestSender -> email from the sender (caller from API)
     * @param userNameToFriend   -> username from user you want to add
     * @throws AlreadyFriendException -> if the user is already your friend an AlreadyFriendException will be thrown
     */
    @Override
    public void sendFriendRequest(String emailRequestSender, String userNameToFriend) {
        User requestSender = userRepository.findByEmail(emailRequestSender);
        User receiver = userRepository.findByUsername(userNameToFriend);
        if (!this.isFriendFrom(emailRequestSender, userNameToFriend)) {
            notificationService.addNotification(requestSender.getUserId(), NotificationType.FRIEND_REQUEST, requestSender.getUsername() + " send you a friend request.", receiver.getEmail());
        } else {
            throw new AlreadyFriendException("Already friends with " + userNameToFriend);
        }

    }

    /**
     * Accept a friend request only if you have a friend request from that user.
     * If you don't have a friend request, the call will be ignored.
     *
     * @param emailFromAcceptor -> e-mail from the acceptor (caller from API)
     * @param userNameSender    -> username from user that send the friend request
     */
    @Override
    public void acceptFriendRequest(String emailFromAcceptor, String userNameSender) {
        User requestAcceptor = userRepository.findByEmail(emailFromAcceptor);
        User sender = userRepository.findByUsername(userNameSender);
        if (this.hasFriendRequestFromUser(requestAcceptor.getEmail(), sender.getUsername(), true)) {
            requestAcceptor.getFriends().add(sender);
            userRepository.save(requestAcceptor);
            sender.getFriends().add(requestAcceptor);
            userRepository.save(sender);
            this.removeFriendRequestNotificationFromUser(requestAcceptor, sender.getUserId());
            chatService.createPrivateChat(requestAcceptor, sender);
        }
    }

    /**
     * Decline a friend request only if you have a friend request from that user.
     * If you don't have a friend request, the call will be ignored.
     *
     * @param emailFromAcceptor -> e-mail from the acceptor (caller from API)
     * @param userNameSender    -> username from user that send the friend request
     */
    @Override
    public void declineFriendRequest(String emailFromAcceptor, String userNameSender) {
        User requestAcceptor = userRepository.findByEmail(emailFromAcceptor);
        User sender = userRepository.findByUsername(userNameSender);
        if (this.hasFriendRequestFromUser(requestAcceptor.getEmail(), sender.getUsername(), true)) {
            this.removeFriendRequestNotificationFromUser(requestAcceptor, sender.getUserId());
        }
    }

    @Override
    public User findById(int userId) {
        Optional<User> user = userRepository.findById(userId);
        return user.orElse(null);
    }

    /**
     * Remove a friend from your friend list.
     * If one of the users doesn't have the connection, a warning will be logged.
     *
     * @param emailUser1    -> e-mail from user1 (willing to break connection) (caller from API)
     * @param userNameUser2 -> username from user2
     */
    @Override
    public void removeFriend(String emailUser1, String userNameUser2) {
        User user1 = this.findByEmail(emailUser1);
        User user2 = this.findByUsername(userNameUser2);
        if (user1.getFriends().contains(user2) && user2.getFriends().contains(user1)) {
            user1.getFriends().remove(user2);
            user2.getFriends().remove(user1);
            save(user1);
            save(user2);
        } else {
            LOGGER.warn(user1.getUsername() + " tried to unfriend " + user2.getUsername() + " but never were friends");
        }
    }

    private List<Notification> getNotificationsFromUserOfType(User user, NotificationType type) {
        return user.getNotifications()
                .values()
                .stream()
                .filter(n -> n.getNotificatoinType().equals(type))
                .collect(Collectors.toList());
    }


    /**
     * Remove a friend request from your notifications.
     *
     * @param user     -> user to remove notification from
     * @param targetId -> id from the user that send you the friend request (targetId isn't userId if notificationType isn't of type FRIEND_REQUEST)
     */
    @Override
    public boolean removeFriendRequestNotificationFromUser(User user, int targetId) {
        Optional<Notification> notification = getNotificationsFromUserOfType(user, NotificationType.FRIEND_REQUEST)
                .stream()
                .filter(n -> n.getTargetId() == targetId)
                .findFirst();
        if (notification.isPresent()) {
            user.getNotifications().remove(notification.get().getNotificationId());
            this.save(user);
            notificationService.remove(notification.get());
            return true;
        } else {
            return false;
        }
    }

    /**
     * Save changes from a user. Username, password and profile picture can be changed.
     *
     * @param user -> user to remove notification from
     * @throws IncorrectPasswordException -> if the user changes his password and it doesn't matches the old password, an IncorrectPasswordException will be thrown.
     */
    @Override
    public void changeUserDetails(UserDTO user) {
        User existingUser = this.findByEmail(user.getEmail());
        if (!user.getNewPassword().equals("")) {
            if (bCryptPasswordEncoder.matches(user.getOldPassword(), existingUser.getPassword())) {
                existingUser.setPassword(bCryptPasswordEncoder.encode(user.getNewPassword()));
            } else {
                throw new IncorrectPasswordException("Old password isn't correct!");
            }
        }
        existingUser.setUsername(user.getUsername());
        if (!user.getProfilePicture().equals("")) {
            String delims = "[,]";
            String[] parts = user.getProfilePicture().split(delims);
            String imageString = parts[1];
            byte[] imageByteArray = Base64.getDecoder().decode(imageString);

            InputStream is = new ByteArrayInputStream(imageByteArray);

            //Find out image type
            String mimeType = null;
            String fileExtension = null;
            try {
                mimeType = URLConnection.guessContentTypeFromStream(is); //mimeType is something like "image/jpeg"
                String delimiter = "[/]";
                String[] tokens = mimeType.split(delimiter);
                fileExtension = tokens[1];
            } catch (IOException | NullPointerException e) {
                throw new WrongFileTypeException("File must be an image");
            }

            try {
                if (mimeType.contains("image")) {
                    existingUser.setProfilePicture(user.getProfilePicture());
                } else {
                    throw new WrongFileTypeException("File must be an image");
                }
            } catch (NullPointerException e) {
                throw new WrongFileTypeException("File must be an image");
            }
        }

        this.save(existingUser);
    }


    /**
     * Get the statistics from a user by his username.
     *
     * @param username -> username from the user
     */
    @Override
    public UserStatDTO getUserGameStats(String username) {
        User user = userRepository.findByUsername(username);
        UserStatDTO userStatDTO = new UserStatDTO();
        long playedAsStalin = user.getParticipations()
                .stream()
                .flatMap(p -> p.getRoles()
                        .stream()
                        .filter(r -> r.getCard().getRoleType().equals(RoleType.STALIN)))
                .count();
        long playedAsSlav = user.getParticipations()
                .stream()
                .filter(p -> p.getRoles().size() <= 1)
                .flatMap(p -> p.getRoles()
                        .stream()
                        .filter(r -> !r.getCard().getRoleType().equals(RoleType.STALIN) &&
                                r.getCard().getRoleType().equals(RoleType.SLAV)))
                .count();

        int wonAsSlav = 0;
        int wonAsStalin = 0;
        for (Participation participation : user.getParticipations()) {
            Game game = participation.getGame();
            if (!game.isActive()) {
                if (participation.isAlive()) {
                    switch (game.getWinningRole()) {
                        case SLAV:
                            wonAsSlav++;
                            break;
                        case STALIN:
                            wonAsStalin++;
                            break;
                    }
                }
            }

        }
        userStatDTO.setGamesPlayedAsSlav((int) playedAsSlav);
        userStatDTO.setGamePlayedAsStalin((int) playedAsStalin);
        userStatDTO.setGameWonAsSlav(wonAsSlav);
        userStatDTO.setGamesWonAsStalin(wonAsStalin);
        return userStatDTO;
    }

    @Override
    public void setFireBaseToken(String email, String firebasetoken) {
        User user = userRepository.findByEmail(email);
        user.setDeviceFireBaseToken(firebasetoken);
        userRepository.save(user);
    }
}
