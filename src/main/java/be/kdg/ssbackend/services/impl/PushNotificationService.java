package be.kdg.ssbackend.services.impl;


import be.kdg.ssbackend.config.HeaderRequestInterceptor;
import be.kdg.ssbackend.model.user.Notification;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.services.api.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class PushNotificationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PushNotificationService.class);
    private static final String FIREBASE_SERVER_KEY = "AAAAheQQhQQ:APA91bHLAUgqdcRsaptWdizas5mOG4jsnrV8L8ngY8Su__nV_Zk0-F5MKLTjlEMZxfeqmwsSaedwwr0WiRVQITxGdnoQTSx4etKxKU3C6hKP0mNScxEqEhO5M6KmmQa-64S2-kltdC4w";
    private static final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";

    private final UserService userService;

    public PushNotificationService(UserService userService) {
        this.userService = userService;
    }

    @Async
    public CompletableFuture<String> send(HttpEntity<String> entity) {

        RestTemplate restTemplate = new RestTemplate();

        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
        restTemplate.setInterceptors(interceptors);

        String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);

        return CompletableFuture.completedFuture(firebaseResponse);
    }

    public void send(String email, String message, String header, Notification noti) {

        ObjectMapper mapper = new ObjectMapper();

        ObjectNode body = mapper.createObjectNode();

        ObjectNode notification = mapper.createObjectNode();

        ObjectNode data = mapper.createObjectNode();

        User user = userService.findByEmail(email);

        body.put("to", user.getDeviceFireBaseToken());
        body.put("priority", "high");

        notification.put("title", header);
        notification.put("body", message);

        data.put("TYPE", noti.getNotificatoinType().toString());
        data.put("TARGETID", noti.getTargetId());

        body.put("notification", notification);
        body.put("data", data);

        HttpEntity<String> request = new HttpEntity<>(body.toString());

        CompletableFuture<String> pushNotification = this.send(request);
        CompletableFuture.allOf(pushNotification).join();

        try {
            String firebaseResponse = pushNotification.get();
        } catch (InterruptedException | ExecutionException e) {
            LOGGER.warn(e.getMessage());
        }
    }
}
