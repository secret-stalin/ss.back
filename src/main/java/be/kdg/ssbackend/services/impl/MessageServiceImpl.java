package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.MessageDTO;
import be.kdg.ssbackend.model.chat.Chat;
import be.kdg.ssbackend.model.chat.Message;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.MessageRepository;
import be.kdg.ssbackend.services.api.MessageService;
import be.kdg.ssbackend.services.api.UserService;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
    private final MessageRepository messageRepository;
    private final UserService userService;

    public MessageServiceImpl(MessageRepository messageRepository, UserService userService) {
        this.messageRepository = messageRepository;
        this.userService = userService;
    }

    @Override
    public Message save(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public List<Message> getMessagesFromChat(int chatId) {
        return messageRepository.findAllByChat_ChatId(chatId);
    }

    @Override
    public Message DTOToMessage(MessageDTO messageDTO, String email, Chat chat) {
        User sender = userService.findByEmail(email);
        return new Message(messageDTO.getText(), sender, chat);
    }

    @Override
    public MessageDTO MessageToDTO(Message message, String email) {
        return new MessageDTO(email + ": " + message.getText(), message.getTime().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
    }
}
