package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.NotificationDTO;
import be.kdg.ssbackend.exceptions.NoAccessException;
import be.kdg.ssbackend.model.user.Notification;
import be.kdg.ssbackend.model.user.NotificationSettings;
import be.kdg.ssbackend.model.user.NotificationType;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.NotificationRepository;
import be.kdg.ssbackend.services.api.NotificationService;
import be.kdg.ssbackend.services.api.NotificationsSettingsService;
import be.kdg.ssbackend.services.api.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService {
    private final NotificationRepository notificationRepository;
    private final UserService userService;
    private final SimpMessagingTemplate template;
    private final PushNotificationService pushNotificationService;
    private final NotificationsSettingsService settingsService;

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);

    public NotificationServiceImpl(NotificationRepository notificationRepository, UserService userService, SimpMessagingTemplate template, PushNotificationService pushNotificationService, NotificationsSettingsService settingsService) {
        this.notificationRepository = notificationRepository;
        this.userService = userService;
        this.template = template;
        this.pushNotificationService = pushNotificationService;
        this.settingsService = settingsService;
    }

    @Override
    public List<NotificationDTO> getNotifications(String email) {
        User user = userService.findByEmail(email);
        List<Notification> notifications = notificationRepository.findNotificationsByUser_UserId(user.getUserId());
        List<NotificationDTO> notificationDTOs = new ArrayList<>();
        for (Notification notification : notifications) {
            notificationDTOs.add(toNotificationDTO(notification));
        }
        Collections.reverse(notificationDTOs);
        return notificationDTOs;
    }

    /**
     * Get the notification from the user and set the read attribute to true.
     *
     * @param email          -> user details from the token (e-mail used)
     * @param notificationId -> username from user that send the friend request
     * @throws NullPointerException -> If the notification isn't found a NullPointerE
     * @throws NoAccessException    -> If the notification is not from the user a NoAccessException will be thrown.
     */
    @Override
    public void readNotification(String email, int notificationId) {
        User user = userService.findByEmail(email);
        try {
            Notification notification = notificationRepository.findByNotificationId(notificationId);
            if (user.getNotifications().get(notificationId).getNotificationId() == notificationId) {
                notification.setRead(true);
                notificationRepository.save(notification);
            } else {
                throw new NoAccessException("User " + email + " has no access to read this notification!");
            }
        } catch (NullPointerException e) {
            LOGGER.info(e.getMessage());
            throw e;
        }
    }

    @Override
    public int getUnreadNotificationCount(String email) {
        return (int) getNotifications(email).stream().filter(n -> !n.isRead()).count();
    }

    /**
     * Get the count of unread notifications and send it to the user (using websocket).
     */
    @Override
    public void sendNotificationCount(String email) {
        int count = getUnreadNotificationCount(email);

        template.convertAndSendToUser(email, "/notification/1", count);
    }


    /**
     * Add a notification if the user wants the notification based on notification settings.
     * Friend requests can't not be ignored.
     *
     * @param targetId -> id from the target based on type
     * @param type -> type from the notification (ENUM -> INVITE/FRIEND_REQUEST/ENDPHASE/YOUR_TURN)
     * @param text -> text send with the notification
     * @param email  -> e-mail from the user requesting the statistics from the game
     */
    @Override
    public void addNotification(int targetId, NotificationType type, String text, String email) {
        User user = userService.findByEmail(email);
        NotificationSettings settings = this.settingsService.readSettings(user.getUserId());
        boolean sendIt = true;
        if (settings == null) {
            settings = new NotificationSettings();
        }
        switch (type) {
            case FRIEND_REQUEST:
                if (settings.isIgnoreFriendRequest()) {
                    //send it stays on true otherwise it is not possible to receive friend requests
                }
                break;
            case INVITE:
                if (settings.isIgnoreInvite()) {
                    sendIt = false;
                }
                break;
            case ENDPHASE:
                if (settings.isIgnoreEndPhase()) {
                    sendIt = false;
                }
                break;
            case YOUR_TURN:
                if (settings.isIgnoreYourTurn()) {
                    sendIt = false;
                }
                break;
        }
        if (sendIt) {
            Notification notification = notificationRepository.save(new Notification(targetId, text, false, type, user));
            if (user.getDeviceFireBaseToken() != null) {
                pushNotificationService.send(email, type.toString(), text, notification);
            }
            this.sendNotificationCount(email);
        }
    }

    @Override
    public NotificationDTO toNotificationDTO(Notification notification) {
        return new NotificationDTO(
                notification.getNotificationId(),
                notification.getTargetId(),
                notification.getText(),
                notification.isRead(),
                notification.getNotificatoinType(),
                notification.getUser().getUserId()
        );
    }

    @Override
    public void remove(Notification notification) {
        notificationRepository.delete(notification);
    }

}
