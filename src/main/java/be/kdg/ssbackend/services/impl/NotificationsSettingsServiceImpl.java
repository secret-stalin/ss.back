package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.NotificationSettingsDTO;
import be.kdg.ssbackend.model.user.NotificationSettings;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.NotificationSettingsRepository;
import be.kdg.ssbackend.services.api.NotificationsSettingsService;
import be.kdg.ssbackend.services.api.UserService;
import org.springframework.stereotype.Service;

@Service
public class NotificationsSettingsServiceImpl implements NotificationsSettingsService {
    private NotificationSettingsRepository notificationsSettingsRepository;
    private UserService userService;

    public NotificationsSettingsServiceImpl(NotificationSettingsRepository notificationsSettingsRepository, UserService userService) {
        this.notificationsSettingsRepository = notificationsSettingsRepository;
        this.userService = userService;
    }

    @Override
    public NotificationSettings readSettings(int userId) {
        return this.notificationsSettingsRepository.findByUserUserId(userId);
    }

    @Override
    public void setSettings(String email, NotificationSettingsDTO settings) {
        User user = userService.findByEmail(email);
        NotificationSettings s = readSettings(user.getUserId());
        if (s == null) {
            s = new NotificationSettings();
        }
        s.setIgnoreEndPhase(settings.isIgnoreEndPhase());
        s.setIgnoreFriendRequest(settings.isIgnoreFriendRequest());
        s.setIgnoreInvite(settings.isIgnoreInvite());
        s.setIgnoreYourTurn(settings.isIgnoreYourTurn());
        s.setUser(user);
        this.notificationsSettingsRepository.save(s);
    }
}
