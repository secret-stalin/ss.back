package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.GameDTO;
import be.kdg.ssbackend.dto.GameStatsDTO;
import be.kdg.ssbackend.dto.ParticipationDTO;
import be.kdg.ssbackend.dto.RoleCardDTO;
import be.kdg.ssbackend.exceptions.GameNotFoundException;
import be.kdg.ssbackend.exceptions.NoAccessException;
import be.kdg.ssbackend.exceptions.ParticipationNotFoundException;
import be.kdg.ssbackend.exceptions.UserNotFoundException;
import be.kdg.ssbackend.model.game.*;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.CardRepository;
import be.kdg.ssbackend.repositories.api.GameRepository;
import be.kdg.ssbackend.services.api.GameService;
import be.kdg.ssbackend.services.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class GameServiceImpl implements GameService {

    private final GameRepository gameRepo;
    private final CardRepository cardRepo;
    private final UserService userService;

    @Autowired
    public GameServiceImpl(GameRepository gameRepo, CardRepository cardRepo, UserService userService) {
        this.gameRepo = gameRepo;
        this.cardRepo = cardRepo;
        this.userService = userService;
    }

    public Game createGame() {
        return gameRepo.save(new Game());
    }

    public Game getGame(int gameId) throws GameNotFoundException {
        if (gameRepo.findById(gameId).isPresent()) {
            return gameRepo.findGameByGameId(gameId);
        } else {
            throw new GameNotFoundException(String.format("Game %d not found", gameId));
        }
    }

    public Game saveGame(Game game) {
        return gameRepo.save(game);
    }

    /**
     * get all of the games where the user is participant
     *
     * @param email -> e-mail from the user requesting his games
     */
    @Override
    public List<GameDTO> getActiveGames(String email) {
        User user = userService.findByEmail(email);
        List<Game> games = user.getParticipations().stream().map(Participation::getGame).collect(Collectors.toList());
        Collections.reverse(games);
        List<GameDTO> gameDTOs = new ArrayList<>();
        for (Game game : games) {
            GameDTO gameDTO = new GameDTO();
            gameDTO.setGameId(game.getGameId());
            gameDTO.setName(game.getName());
            gameDTO.setActive(game.isActive());
            gameDTO.setRoleTypeToVote(game.getPhases().get(game.getPhases().size() - 1).getRoleTypeVoter());
            gameDTO.setState(game.getPhases().get(game.getPhases().size() - 1).getState());
            if (!game.isActive()) {
                gameDTO.setWinningRole(game.getWinningRole().name());
            }
            gameDTOs.add(gameDTO);
        }
        return gameDTOs;
    }

    /**
     * Get the statistics from a game. If the games has ended a winning role will be set.
     *
     * @param gameId -> id from the game
     * @param email  -> e-mail from the user requesting the statistics from the game
     * @throws GameNotFoundException -> if the game can't be found a GameNotFoundException will be thrown
     * @throws NoAccessException     -> if you're not a participant from this game a NoAccessException will be thrown
     */
    @Override
    public GameStatsDTO getGameResultStats(int gameId, String email) {

        // can throw exception (GameNotFoundException)
        Game game = getGame(gameId);
        User user = userService.findByEmail(email);

        if (game.getParticipations().stream().anyMatch(p -> p.getUser().equals(user))) {
            GameStatsDTO gameStatsDTO = new GameStatsDTO();

            // Winning Role ( + card)
            RoleType winnerRoleType = game.getWinningRole();
            if (!game.isActive()) {
                gameStatsDTO.setWinningRole(winnerRoleType.name());
                gameStatsDTO.setWinningRoleImg(cardRepo.findByRoleType(winnerRoleType).getImage());
            }

            // Winners
            gameStatsDTO.setWinners(
                    game.getParticipations()
                            .stream()
                            .filter(Participation::isAlive)
                            .map(p -> p.getUser().getUsername())
                            .collect(Collectors.toList())
            );

            // RoundCount
            gameStatsDTO.setRoundCount(game.getPhases().size());

            // KillsInOrder
            List<String> killsInOrder = game
                    .getPhases()
                    .stream()
                    .flatMap(p -> p.getActions().stream()
                            .filter(a -> a.getActionType().equals(ActionType.KILL)))
                    .sorted(Comparator.comparing(Action::getTime))
                    .map(a -> a.getAffected().getUser().getUsername())
                    .collect(Collectors.toList());
            gameStatsDTO.setKillsInOrder(killsInOrder);

            return gameStatsDTO;
        } else {
            throw new NoAccessException("You don't have access to this game!");
        }
    }

    /**
     * Get the roles from a participation.
     *
     * @param gameId -> id from the game
     * @param email  -> e-mail from the user requesting the statistics from the game
     * @throws UserNotFoundException -> if the user can't be found a GameNotFoundException will be thrown
     * @throws NoAccessException     -> if you're not a participant from this game a NoAccessException will be thrown
     */
    @Override
    public List<RoleCardDTO> getRolesByParticipation(String email, int gameId) {

        try {
            User user = userService.findByEmail(email);
            Optional<Participation> participation = user.getParticipations().stream().filter(p -> p.getGame().getGameId() == gameId).findFirst();
            if (participation.isPresent()) {
                List<Role> roles = new ArrayList<>(participation.get().getRoles());
                List<RoleCardDTO> roleCardDTOS = new ArrayList<>();
                roles.forEach(r -> roleCardDTOS.add(new RoleCardDTO(r.getCard().getRoleType().name(), r.getCard().getImage())));
                return roleCardDTOS;
            } else {
                throw new NoAccessException("User has no access to this game!");
            }
        } catch (NullPointerException e) {
            throw new UserNotFoundException("User not found!");
        }
    }

    /**
     * Get the participation from a user with a gameId.
     *
     * @param gameId -> id from the game
     * @param email  -> e-mail from the user requesting the statistics from the game
     * @throws ParticipationNotFoundException -> if the game doesn't have a participation linked to the user a ParticipationNotFoundException will be thrown
     */
    @Override
    public ParticipationDTO getCurrentParticipation(String email, int gameId) {
        Game game = getGame(gameId);
        Optional<Participation> participationResult = game.getParticipations().stream().filter(p -> p.getUser().getEmail().equals(email)).findFirst();
        ParticipationDTO participationDTO;
        if (participationResult.isPresent()) {
            Participation participation = participationResult.get();
            participationDTO = new ParticipationDTO(participation.getParticipationId(), participation.isAlive(), participation.getColorCode(), participation.getUser());
            participationDTO.setRoleNames(participation.getRoles().stream().map(r -> r.getCard().getRoleType().name()).collect(Collectors.toList()));
            return participationDTO;
        } else {
            throw new ParticipationNotFoundException("Participation not found");
        }

    }

    /**
     * Get the participationId from the users target filtered by the roleType.
     *
     * @param gameId -> id from the game
     * @param email  -> e-mail from the user requesting the statistics from the game
     * @param role -> role where you want the target from (HUNTER, MAYOR)
     */
    @Override
    public int getTargetFromRole(String email, int gameId, RoleType role) {
        Game game = getGame(gameId);
        Participation participation = game.getParticipations().stream().filter(p -> p.getUser().getEmail().equals(email)).findFirst().get();
        switch (role) {
            case SLAV:
                return participation
                        .getActionsDone()
                        .stream()
                        .filter(p ->
                                p.getActionType().equals(ActionType.VOTE) &&
                                        p.getPhase().getPhaseId() == game.getPhases()
                                                .stream()
                                                .max(Comparator.comparing(Phase::getPhaseId))
                                                .get()
                                                .getPhaseId())
                        .max(Comparator.comparing(Action::getTime))
                        .get()
                        .getAffected()
                        .getParticipationId();
            case HUNTER:
                return participation
                        .getActionsDone()
                        .stream()
                        .filter(p -> p.getActionType().equals(ActionType.HUNTER_TARGET))
                        .max(Comparator.comparing(Action::getTime))
                        .get()
                        .getAffected()
                        .getParticipationId();
            case MAYOR:
                return participation
                        .getActionsDone()
                        .stream()
                        .filter(p -> p.getActionType().equals(ActionType.MAYOR_CHOICE))
                        .max(Comparator.comparing(Action::getTime))
                        .get()
                        .getAffected()
                        .getParticipationId();
            default:
                return 0;
        }
    }
}
