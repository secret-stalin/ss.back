package be.kdg.ssbackend.services.impl;

import be.kdg.ssbackend.dto.EventDTO;
import be.kdg.ssbackend.dto.MessageDTO;
import be.kdg.ssbackend.dto.ParticipationDTO;
import be.kdg.ssbackend.dto.ReplayDTO;
import be.kdg.ssbackend.exceptions.NoAccessException;
import be.kdg.ssbackend.model.chat.GameChat;
import be.kdg.ssbackend.model.chat.Message;
import be.kdg.ssbackend.model.game.*;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.repositories.api.PhaseRepository;
import be.kdg.ssbackend.services.api.GameChatService;
import be.kdg.ssbackend.services.api.GameService;
import be.kdg.ssbackend.services.api.ReplayService;
import be.kdg.ssbackend.services.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ReplayServiceImpl implements ReplayService {

    private final UserService userService;
    private final GameService gameService;
    private final GameChatService gameChatService;
    private final PhaseRepository phaseRepo;

    @Autowired
    public ReplayServiceImpl(UserService userService, GameService gameService, GameChatService gameChatService, PhaseRepository phaseRepo) {
        this.userService = userService;
        this.gameService = gameService;
        this.gameChatService = gameChatService;
        this.phaseRepo = phaseRepo;
    }

    @Override
    public ReplayDTO getReplay(int gameId, String email) {
        User user = userService.findByEmail(email);
        Game game = gameService.getGame(gameId);
        int userCount = game.getParticipations().stream().filter(p -> p.getUser().getUserId() == user.getUserId()).collect(Collectors.toList()).size();
        if (userCount == 1) {
            List<EventDTO> eventDTOS = new ArrayList<>();
            ReplayDTO result = new ReplayDTO();
            for (GameChat gc : game.getGameChats()) {
                List<Message> messages = gameChatService.getMessagesFromChat(gc.getChatId());
                List<MessageDTO> messageDTOs = messages.stream().map(m -> {
                    MessageDTO dto = new MessageDTO(m.getSender().getUsername() + ": " + m.getText(), m.getTime().toString());
                    return dto;
                }).collect(Collectors.toList());

                for (MessageDTO messageDTO : messageDTOs) {
                    EventDTO eventDTO = new EventDTO();
                    eventDTO.setEventType(EventType.CHAT);
                    eventDTO.setMessage(messageDTO);
                    eventDTO.setTimestamp(messageDTO.getTimestamp());
                    eventDTO.setChatAccess(gc.getAccess());
                    eventDTOS.add(eventDTO);
                }
            }

            List<Phase> phases = phaseRepo.findAllByGame(game);
            for (Phase phase : phases) {
                List<Action> actions = phase.getActions();

                for (Action action : actions) {
                    EventDTO eventDTO = new EventDTO();
                    if (action.getActionType().equals(ActionType.VOTE)) {
                        eventDTO.setEventType(EventType.VOTE);
                    } else if (action.getActionType().equals(ActionType.KILL)) {
                        eventDTO.setEventType(EventType.KILL);
                    }
                    eventDTO.setAffectedPlayerId(action.getAffected().getParticipationId());
                    if (!action.getActionType().equals(ActionType.KILL)) {
                        eventDTO.setExecutorId(action.getExecutor().getParticipationId());
                    }
                    eventDTO.setTimestamp(action.getTime().toString());
                    if (!eventDTOS.contains(eventDTO)) {
                        eventDTOS.add(eventDTO);
                    }
                }

                eventDTOS.add(new EventDTO(eventDTOS.get(eventDTOS.size() - 1).getTimestamp(), EventType.ENDPHASE, null, null, 0, 0));
            }

            eventDTOS.sort(Comparator.comparing(o -> LocalDateTime.parse(o.getTimestamp())));

            result.setEvents(eventDTOS);
            Set<Participation> participations = game.getParticipations();
            result.setParticipations(participations
                    .stream()
                    .map(p -> new ParticipationDTO(p.getParticipationId(), p.isAlive(), p.getColorCode(), p.getUser()))
                    .collect(Collectors.toList()));
            return result;
        } else {
            throw new NoAccessException(String.format("User %d did not participate in game %d", user.getUserId(), game.getGameId()));
        }
    }
}
