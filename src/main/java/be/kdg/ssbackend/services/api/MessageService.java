package be.kdg.ssbackend.services.api;

import be.kdg.ssbackend.dto.MessageDTO;
import be.kdg.ssbackend.model.chat.Chat;
import be.kdg.ssbackend.model.chat.Message;

import java.util.List;

public interface MessageService {
    Message save(Message message);

    List<Message> getMessagesFromChat(int chatId);

    Message DTOToMessage(MessageDTO messageDTO, String email, Chat chat);

    MessageDTO MessageToDTO(Message message, String email);
}
