package be.kdg.ssbackend.services.api;

import be.kdg.ssbackend.dto.NotificationSettingsDTO;
import be.kdg.ssbackend.model.user.NotificationSettings;

public interface NotificationsSettingsService {

    NotificationSettings readSettings(int userId);

    void setSettings(String email, NotificationSettingsDTO settings);
}
