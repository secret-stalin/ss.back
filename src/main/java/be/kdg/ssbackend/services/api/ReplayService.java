package be.kdg.ssbackend.services.api;

import be.kdg.ssbackend.dto.ReplayDTO;

public interface ReplayService {
    ReplayDTO getReplay(int gameId, String email);
}
