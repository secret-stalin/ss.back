package be.kdg.ssbackend.services.api;

import be.kdg.ssbackend.dto.GameChatDTO;
import be.kdg.ssbackend.dto.MessageDTO;
import be.kdg.ssbackend.model.chat.GameChat;
import be.kdg.ssbackend.model.chat.Message;

import java.util.List;

public interface GameChatService {
    List<GameChatDTO> getChatsFromGame(String email, int gameId);

    void sendMessage(int chatId, MessageDTO message, String name);

    void addMessageToGameChat(GameChat gameChat, MessageDTO messageDTO, String emailSender);

    List<Message> getMessagesFromChat(int chatId);
}
