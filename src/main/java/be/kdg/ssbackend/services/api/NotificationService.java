package be.kdg.ssbackend.services.api;

import be.kdg.ssbackend.dto.NotificationDTO;
import be.kdg.ssbackend.model.user.Notification;
import be.kdg.ssbackend.model.user.NotificationType;

import java.util.List;

public interface NotificationService {

    List<NotificationDTO> getNotifications(String email);

    void readNotification(String email, int notificationId);

    int getUnreadNotificationCount(String email);

    void sendNotificationCount(String email);

    void addNotification(int targetId, NotificationType type, String text, String email);

    NotificationDTO toNotificationDTO(Notification notification);

    void remove(Notification notification);

}
