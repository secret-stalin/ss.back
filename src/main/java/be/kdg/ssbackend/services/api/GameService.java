package be.kdg.ssbackend.services.api;

import be.kdg.ssbackend.dto.GameDTO;
import be.kdg.ssbackend.dto.GameStatsDTO;
import be.kdg.ssbackend.dto.ParticipationDTO;
import be.kdg.ssbackend.dto.RoleCardDTO;
import be.kdg.ssbackend.model.game.Game;
import be.kdg.ssbackend.model.game.RoleType;

import java.util.List;

public interface GameService {
    Game createGame();

    Game getGame(int gameId);

    Game saveGame(Game game);

    List<GameDTO> getActiveGames(String email);

    GameStatsDTO getGameResultStats(int gameId, String email);

    List<RoleCardDTO> getRolesByParticipation(String email, int gameId);

    ParticipationDTO getCurrentParticipation(String email, int gameId);

    int getTargetFromRole(String email, int gameId, RoleType role);
}
