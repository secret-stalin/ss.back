package be.kdg.ssbackend.services.api;

import be.kdg.ssbackend.dto.UserDTO;
import be.kdg.ssbackend.dto.UserStatDTO;
import be.kdg.ssbackend.model.chat.PrivateChat;
import be.kdg.ssbackend.model.user.NotificationSettings;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.model.user.VerificationToken;

import java.util.List;

public interface UserService {
    User findByEmail(String email);

    List<User> findAllUsers();

    User findByUsername(String username);

    User save(User user);

    void sendConfirmationMail(User user);

    void checkToken(String verificationToken);

    VerificationToken getVerificationToken(String VerificationToken);

    VerificationToken createVerificationToken(User user, String token);

    void resendVerificationMail(String email);

    void createUserAccount(UserDTO accountDto, boolean sendMail);

    boolean checkEmailRegex(String email);

    UserDTO userToDTO(User user);

    List<PrivateChat> getChatsFromUser(String email);

    List<User> findFriendsFromUser(String email);

    boolean isFriendFrom(String requestUserEmail, String username);

    boolean hasFriendRequestFromUser(String requestUserEmail, String username, boolean inOwnNotifications);

    void sendFriendRequest(String emailRequestSender, String userNameToFriend);

    void acceptFriendRequest(String emailFromAcceptor, String userNameSender);

    void declineFriendRequest(String emailFromAcceptor, String userNameSender);

    User findById(int userId);

    void removeFriend(String emailUser1, String userNameUser2);

    boolean removeFriendRequestNotificationFromUser(User user, int targerId);

    void changeUserDetails(UserDTO user);

    UserStatDTO getUserGameStats(String username);

    void setFireBaseToken(String email, String firebasetoken);

}
