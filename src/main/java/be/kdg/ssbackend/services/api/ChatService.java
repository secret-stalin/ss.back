package be.kdg.ssbackend.services.api;

import be.kdg.ssbackend.dto.ChatDTO;
import be.kdg.ssbackend.dto.MessageDTO;
import be.kdg.ssbackend.model.chat.Message;
import be.kdg.ssbackend.model.user.User;

import java.util.List;

public interface ChatService {
    List<Message> getMessagesFromChat(int chatId);

    List<String> getEmailsOfParticipants(int chatId);

    void addMessage(int chatId, MessageDTO message, String email);

    void sendMessage(String emailSender, MessageDTO message, int chatId);

    List<MessageDTO> getMessagesFromChat(String email, int chatId);

    List<ChatDTO> getChatsFromUser(String email);

    void createPrivateChat(User requestAcceptor, User sender);
}
