package be.kdg.ssbackend.services.api;

import be.kdg.ssbackend.dto.GameSettingsDTO;
import be.kdg.ssbackend.dto.LobbyDTO;
import be.kdg.ssbackend.dto.UserDTO;
import be.kdg.ssbackend.model.game.Lobby;

import java.util.List;

public interface LobbyService {
    LobbyDTO createLobby(int gameId, GameSettingsDTO settings, String hostMail);

    LobbyDTO getLobby(int lobbyId);

    int startLobby(int lobbyId, String user);

    void createInvitation(int lobbyId, String email);

    LobbyDTO lobbyToDTO(Lobby lobby);

    Lobby acceptedInvitation(int invitationId);

    List<UserDTO> getJoined(int lobbyId);

    List<LobbyDTO> getPublicLobbies();

    Lobby joinPublicLobby(int lobbyId, String userMail);

    List<LobbyDTO> getActiveLobbies(String userMail);
}
