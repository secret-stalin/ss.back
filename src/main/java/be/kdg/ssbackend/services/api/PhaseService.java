package be.kdg.ssbackend.services.api;

import be.kdg.ssbackend.dto.VoteResultDTO;
import be.kdg.ssbackend.model.game.RoleType;
import be.kdg.ssbackend.model.user.User;

import java.util.List;

public interface PhaseService {
    void addPhase(int gameId);

    void addAction(int gameId, User executor, int partIdAffected, RoleType roleType);

    void endPhase(int gameId);

    List<VoteResultDTO> getVoteResults(User user, int gameId);
}
