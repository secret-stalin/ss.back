package be.kdg.ssbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsbackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsbackendApplication.class, args);
    }

}

