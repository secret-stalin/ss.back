package be.kdg.ssbackend.exceptions;

public class GameNotFoundException extends RuntimeException {
	public GameNotFoundException(String message){super(message);}
}
