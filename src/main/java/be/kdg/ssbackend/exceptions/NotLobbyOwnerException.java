package be.kdg.ssbackend.exceptions;

public class NotLobbyOwnerException extends RuntimeException {
    public NotLobbyOwnerException(String message){super(message);}
}
