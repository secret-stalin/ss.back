package be.kdg.ssbackend.exceptions;

public class LobbyNotFoundException extends RuntimeException {
    public LobbyNotFoundException(String message){super(message);}
}
