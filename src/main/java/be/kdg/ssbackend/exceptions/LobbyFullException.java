package be.kdg.ssbackend.exceptions;

public class LobbyFullException extends RuntimeException {
    public LobbyFullException(String message){
        super(message);
    }
}
