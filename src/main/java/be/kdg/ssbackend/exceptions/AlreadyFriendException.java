package be.kdg.ssbackend.exceptions;

public class AlreadyFriendException extends RuntimeException {

    public AlreadyFriendException(String message) {
        super(message);
    }
}
