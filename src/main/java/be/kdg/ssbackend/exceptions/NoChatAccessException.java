package be.kdg.ssbackend.exceptions;

public class NoChatAccessException extends RuntimeException {

    public NoChatAccessException(String message) {
        super(message);
    }
}
