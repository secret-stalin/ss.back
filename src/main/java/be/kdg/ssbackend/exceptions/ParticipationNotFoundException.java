package be.kdg.ssbackend.exceptions;

public class ParticipationNotFoundException extends RuntimeException {
    public ParticipationNotFoundException(String message){super(message);}
}
