package be.kdg.ssbackend.dto;

import java.util.List;

public class ReplayDTO {
    private List<EventDTO> events;
    private List<ParticipationDTO> participations;

    public ReplayDTO(List<EventDTO> events, List<ParticipationDTO> participations) {
        this.events = events;
        this.participations = participations;
    }

    public ReplayDTO(){}

    public List<ParticipationDTO> getParticipations() {
        return participations;
    }

    public void setParticipations(List<ParticipationDTO> participations) {
        this.participations = participations;
    }

    public List<EventDTO> getEvents() {
        return events;
    }

    public void setEvents(List<EventDTO> events) {
        this.events = events;
    }
}
