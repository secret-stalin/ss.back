package be.kdg.ssbackend.dto;

public class RoleCardDTO {

    private String roleName;
    private String card;

    public RoleCardDTO(String roleName, String card) {
        this.roleName = roleName;
        this.card = card;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }
}
