package be.kdg.ssbackend.dto;

import java.util.List;

public class GameStatsDTO {

    private String winningRole;
    private String winningRoleImg;
    private List<String> winners;
    private int roundCount;
    private List<String> killsInOrder;

    public GameStatsDTO() {

    }

    public String getWinningRole() {
        return winningRole;
    }

    public void setWinningRole(String winningRole) {
        this.winningRole = winningRole;
    }

    public String getWinningRoleImg() {
        return winningRoleImg;
    }

    public void setWinningRoleImg(String winningRoleImg) {
        this.winningRoleImg = winningRoleImg;
    }

    public List<String> getWinners() {
        return winners;
    }

    public void setWinners(List<String> winners) {
        this.winners = winners;
    }

    public int getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }

    public List<String> getKillsInOrder() {
        return killsInOrder;
    }

    public void setKillsInOrder(List<String> killsInOrder) {
        this.killsInOrder = killsInOrder;
    }
}
