package be.kdg.ssbackend.dto;

import java.util.List;

public class GameSettingsDTO {
    private int _gameId;
    private String _gameName;
    private int _maxPlayers;
    private boolean _isPrivate;
    private int _dayTime;
    private int _nightTime;
    private List<String> _roles;

    public GameSettingsDTO() {
    }

    public GameSettingsDTO(int _gameId, String _gameName, int _maxPlayers, boolean _isPrivate, int _dayTime, int _nightTime, List<String> _roles) {
        this._gameId = _gameId;
        this._gameName = _gameName;
        this._maxPlayers = _maxPlayers;
        this._isPrivate = _isPrivate;
        this._dayTime = _dayTime;
        this._nightTime = _nightTime;
        this._roles = _roles;
    }

    public int get_gameId() {
        return _gameId;
    }

    public void set_gameId(int _gameId) {
        this._gameId = _gameId;
    }

    public String get_gameName() {
        return _gameName;
    }

    public void set_gameName(String _gameName) {
        this._gameName = _gameName;
    }

    public int get_maxPlayers() {
        return _maxPlayers;
    }

    public void set_maxPlayers(int _maxPlayers) {
        this._maxPlayers = _maxPlayers;
    }

    public boolean is_isPrivate() {
        return _isPrivate;
    }

    public void set_isPrivate(boolean _isPrivate) {
        this._isPrivate = _isPrivate;
    }

    public int get_dayTime() {
        return _dayTime;
    }

    public void set_dayTime(int _dayTime) {
        this._dayTime = _dayTime;
    }

    public int get_nightTime() {
        return _nightTime;
    }

    public void set_nightTime(int _nightTime) {
        this._nightTime = _nightTime;
    }

    public List<String> get_roles() {
        return _roles;
    }

    public void set_roles(List<String> _roles) {
        this._roles = _roles;
    }
}
