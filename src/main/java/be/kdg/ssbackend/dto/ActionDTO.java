package be.kdg.ssbackend.dto;

import be.kdg.ssbackend.model.game.RoleType;

public class ActionDTO {
    private int gameId;
    private boolean isWin;
    private boolean isEndPhase;
    private String roleTypeWinner;
    private int affectedPlayerId;
    private String timeStamp;

    private int executorId;

    public ActionDTO() {
    }

    public ActionDTO(int gameId, boolean isWin, boolean isEndPhase) {
        this.gameId = gameId;
        this.isWin = isWin;
        this.isEndPhase = isEndPhase;
    }

    public ActionDTO(int gameId, int affectedPlayerId, String timeStamp, int executorId) {
        this.gameId = gameId;
        this.affectedPlayerId = affectedPlayerId;
        this.timeStamp = timeStamp;
        this.executorId = executorId;
        this.isWin = false;
        this.isEndPhase = false;
    }

    public ActionDTO(int gameId, boolean isWin, boolean isEndPhase, RoleType roleTypeWinner, int affectedPlayerId) {
        this.gameId = gameId;
        this.isWin = isWin;
        this.isEndPhase = isEndPhase;
        this.roleTypeWinner = roleTypeWinner.toString();
        this.affectedPlayerId = affectedPlayerId;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public boolean isWin() {
        return isWin;
    }

    public void setWin(boolean win) {
        isWin = win;
    }

    public boolean isEndPhase() {
        return isEndPhase;
    }

    public void setEndPhase(boolean endPhase) {
        isEndPhase = endPhase;
    }

    public String getRoleTypeWinner() {
        return roleTypeWinner;
    }

    public void setRoleTypeWinner(RoleType roleTypeWinner) {
        this.roleTypeWinner = roleTypeWinner.toString();
    }

    public int getAffectedPlayerId() {
        return affectedPlayerId;
    }

    public void setAffectedPlayerId(int affectedPlayerId) {
        this.affectedPlayerId = affectedPlayerId;
    }

    public int getExecutorId() {
        return executorId;
    }

    public void setExecutorId(int executorId) {
        this.executorId = executorId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
