package be.kdg.ssbackend.dto;

public class UserStatDTO {
    private int gamesPlayedAsSlav;
    private int gameWonAsSlav;
    private int gamePlayedAsStalin;
    private int gamesWonAsStalin;

    public UserStatDTO() {
    }

    public int getGamesPlayedAsSlav() {
        return gamesPlayedAsSlav;
    }

    public void setGamesPlayedAsSlav(int gamesPlayedAsSlav) {
        this.gamesPlayedAsSlav = gamesPlayedAsSlav;
    }

    public int getGameWonAsSlav() {
        return gameWonAsSlav;
    }

    public void setGameWonAsSlav(int gameWonAsSlav) {
        this.gameWonAsSlav = gameWonAsSlav;
    }

    public int getGamePlayedAsStalin() {
        return gamePlayedAsStalin;
    }

    public void setGamePlayedAsStalin(int gamePlayedAsStalin) {
        this.gamePlayedAsStalin = gamePlayedAsStalin;
    }

    public int getGamesWonAsStalin() {
        return gamesWonAsStalin;
    }

    public void setGamesWonAsStalin(int gamesWonAsStalin) {
        this.gamesWonAsStalin = gamesWonAsStalin;
    }
}
