package be.kdg.ssbackend.dto;

import be.kdg.ssbackend.model.game.EventType;
import be.kdg.ssbackend.model.game.RoleType;

import java.util.Objects;

public class EventDTO {
    private String timestamp;
    private EventType eventType;
    private RoleType chatAccess;
    private MessageDTO message;
    private int affectedPlayerId;
    private int executorId;

    public EventDTO() {
    }

    public EventDTO(String timestamp, EventType eventType, RoleType chatAccess, MessageDTO message, int affectedPlayerId, int executorId) {
        this.timestamp = timestamp;
        this.eventType = eventType;
        this.chatAccess = chatAccess;
        this.message = message;
        this.affectedPlayerId = affectedPlayerId;
        this.executorId = executorId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public MessageDTO getMessage() {
        return message;
    }

    public void setMessage(MessageDTO message) {
        this.message = message;
    }

    public int getAffectedPlayerId() {
        return affectedPlayerId;
    }

    public void setAffectedPlayerId(int affectedPlayerId) {
        this.affectedPlayerId = affectedPlayerId;
    }

    public RoleType getChatAccess() {
        return chatAccess;
    }

    public void setChatAccess(RoleType chatAccess) {
        this.chatAccess = chatAccess;
    }

    public int getExecutorId() {
        return executorId;
    }

    public void setExecutorId(int executorId) {
        this.executorId = executorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventDTO eventDTO = (EventDTO) o;
        return affectedPlayerId == eventDTO.affectedPlayerId &&
                Objects.equals(timestamp, eventDTO.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, affectedPlayerId);
    }
}
