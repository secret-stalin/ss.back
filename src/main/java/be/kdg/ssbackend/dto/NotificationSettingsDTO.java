package be.kdg.ssbackend.dto;

public class NotificationSettingsDTO {
    private boolean ignoreFriendRequest;
    private boolean ignoreInvite;
    private boolean ignoreEndPhase;
    private boolean ignoreYourTurn;


    public NotificationSettingsDTO() {
    }

    public NotificationSettingsDTO(boolean ignoreFriendRequest, boolean ignoreInvite, boolean ignoreEndPhase, boolean ignoreYourTurn) {
        this.ignoreFriendRequest = ignoreFriendRequest;
        this.ignoreInvite = ignoreInvite;
        this.ignoreEndPhase = ignoreEndPhase;
        this.ignoreYourTurn = ignoreYourTurn;
    }

    public boolean isIgnoreFriendRequest() {
        return ignoreFriendRequest;
    }

    public void setIgnoreFriendRequest(boolean ignoreFriendRequest) {
        this.ignoreFriendRequest = ignoreFriendRequest;
    }

    public boolean isIgnoreInvite() {
        return ignoreInvite;
    }

    public void setIgnoreInvite(boolean ignoreInvite) {
        this.ignoreInvite = ignoreInvite;
    }

    public boolean isIgnoreEndPhase() {
        return ignoreEndPhase;
    }

    public void setIgnoreEndPhase(boolean ignoreEndPhase) {
        this.ignoreEndPhase = ignoreEndPhase;
    }

    public boolean isIgnoreYourTurn() {
        return ignoreYourTurn;
    }

    public void setIgnoreYourTurn(boolean ignoreYourTurn) {
        this.ignoreYourTurn = ignoreYourTurn;
    }
}

