package be.kdg.ssbackend.dto;

import java.util.List;

public class LobbyDTO {
    private int lobbyId;
    private List<UserDTO> users;
    private GameSettingsDTO gameSettings;

    public LobbyDTO() {
    }

    public int getLobbyId() {
        return lobbyId;
    }

    public void setLobbyId(int lobbyId) {
        this.lobbyId = lobbyId;
    }

    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }

    public GameSettingsDTO getGameSettings() {
        return gameSettings;
    }

    public void setGameSettings(GameSettingsDTO gameSettings) {
        this.gameSettings = gameSettings;
    }
}
