package be.kdg.ssbackend.dto;

import java.util.ArrayList;
import java.util.List;

public class GameChatDTO {
    public int id;
    public String name;
    public List<MessageDTO> messages;

    public GameChatDTO() {
        this.messages= new ArrayList<>();
    }

    public GameChatDTO(int id, String name, List<MessageDTO> messages) {
        this.id = id;
        this.name = name;
        this.messages = messages;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MessageDTO> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageDTO> messages) {
        this.messages = messages;
    }
}
