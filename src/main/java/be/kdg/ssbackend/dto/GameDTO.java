package be.kdg.ssbackend.dto;

import be.kdg.ssbackend.model.game.Participation;
import be.kdg.ssbackend.model.game.RoleType;
import be.kdg.ssbackend.model.game.State;

import java.util.ArrayList;
import java.util.List;

public class GameDTO {
    private int gameId;
    private String name;
    private RoleType roleTypeToVote;
    private State state;
    private List<ParticipationDTO> participationDTOs;
    private boolean isActive;
    private String winningRole;

    public GameDTO() {

    }

    public GameDTO(int gameId, String name, RoleType roleTypeToVote, State state, List<Participation> participations) {
        participationDTOs = new ArrayList<>();
        this.gameId = gameId;
        this.name = name;
        this.roleTypeToVote = roleTypeToVote;
        this.state = state;
        for (Participation p : participations) {
            ParticipationDTO partDto = new ParticipationDTO(p.getParticipationId(), p.isAlive(), p.getColorCode(), p.getUser());
            participationDTOs.add(partDto);
        }

    }

    public List<ParticipationDTO> getParticipationDTOs() {
        return participationDTOs;
    }

    public void setParticipationDTOs(List<ParticipationDTO> participationDTOs) {
        this.participationDTOs = participationDTOs;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public RoleType getRoleTypeToVote() {
        return roleTypeToVote;
    }

    public void setRoleTypeToVote(RoleType roleTypeToVote) {
        this.roleTypeToVote = roleTypeToVote;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public List<ParticipationDTO> getParticipations() {
        return participationDTOs;
    }

    public void setParticipations(List<ParticipationDTO> participations) {
        this.participationDTOs = participations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWinningRole() {
        return winningRole;
    }

    public void setWinningRole(String winningRole) {
        this.winningRole = winningRole;
    }
}
