package be.kdg.ssbackend.dto;

public class MessageDTO {
    private String text;
    private String timestamp;

    public MessageDTO() {
    }

    public MessageDTO(String text, String timestamp) {
        this.text = text;
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
