package be.kdg.ssbackend.dto;

public class VoteResultDTO {
    private String username;
    private int voteCount;

    public VoteResultDTO(String username, int voteCount) {
        this.username = username;
        this.voteCount = voteCount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }
}
