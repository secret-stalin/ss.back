package be.kdg.ssbackend.dto;

import be.kdg.ssbackend.model.game.RoleType;

public class WinDTO {
    private boolean isWin;
    private RoleType roleTypeWinner;

    public WinDTO() {
    }

    public WinDTO(boolean isWin, RoleType roleTypeWinner) {
        this.isWin = isWin;
        this.roleTypeWinner = roleTypeWinner;
    }

    public boolean isWin() {
        return isWin;
    }

    public void setWin(boolean win) {
        isWin = win;
    }

    public RoleType getRoleTypeWinner() {
        return roleTypeWinner;
    }

    public void setRoleTypeWinner(RoleType roleTypeWinner) {
        this.roleTypeWinner = roleTypeWinner;
    }
}
