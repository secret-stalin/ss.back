package be.kdg.ssbackend.dto;

import be.kdg.ssbackend.model.user.User;

import java.util.ArrayList;
import java.util.List;

public class ParticipationDTO {
    private int participationId;
    private boolean isAlive;
    private String colorCode;
    private UserDTO userDTO;
    private List<String> roleNames;

    public ParticipationDTO() {
    }

    public ParticipationDTO(int participationId, boolean isAlive, String colorCode, User user) {
        userDTO = new UserDTO();

        roleNames = new ArrayList<>();
        this.participationId = participationId;
        this.isAlive = isAlive;
        this.colorCode = colorCode;
        this.userDTO.setProfilePicture(user.getProfilePicture());
        this.userDTO.setEmail(user.getEmail());
        this.userDTO.setUsername(user.getUsername());
    }

    public List<String> getRoleNames() {
        return roleNames;
    }

    public void setRoleNames(List<String> roleNames) {
        this.roleNames = roleNames;
    }

    public int getParticipationId() {
        return participationId;
    }

    public void setParticipationId(int participationId) {
        this.participationId = participationId;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
