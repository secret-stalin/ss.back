package be.kdg.ssbackend.dto;

public class VoteDTO {
    private int gameId;
    private int affectedPlayerId;
    private String roleTypeExecutor;

    public VoteDTO() {
        this.gameId = 0;
        this.affectedPlayerId = 0;
        roleTypeExecutor = "SLAV";
    }

    public VoteDTO(int gameId, int affectedPlayerId) {
        this.gameId = gameId;
        this.affectedPlayerId = affectedPlayerId;
        roleTypeExecutor = "SLAV";
    }

    public String getRoleTypeExecutor() {
        return roleTypeExecutor;
    }

    public void setRoleTypeExecutor(String roleTypeExecutor) {
        this.roleTypeExecutor = roleTypeExecutor;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getAffectedPlayerId() {
        return affectedPlayerId;
    }

    public void setAffectedPlayerId(int affectedPlayerId) {
        this.affectedPlayerId = affectedPlayerId;
    }
}
