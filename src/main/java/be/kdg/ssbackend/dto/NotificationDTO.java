package be.kdg.ssbackend.dto;


import be.kdg.ssbackend.model.user.NotificationType;

public class NotificationDTO {
    private int notificationId;
    private int targetId;
    private String text;
    private boolean isRead;
    private NotificationType notificatoinType;
    private int userId;

    public NotificationDTO() {
    }

    public NotificationDTO(int notificationId, int targetId, String text, boolean isRead, NotificationType notificatoinType, int userId) {
        this.notificationId = notificationId;
        this.targetId = targetId;
        this.text = text;
        this.isRead = isRead;
        this.notificatoinType = notificatoinType;
        this.userId = userId;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public NotificationType getNotificatoinType() {
        return notificatoinType;
    }

    public void setNotificatoinType(NotificationType notificatoinType) {
        this.notificatoinType = notificatoinType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
