package be.kdg.ssbackend.dto;

public class UserDTO {
    private String email;
    private String username;
    private String newPassword;
    private String oldPassword;
    private String profilePicture;

    public UserDTO() {
    }

    public UserDTO(String email, String username, String newPassword) {
        this.email = email;
        this.username = username;
        this.newPassword = newPassword;
        this.profilePicture = "";
    }

    public UserDTO(String email, String username, String newPassword, String profilePicture) {
        this.email = email;
        this.username = username;
        this.newPassword = newPassword;
        this.profilePicture = profilePicture;
    }

    public UserDTO(String email, String username, String newPassword, String oldPassword, String profilePicture) {
        this.email = email;
        this.username = username;
        this.newPassword = newPassword;
        this.oldPassword = oldPassword;
        this.profilePicture = profilePicture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
