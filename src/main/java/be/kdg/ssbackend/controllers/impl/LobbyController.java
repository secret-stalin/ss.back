package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.GameDTO;
import be.kdg.ssbackend.dto.GameSettingsDTO;
import be.kdg.ssbackend.dto.LobbyDTO;
import be.kdg.ssbackend.dto.UserDTO;
import be.kdg.ssbackend.exceptions.GameNotFoundException;
import be.kdg.ssbackend.exceptions.LobbyFullException;
import be.kdg.ssbackend.exceptions.LobbyNotFoundException;
import be.kdg.ssbackend.exceptions.NotLobbyOwnerException;
import be.kdg.ssbackend.model.game.Lobby;
import be.kdg.ssbackend.services.api.LobbyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api")
public class LobbyController {

    @Value("${url-home}")
    private String homePage;
    @Value("${url-error}")
    private String errorPage;

    private LobbyService lobbyService;

    @Autowired
    public LobbyController(LobbyService lobbyService) {
        this.lobbyService = lobbyService;
    }


    @PostMapping("/lobby/{gameId}")
    public LobbyDTO createLobby(@PathVariable int gameId, @RequestBody GameSettingsDTO settings, Principal principal) {
        return lobbyService.createLobby(gameId, settings, principal.getName());
    }


    @GetMapping("/lobby/{lobbyId}")
    public ResponseEntity<LobbyDTO> getLobby(@PathVariable int lobbyId) {
        LobbyDTO dto;
        try {
            dto = lobbyService.getLobby(lobbyId);
        } catch (LobbyNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(dto);

    }

    @PostMapping("/lobby/start/{lobbyId}")
    public ResponseEntity<GameDTO> startLobby(@PathVariable int lobbyId, Principal principal) {
        GameDTO dto = new GameDTO();
        String user = principal.getName();
        try {
            int gameId = lobbyService.startLobby(lobbyId, user);
            dto.setGameId(gameId);
            return ResponseEntity.ok().body(dto);
        } catch (GameNotFoundException | LobbyNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (NotLobbyOwnerException e) {
            return ResponseEntity.status(403).build();
        }

    }

    @PostMapping("/lobby/{lobbyId}/invite/{userEmail}")
    public ResponseEntity inviteToLobby(@PathVariable int lobbyId, @PathVariable String userEmail) {
        lobbyService.createInvitation(lobbyId, userEmail);
        return ResponseEntity.ok().body("");
    }

    @RequestMapping(value = "/accepted/{invitationId}", method = RequestMethod.GET)
    public ResponseEntity<?> acceptInvitation(@PathVariable int invitationId, HttpServletResponse response) {
        try {
            Lobby lobby = lobbyService.acceptedInvitation(invitationId);
            return ResponseEntity.ok(lobby.getLobbyId());
        } catch (LobbyFullException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @GetMapping("/lobby/joined/{lobbyId}")
    public ResponseEntity<List<UserDTO>> getJoinedUsers(@PathVariable int lobbyId) {
        try {
            List<UserDTO> dtos = lobbyService.getJoined(lobbyId);
            return ResponseEntity.ok().body(dtos);
        } catch (LobbyNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/lobby/public")
    public ResponseEntity<List<LobbyDTO>> getPublicLobbies() {
        List<LobbyDTO> dtos = lobbyService.getPublicLobbies();
        return ResponseEntity.ok().body(dtos);
    }

    @PostMapping("/lobby/public/{lobbyId}")
    public ResponseEntity joinPublicLobby(@PathVariable int lobbyId, Principal principal) {
        try {
            lobbyService.joinPublicLobby(lobbyId, principal.getName());
            return ResponseEntity.ok().build();
        } catch (LobbyFullException e) {
            return ResponseEntity.status(302).build();
        }

    }

    @GetMapping("/lobby/active")
    public ResponseEntity<?> getActiveLobbies(Principal principal) {
        try {
            List<LobbyDTO> dtos = lobbyService.getActiveLobbies(principal.getName());
            return ResponseEntity.ok().body(dtos);
        } catch (NullPointerException e) {
            return new ResponseEntity<>("No lobbies found", HttpStatus.NOT_FOUND);
        }
    }

}
