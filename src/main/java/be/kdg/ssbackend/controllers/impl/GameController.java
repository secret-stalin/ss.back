package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.GameDTO;
import be.kdg.ssbackend.dto.GameStatsDTO;
import be.kdg.ssbackend.dto.ParticipationDTO;
import be.kdg.ssbackend.dto.RoleCardDTO;
import be.kdg.ssbackend.exceptions.GameNotFoundException;
import be.kdg.ssbackend.exceptions.LobbyNotFoundException;
import be.kdg.ssbackend.exceptions.NoAccessException;
import be.kdg.ssbackend.exceptions.ParticipationNotFoundException;
import be.kdg.ssbackend.model.game.Game;
import be.kdg.ssbackend.model.game.Participation;
import be.kdg.ssbackend.model.game.Phase;
import be.kdg.ssbackend.model.game.RoleType;
import be.kdg.ssbackend.services.api.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api")
public class GameController {

    private GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }


    @GetMapping("/game")
    public Game game(HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_CREATED);
        return gameService.createGame();
    }

    @GetMapping("/game/{id}")
    public ResponseEntity<GameDTO> getGame(@PathVariable int id) {
        try {
            Game game = gameService.getGame(id);
            Phase currentPhase = game.getPhases().get(game.getPhases().size() - 1);
            List<Participation> participations = new ArrayList<>(game.getParticipations());
            participations.sort(Comparator.comparing(Participation::getParticipationId));
            GameDTO dto = new GameDTO(game.getGameId(), game.getName(), currentPhase.getRoleTypeVoter(), currentPhase.getState(), participations);
            dto.setActive(game.isActive());
            return ResponseEntity.ok().body(dto);
        } catch (GameNotFoundException | LobbyNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Call this API method to receive your games.
     *
     * @param principal -> user details from the token (e-mail used)
     */
    @GetMapping("/games/active")
    public ResponseEntity<?> getGamesFromUser(Principal principal) {
        try {
            return new ResponseEntity<>(gameService.getActiveGames(principal.getName()), HttpStatus.OK);
        } catch (NullPointerException e) {
            return new ResponseEntity<>("No games found", HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Call this API method to get statistics from the game.
     * Access to the chat will be checked and no access results in unauthorized request.
     * If the game can't be found the call will result in a bad request.
     *
     * @param principal -> user details from the token (e-mail used)
     * @param gameId    -> id from the game
     */
    @GetMapping("/game/stats/{gameId}")
    public ResponseEntity<?> getGameStatistics(Principal principal, @PathVariable int gameId) {
        try {
            if (principal != null) {
                GameStatsDTO gameResultStats = this.gameService.getGameResultStats(gameId, principal.getName());
                return new ResponseEntity<>(gameResultStats, HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Not authorized.", HttpStatus.UNAUTHORIZED);
            }
        } catch (NoAccessException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        } catch (GameNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Call this API method to get all your roles for a specific game.
     * Access to the game will be checked and no access results in unauthorized request.
     * If the game can't be found the call will result in a bad request.
     *
     * @param principal -> user details from the token (e-mail used)
     * @param gameId    -> id from the game
     */
    @GetMapping("/game/my-roles/{gameId}")
    public ResponseEntity<?> getRolesByParticipation(Principal principal, @PathVariable int gameId) {
        try {
            if (principal != null) {
                List<RoleCardDTO> roleCardDTOS = gameService.getRolesByParticipation(principal.getName(), gameId);
                return new ResponseEntity<>(roleCardDTOS, HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Not authorized.", HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Call this API method to get your participation in a specific game.
     * Access to the game will be checked and no access results in unauthorized request.
     * If the game can't be found the call will result in a bad request.
     *
     * @param principal -> user details from the token (e-mail used)
     * @param gameId    -> id from the game
     */
    @GetMapping("/game/participation/{gameId}")
    public ResponseEntity<?> getCurrentParticipation(Principal principal, @PathVariable int gameId) {
        try {
            if (principal != null) {
                ParticipationDTO partDTO = gameService.getCurrentParticipation(principal.getName(), gameId);
                return new ResponseEntity<>(partDTO, HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Not authorized.", HttpStatus.UNAUTHORIZED);
            }
        } catch (GameNotFoundException | ParticipationNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    /**
     * Call this API method to get a target you selected for your special role.
     *
     * @param principal -> user details from the token (e-mail used)
     * @param gameId    -> id from the game
     * @param role -> the role where you want the target from
     */
    @GetMapping("/game/{gameId}/target/{role}")
    public ResponseEntity<?> getTargetFromRole(Principal principal, @PathVariable int gameId, @PathVariable String role) {
        try {
            return new ResponseEntity<>(this.gameService.getTargetFromRole(principal.getName(), gameId, RoleType.valueOf(role)), HttpStatus.OK);
        } catch (NullPointerException | NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

}
