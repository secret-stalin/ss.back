package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.NotificationDTO;
import be.kdg.ssbackend.dto.NotificationSettingsDTO;
import be.kdg.ssbackend.exceptions.NoAccessException;
import be.kdg.ssbackend.model.user.NotificationSettings;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.services.api.NotificationService;
import be.kdg.ssbackend.services.api.NotificationsSettingsService;
import be.kdg.ssbackend.services.api.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api")
public class NotificationController {

    private final NotificationService notificationService;
    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);
    private final NotificationsSettingsService settingsService;
    private final UserService userService;

    public NotificationController(NotificationService notificationService, NotificationsSettingsService settingsService, UserService userService) {
        this.notificationService = notificationService;
        this.settingsService = settingsService;
        this.userService = userService;
    }

    /**
     * Call this API method to get all the notifications from a user with his principal.
     * If no notifications where found an empty OK response will be given.
     *
     * @param principal -> user details from the token (e-mail used)
     */
    @GetMapping("/notifications")
    public ResponseEntity<List<NotificationDTO>> getNotifications(Principal principal) {
        try {
            List<NotificationDTO> notifications = notificationService.getNotifications(principal.getName());
            return new ResponseEntity<>(notifications, HttpStatus.OK);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    /**
     * Call this API method to see how many new notifications you have.
     *
     * @param principal -> user details from the token (e-mail used)
     */
    @GetMapping("/notifications/count")
    public ResponseEntity<Integer> getNotificationCount(Principal principal) {
        try {
            int notificationCount = notificationService.getUnreadNotificationCount(principal.getName());
            return new ResponseEntity<>(notificationCount, HttpStatus.OK);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(0, HttpStatus.OK);
        }
    }

    /**
     * Call this API method when a user reads a notification.
     * If the user has no access to the notification or the notification can't be found an error will be logged.
     *
     * @param notificationId -> id from the notification that is read.
     * @param principal      -> user details from the token (e-mail used)
     */
    @PostMapping("/notification/{notificationId}")
    public void readNotification(Principal principal, @PathVariable int notificationId) {
        try {
            notificationService.readNotification(principal.getName(), notificationId);
        } catch (NoAccessException | NullPointerException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @GetMapping("/notification/settings/")
    public NotificationSettingsDTO getSettings(Principal principal) {
        try {
            User user = userService.findByEmail(principal.getName());
            NotificationSettings notificationSettings = settingsService.readSettings(user.getUserId());
            return new NotificationSettingsDTO(notificationSettings.isIgnoreFriendRequest(), notificationSettings.isIgnoreInvite(), notificationSettings.isIgnoreEndPhase(), notificationSettings.isIgnoreYourTurn());
        } catch (NullPointerException e) {
            return new NotificationSettingsDTO();
        }
    }

    @PostMapping("/notification/settings/")
    public void setNotificationSettings(Principal principal, @RequestBody NotificationSettingsDTO settings) {
        settingsService.setSettings(principal.getName(), settings);
    }
}

