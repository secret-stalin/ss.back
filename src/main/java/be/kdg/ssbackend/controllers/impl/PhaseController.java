package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.VoteDTO;
import be.kdg.ssbackend.dto.VoteResultDTO;
import be.kdg.ssbackend.exceptions.GameNotFoundException;
import be.kdg.ssbackend.exceptions.ParticipationNotFoundException;
import be.kdg.ssbackend.model.game.RoleType;
import be.kdg.ssbackend.services.api.PhaseService;
import be.kdg.ssbackend.services.api.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PhaseController {
    private final PhaseService phaseService;
    private final UserService userService;

    private static final Logger LOGGER = LoggerFactory.getLogger(PhaseController.class);

    public PhaseController(PhaseService phaseService, UserService userService) {
        this.phaseService = phaseService;
        this.userService = userService;
    }

    /**
     * Call this API method to register a vote.
     *
     * @param principal -> user details from the token (e-mail used)
     * @param body      -> body will have a game id and the affected participation id
     */
    @PostMapping("/vote")
    public void vote(Principal principal, @RequestBody VoteDTO body) {
        try {
            LOGGER.info("registering vote" + body.getRoleTypeExecutor());
            this.phaseService.addAction(body.getGameId(), userService.findByEmail(principal.getName()), body.getAffectedPlayerId(), RoleType.valueOf(body.getRoleTypeExecutor()));
        } catch (GameNotFoundException | ParticipationNotFoundException e) {
            LOGGER.error(e.getMessage());
        }

    }

    /**
     * Call this API method to get the vote results of the current phase of a given game
     *
     * @param principal -> user details from the token (e-mail used)
     * @param id        -> id of a game
     */
    @GetMapping("/voteResults/{id}")
    public ResponseEntity<List<VoteResultDTO>> getVoteResults(Principal principal, @PathVariable int id) {
        try {
            LOGGER.info("getting vote results");
            List<VoteResultDTO> dtos = phaseService.getVoteResults(userService.findByEmail(principal.getName()), id);
            return ResponseEntity.ok().body(dtos);
        } catch (GameNotFoundException | ParticipationNotFoundException e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }
}
