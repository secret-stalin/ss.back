package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.ReplayDTO;
import be.kdg.ssbackend.exceptions.NoAccessException;
import be.kdg.ssbackend.services.api.ReplayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/api")
public class ReplayController {

    private ReplayService replayService;

    @Autowired
    public ReplayController(ReplayService replayService) {
        this.replayService = replayService;
    }

    @GetMapping("/replay/{gameId}")
    public ResponseEntity<ReplayDTO> getReplay(@PathVariable int gameId, Principal principal) {
        try {
            ReplayDTO dto = replayService.getReplay(gameId, principal.getName());
            return ResponseEntity.ok().body(dto);
        } catch (NoAccessException e) {
            return ResponseEntity.status(203).build();
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
