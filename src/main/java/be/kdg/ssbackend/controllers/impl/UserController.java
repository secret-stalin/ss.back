package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.UserDTO;
import be.kdg.ssbackend.dto.UserStatDTO;
import be.kdg.ssbackend.exceptions.*;
import be.kdg.ssbackend.model.user.User;
import be.kdg.ssbackend.services.api.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api")
public class UserController {

    @Value("${url-home}")
    private String homePage;
    @Value("${url-error}")
    private String errorPage;

    private final UserService userService;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Call this API method to register a user.
     * This API call will also send a mail to the user for registration confirmation.
     * If the e-mail or username is already taken the response will be a bad request
     *
     * @param accountDto -> DTO containing e-mail, password & username (profile picture currently not in use)
     */
    @PostMapping("/registration")
    public ResponseEntity registerUserAccount(@RequestBody UserDTO accountDto) {
        LOGGER.info("Register user" + accountDto.getEmail());
        try {
            userService.createUserAccount(accountDto, true);
        } catch (UserAlreadyExistException | EmailFormatException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body("");
    }

    /**
     * When clicked on the registration link in the registration confirmation e-mail this method will be triggered.
     * Access to the chat will be checked and no access results in a bad request.
     *
     * @param verificationToken -> A registration token that was given with the e-mail valid for 24hours
     * @param response          -> the incoming request used to redirect to front end
     */
    @RequestMapping(value = "/registration/confirm", method = RequestMethod.GET)
    public void confirmRegistration(@RequestParam("token") String verificationToken, HttpServletResponse response) throws IOException {
        try {
            userService.checkToken(verificationToken);
            response.sendRedirect(homePage);
        } catch (InvalidTokenException ex) {
            response.sendRedirect(errorPage);
        }
    }

    /**
     * Call this API method to resend e-mail confirmation.
     *
     * @param email -> email where the confirmation mail should go to (must match a users email)
     */
    @RequestMapping(value = "/registration/resend", method = RequestMethod.POST)
    public void resendVerification(@RequestBody String email) {
        try {
            userService.resendVerificationMail(email);
            LOGGER.info("confirmation e-mail has been resend");
        } catch (EmailFormatException e) {
            LOGGER.error(e.getMessage());
        }
    }


    @GetMapping("/user/{userEmail}")
    public ResponseEntity<List<UserDTO>> findFriends(@PathVariable String userEmail) {
        List<User> users = userService.findFriendsFromUser(userEmail);
        List<UserDTO> userDTOS = new ArrayList<>();
        for (User user : users) {
            UserDTO dto = new UserDTO();
            dto.setUsername(user.getUsername());
            dto.setEmail(user.getEmail());
            userDTOS.add(dto);
        }
        return ResponseEntity.ok().body(userDTOS);
    }


    /**
     * Call this API method to load all the users with their user names and emails.
     */
    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        List<User> users = userService.findAllUsers();
        List<UserDTO> dtos = new ArrayList<>();
        for (User user : users) {
            dtos.add(userService.userToDTO(user));
        }
        return ResponseEntity.ok().body(dtos);
    }

    /**
     * Call this API method to search a user by his username.
     *
     * @param username -> username from the user to search
     */
    @GetMapping("/user/profile/{username}")
    public ResponseEntity<?> getUserByUsername(@PathVariable String username) {
        try {
            return new ResponseEntity<>(userService.userToDTO(userService.findByUsername(username)), HttpStatus.OK);
        } catch (NullPointerException e) {
            return new ResponseEntity<>("User" + username + " not found", HttpStatus.NOT_FOUND);
        }

    }

    /**
     * Call this API method to get a user by his id.
     *
     * @param userId -> id from the user
     */
    @GetMapping("/user/id/{userId}")
    public ResponseEntity<?> getUserById(@PathVariable int userId) {
        try {
            return new ResponseEntity<>(userService.userToDTO(userService.findById(userId)), HttpStatus.OK);
        } catch (NullPointerException | NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Call this API method to see if the user is your friend.
     *
     * @param principal -> user details from the token (e-mail used)
     * @param username  -> username from the user that you want to check if he is your friend
     */
    @GetMapping("/user/is-friend/{username}")
    public ResponseEntity<?> isFriendFromUser(Principal principal, @PathVariable String username) {
        try {
            return new ResponseEntity<>(userService.isFriendFrom(principal.getName(), username), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Call this API method to see if you have a friend request from a specific user.
     *
     * @param principal      -> user details from the token (e-mail used)
     * @param usernameSender -> username from the sender of the friend request
     */
    @GetMapping("/user/has-friend-request/{usernameSender}")
    public ResponseEntity<?> hasFriendRequestFromUser(Principal principal, @PathVariable String usernameSender) {
        try {
            return new ResponseEntity<>(userService.hasFriendRequestFromUser(principal.getName(), usernameSender, true), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Call this API method to see if a specific user has a friend request pending from you.
     *
     * @param principal        -> user details from the token (e-mail used)
     * @param usernameAcceptor -> username from the user that can have a friend request pending from you
     */
    @GetMapping("/user/has-friend-request-pending/{usernameAcceptor}")
    public ResponseEntity<?> hasFriendRequestPendingToUser(Principal principal, @PathVariable String usernameAcceptor) {
        try {
            return new ResponseEntity<>(userService.hasFriendRequestFromUser(principal.getName(), usernameAcceptor, false), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Call this API method to send a friend request to a specific user.
     * If the user is already your friend the result will be a bad request.
     *
     * @param principal -> user details from the token (e-mail used)
     * @param userName  -> username from user to send to
     */
    @PostMapping("/user/friend-request/send")
    public ResponseEntity<?> sendFriendRequest(Principal principal, @RequestBody String userName) {
        try {
            userService.sendFriendRequest(principal.getName(), userName);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (AlreadyFriendException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Call this API method to accept a friend request from a specific user.
     *
     * @param principal -> user details from the token (e-mail used)
     * @param userName  -> username from user that send the friend request
     */
    @PostMapping("/user/friend-request/accept")
    public ResponseEntity<?> acceptFriendRequest(Principal principal, @RequestBody String userName) {
        try {
            userService.acceptFriendRequest(principal.getName(), userName);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Call this API method to decline a friend request from a specific user.
     *
     * @param principal -> user details from the token (e-mail used)
     * @param userName  -> username from user that send the friend request
     */
    @PostMapping("/user/friend-request/decline")
    public ResponseEntity<?> declineFriendRequest(Principal principal, @RequestBody String userName) {
        try {
            userService.declineFriendRequest(principal.getName(), userName);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Call this API method to remove a friend from your friend list.
     * If the user is not your friend the response will be a bad request.
     *
     * @param principal -> user details from the token (e-mail used)
     * @param userName  -> username from user that is your friend
     */
    @PostMapping("/user/remove-friend")
    public ResponseEntity<?> removeFriend(Principal principal, @RequestBody String userName) {
        try {
            userService.removeFriend(principal.getName(), userName);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/user/")
    public ResponseEntity<?> getUser(Principal principal) {
        try {
            User user = userService.findByEmail(principal.getName());
            UserDTO dto = userService.userToDTO(user);
            return ResponseEntity.ok().body(dto);
        } catch (NullPointerException e) {
            return ResponseEntity.badRequest().body("Invalid token");
        }
    }

    /**
     * Call this API method to change your profile.
     * If the user changes his password and the old password doesn't match the response will be a bad request.
     * If the user upload a profile picture and the file is not an image the response will be a bad request.
     *
     * @param user -> user with changes
     */
    @PostMapping("/user/change/")
    public ResponseEntity saveUser(@RequestBody UserDTO user) {
        try {
            this.userService.changeUserDetails(user);
            return ResponseEntity.ok().body("");
        } catch (IncorrectPasswordException | WrongFileTypeException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    /**
     * Call this API method to get a user his statistics.
     * If the user doesn't exist the response will be a bad request.
     *
     * @param principal -> user details from the token (e-mail used)
     * @param username  -> username from user the user that you visit
     */
    @GetMapping("/user/stats/{username}")
    public ResponseEntity<?> saveUser(Principal principal, @PathVariable String username) {
        try {
            UserStatDTO userGameStats = this.userService.getUserGameStats(username);
            return ResponseEntity.ok().body(userGameStats);
        } catch (NullPointerException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }


    @GetMapping("/user/fcmtoken/{token}")
    public ResponseEntity<String> setFireBaseToken(Principal principal, @PathVariable String token) {
        try {
            this.userService.setFireBaseToken(principal.getName(), token);
            return ResponseEntity.ok().body("");
        } catch (NoAccessException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
