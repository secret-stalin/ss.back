package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.MessageDTO;
import be.kdg.ssbackend.exceptions.NoChatAccessException;
import be.kdg.ssbackend.services.api.ChatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;

@Controller
public class ChatController {

    private final ChatService chatService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ChatController.class);

    public ChatController(ChatService chatService) {
        this.chatService = chatService;
    }

    /**
     * Call this API method to send a message to a specific User.
     * Access to the chat will be checked and no access results in a error log.
     * @param principal -> user details from the token (e-mail used)
     * @param message -> a DTO object containing the text message (without user) and timestamp
     * @param chatId -> id for the destination of the message
     */
    @MessageMapping("/send/message/{chatId}")
    public void sendToSpecificUser(Principal principal, @Payload MessageDTO message, @DestinationVariable int chatId) {
        LOGGER.info("Message received");
        try {
            this.chatService.sendMessage(principal.getName(), message, chatId);
        } catch (NoChatAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Call this API method to load previous messages in a chat.
     * Access to the chat will be checked and no access results in a bad request.
     * @param principal -> user details from the token (e-mail used)
     * @param chatId -> id from chat you want messages from
     */
    @GetMapping("/api/chat/getMessages/{chatId}")
    public ResponseEntity<?> getExistingChatMessages(@PathVariable("chatId") int chatId, Principal principal) {
        try {
            return new ResponseEntity<>(this.chatService.getMessagesFromChat(principal.getName(), chatId), HttpStatus.OK);
        } catch (NoChatAccessException | NullPointerException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * Call this API method to get an overview from a user his chats.
     * Access to the chat will be checked and no access results in a unauthorized request.
     * @param principal -> user details from the token (e-mail used)
     */
    @GetMapping("/api/chat/getChats")
    public ResponseEntity<?> getChatsFromUser(Principal principal) {
        try {
            return new ResponseEntity<>(chatService.getChatsFromUser(principal.getName()).toArray(), HttpStatus.OK);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }
}
