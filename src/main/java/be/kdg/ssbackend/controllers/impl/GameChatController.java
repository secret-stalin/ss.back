package be.kdg.ssbackend.controllers.impl;

import be.kdg.ssbackend.dto.GameChatDTO;
import be.kdg.ssbackend.dto.MessageDTO;
import be.kdg.ssbackend.exceptions.NoChatAccessException;
import be.kdg.ssbackend.exceptions.ObjectNotFoundException;
import be.kdg.ssbackend.services.api.GameChatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;
import java.util.List;

@Controller
public class GameChatController {

    private final GameChatService gameChatService;
    private static final Logger LOGGER = LoggerFactory.getLogger(GameChatController.class);

    public GameChatController(GameChatService gameChatService) {
        this.gameChatService = gameChatService;
    }

    /**
     * Call this API method to load the GameChats from a game.
     * Access to the chat will be checked and no access results in a bad request.
     * @param principal -> user details from the token (e-mail used)
     * @param gameId -> id from the game you want chats (with messages included) from
     */
    @GetMapping("/api/game/chats/{gameId}")
    public ResponseEntity<?> getChatsFromGame(Principal principal, @PathVariable int gameId) {
        try {
            List<GameChatDTO> gameChatDTOS = this.gameChatService.getChatsFromGame(principal.getName(), gameId);
            return new ResponseEntity<>(gameChatDTOS, HttpStatus.OK);
        } catch (ObjectNotFoundException | NullPointerException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Call this API method to send a message to a specific GameChat from a game.
     * Access to the chat will be checked and no access results in an error log (message ignored).
     * @param principal -> user details from the token (e-mail used)
     * @param chatId -> id for destination of the message
     */
    @MessageMapping("/gamechat/send/message/{chatId}")
    public void sendToGameChat(Principal principal, @Payload MessageDTO message, @DestinationVariable int chatId) {
        LOGGER.info("message received");
        try {
            this.gameChatService.sendMessage(chatId, message, principal.getName());
        } catch (NoChatAccessException | ObjectNotFoundException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
