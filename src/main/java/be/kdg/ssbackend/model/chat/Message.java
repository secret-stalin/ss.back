package be.kdg.ssbackend.model.chat;

import be.kdg.ssbackend.model.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table
public class Message {
    @Id
    @GeneratedValue
    private int messageId;
    private LocalDateTime time;
    private String text;
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private User sender;
    @ManyToOne(targetEntity = Chat.class, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "CHAT_ID", nullable = false)
    private Chat chat;

    public Message() {
        this.time = LocalDateTime.now();
        this.chat = new Chat();
        this.sender = new User();
    }

    public Message(String text, User sender, Chat chat) {
        this.text = text;
        this.sender = sender;
        this.time = LocalDateTime.now();
        this.chat = chat;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }
}
