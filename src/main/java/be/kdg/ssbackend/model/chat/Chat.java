package be.kdg.ssbackend.model.chat;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "CHAT")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int chatId;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Message.class, cascade = CascadeType.ALL, mappedBy = "chat")
    private Set<Message> messages;

    public Chat() {
        this.messages = new HashSet<>();
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }


}
