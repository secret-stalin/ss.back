package be.kdg.ssbackend.model.chat;

import be.kdg.ssbackend.model.user.User;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("PRIVATE_CHAT")
public class PrivateChat extends Chat {
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "USER_CHAT",
            joinColumns = @JoinColumn(name = "CHAT_ID", referencedColumnName = "chatId"),
            inverseJoinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "userId"))
    private List<User> users;

    public PrivateChat() {
        this.users = new ArrayList<>();
    }

    public PrivateChat(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }


}
