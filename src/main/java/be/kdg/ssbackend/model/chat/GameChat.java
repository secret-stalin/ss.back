package be.kdg.ssbackend.model.chat;

import be.kdg.ssbackend.model.game.Game;
import be.kdg.ssbackend.model.game.RoleType;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@DiscriminatorValue("GAME_CHAT")
public class GameChat extends Chat {

    private String name;
    @Enumerated(EnumType.STRING)
    private RoleType access;
    @ManyToOne(targetEntity = Game.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "GAME_ID", nullable = true)
    @JsonBackReference
    private Game game;

    public GameChat() {
        this.access = RoleType.SLAV;
        this.game = new Game();
    }

    public GameChat(String name, RoleType access, Game game) {
        this.name = name;
        this.access = access;
        this.game = game;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleType getAccess() {
        return access;
    }

    public void setAccess(RoleType access) {
        this.access = access;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }



}
