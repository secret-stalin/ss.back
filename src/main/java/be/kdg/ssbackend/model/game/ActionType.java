package be.kdg.ssbackend.model.game;

public enum ActionType {
    VOTE, KILL, HUNTER_TARGET, MAYOR_CHOICE
}
