package be.kdg.ssbackend.model.game;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Phase {
    @Id
    @GeneratedValue
    private int phaseId;
    private int phaseNumber;
    private RoleType roleTypeVoter;
    private State state;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Action.class, cascade = CascadeType.ALL, mappedBy = "phase")
    private List<Action> actions;
    @ManyToOne(targetEntity = Game.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "GAME_ID", nullable = false)
    private Game game;

    public Phase() {
        this.actions = new ArrayList<>();
        this.game = new Game();
    }

    public Phase(int phaseNumber, State state, Game game) {
        this.phaseNumber = phaseNumber;
        this.state = state;
        this.game = game;
        this.actions = new ArrayList<>();
    }

    public int getPhaseId() {
        return phaseId;
    }

    public void setPhaseId(int phaseId) {
        this.phaseId = phaseId;
    }

    public int getPhaseNumber() {
        return phaseNumber;
    }

    public void setPhaseNumber(int phaseNumber) {
        this.phaseNumber = phaseNumber;
    }

    public RoleType getRoleTypeVoter() {
        return roleTypeVoter;
    }

    public void setRoleTypeVoter(RoleType roleTypeVoter) {
        this.roleTypeVoter = roleTypeVoter;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
}
