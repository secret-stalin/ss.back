package be.kdg.ssbackend.model.game;

import be.kdg.ssbackend.model.user.User;

import javax.persistence.*;


@Entity
@Table
public class Invitation {

    @Id
    @GeneratedValue
    private int invitationId;
    private boolean accepted;
    private boolean declined;
    @ManyToOne(targetEntity = Lobby.class,cascade = CascadeType.ALL, fetch = FetchType.EAGER,optional = false)
    private Lobby lobby;
    @ManyToOne(targetEntity = User.class,cascade = CascadeType.ALL,fetch = FetchType.EAGER,optional = false)
    private User invitee;

    public Invitation() {
    }

    public Invitation(Lobby lobby, User invitee) {
        this.lobby = lobby;
        this.invitee = invitee;
        this.accepted = false;
        this.declined = false;
    }

    public int getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(int invitationId) {
        this.invitationId = invitationId;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isDeclined() {
        return declined;
    }

    public void setDeclined(boolean declined) {
        this.declined = declined;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public void setLobby(Lobby lobby) {
        this.lobby = lobby;
    }

    public User getInvitee() {
        return invitee;
    }

    public void setInvitee(User invitee) {
        this.invitee = invitee;
    }
}
