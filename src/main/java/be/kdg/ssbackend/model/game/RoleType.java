package be.kdg.ssbackend.model.game;

public enum RoleType {
    SLAV, STALIN, HUNTER, MAYOR
}
