package be.kdg.ssbackend.model.game;

public enum  EventType {
    CHAT, VOTE, KILL, ENDPHASE
}
