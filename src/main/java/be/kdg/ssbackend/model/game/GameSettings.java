package be.kdg.ssbackend.model.game;

import be.kdg.ssbackend.dto.UserDTO;

import java.time.Duration;
import java.util.List;

public class GameSettings {
    private String name;
    private Duration day;
    private Duration night;
    private List<RoleType> roles;
    private boolean isPrivate;
    private int size;
    private UserDTO host;


    public GameSettings(){

    }


    public GameSettings(String name, UserDTO host, Duration day, Duration night, List<RoleType> roles, boolean isPrivate, int size) {
        this.name = name;
        this.day = day;
        this.night = night;
        this.roles = roles;
        this.isPrivate = isPrivate;
        this.size=size;
        this.host=host;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Duration getDay() {
        return day;
    }

    public void setDay(Duration day) {
        this.day = day;
    }

    public Duration getNight() {
        return night;
    }

    public void setNight(Duration night) {
        this.night = night;
    }

    public List<RoleType> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleType> roles) {
        this.roles = roles;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public UserDTO getHost() {
        return host;
    }

    public void setHost(UserDTO host) {
        this.host = host;
    }
}
