package be.kdg.ssbackend.model.game;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table
public class Role {
    @Id
    @GeneratedValue
    private int roleId;
    @ManyToOne(targetEntity = Participation.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "PARTICIPATION_ID", nullable = false)
    @JsonBackReference
    private Participation participation;
    @ManyToOne(targetEntity = Card.class, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "CARD_ID", nullable = false)
    private Card card;

    public Role() {
        this.participation = new Participation();
        this.card = new Card();
    }

    public Role(Participation participation, Card card) {
        this.participation = participation;
        this.card = card;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public Participation getParticipation() {
        return participation;
    }

    public void setParticipation(Participation participationl) {
        this.participation = participationl;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
