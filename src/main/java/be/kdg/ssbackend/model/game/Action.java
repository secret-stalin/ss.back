package be.kdg.ssbackend.model.game;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table
public class Action {
    @Id
    @GeneratedValue
    private int actionId;
    private LocalDateTime time;
    private ActionType actionType;
    @ManyToOne(targetEntity = Participation.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "EXECUTOR_ID")
    private Participation executor;
    @ManyToOne(targetEntity = Participation.class, fetch = FetchType.EAGER)
    private Participation affected;
    @ManyToOne(targetEntity = Phase.class, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "PHASE_ID", nullable = false)
    private Phase phase;

    public Action() {
        time = LocalDateTime.now();
        this.affected = new Participation();
        this.phase = new Phase();
    }

    public Action(ActionType actionType, Participation executor, Participation affected) {
        this.actionType = actionType;
        this.executor = executor;
        this.affected = affected;
        this.time = LocalDateTime.now();
        this.phase = new Phase();
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public Participation getExecutor() {
        return executor;
    }

    public void setExecutor(Participation executor) {
        this.executor = executor;
    }

    public Participation getAffected() {
        return affected;
    }

    public void setAffected(Participation affected) {
        this.affected = affected;
    }

    public Phase getPhase() {
        return phase;
    }

    public void setPhase(Phase phase) {
        this.phase = phase;
    }
}
