package be.kdg.ssbackend.model.game;

import be.kdg.ssbackend.model.user.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Lobby {

    @Id
    @GeneratedValue
    private int LobbyId;
    private boolean isPrivate;
    private boolean isClosed;
    private int size;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(targetEntity = Card.class, cascade = CascadeType.ALL)
    @JoinTable(name = "LOBBY_ROLES",
            joinColumns = @JoinColumn(name = "LOBBY_ID"),
            inverseJoinColumns = @JoinColumn(name = "CARD_ID"))
    private List<Card> rolesIncluded;

    @OneToOne(targetEntity = Game.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    private Game game;

    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    @JsonBackReference
    private User createdBy;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Invitation.class, cascade = CascadeType.ALL, mappedBy = "lobby")
    private List<Invitation> invitations;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(targetEntity = User.class, cascade = CascadeType.ALL)
    @JoinTable(name="LOBBY_JOINED",
            joinColumns=
            @JoinColumn(name="LOBBY_ID"),
            inverseJoinColumns=
            @JoinColumn(name="USER_ID")
    )
    private Set<User> joinedUsers;

    public Lobby() {

    }

    public Lobby(boolean isPrivate, int size){
        this.isPrivate = isPrivate;
        this.size = size;
        isClosed = false;
        invitations = new ArrayList<>();
        joinedUsers = new HashSet<>();


    }

    public int getLobbyId() {
        return LobbyId;
    }

    public void setLobbyId(int lobbyId) {
        LobbyId = lobbyId;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public List<Invitation> getInvitations() {
        return invitations;
    }

    public void setInvitations(List<Invitation> invitations) {
        this.invitations = invitations;
    }

    public Set<User> getJoinedUsers() {
        return joinedUsers;
    }

    public void setJoinedUsers(Set<User> joinedUsers) {
        this.joinedUsers = joinedUsers;
    }

    public List<Card> getRolesIncluded() {
        return rolesIncluded;
    }

    public void setRolesIncluded(List<Card> rolesIncluded) {
        this.rolesIncluded = rolesIncluded;
    }
}
