package be.kdg.ssbackend.model.game;

import be.kdg.ssbackend.model.user.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table
public class Participation {
    @Id
    @GeneratedValue
    private int participationId;
    private boolean isAlive;
    private boolean isMayor;
    private String colorCode;
    @ManyToOne(targetEntity = Game.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "GAME_ID", nullable = false)
    @JsonBackReference
    private Game game;
    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Role.class, cascade = CascadeType.ALL, mappedBy = "participation")
    @JsonManagedReference
    private Set<Role> roles;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Action.class, cascade = CascadeType.ALL, mappedBy = "executor")
    private List<Action> actionsDone;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "affected")
    private List<Action> actionsDoneTo;

    public Participation() {
        this.isAlive = true;
        this.isMayor = false;
        this.roles = new HashSet<>();
        this.user = new User();
        this.game = new Game();
        this.actionsDone = new ArrayList<>();
        this.actionsDoneTo = new ArrayList<>();
    }

    public Participation(Game game, User user, Set<Role> roles) {
        this.game = game;
        this.user = user;
        this.roles = roles;
        this.isAlive = true;
        this.isMayor = false;
        this.actionsDone = new ArrayList<>();
        this.actionsDoneTo = new ArrayList<>();
    }

    public int getParticipationId() {
        return participationId;
    }

    public void setParticipationId(int participationId) {
        this.participationId = participationId;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public boolean isMayor() {
        return isMayor;
    }

    public void setMayor(boolean mayor) {
        isMayor = mayor;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public List<Action> getActionsDone() {
        return actionsDone;
    }

    public void setActionsDone(List<Action> actionsDone) {
        this.actionsDone = actionsDone;
    }

    public List<Action> getActionsDoneTo() {
        return actionsDoneTo;
    }

    public void setActionsDoneTo(List<Action> actionsDoneTo) {
        this.actionsDoneTo = actionsDoneTo;
    }
}
