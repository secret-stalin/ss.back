package be.kdg.ssbackend.model.game;

import be.kdg.ssbackend.model.chat.GameChat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int gameId;
    private String name;
    private Duration dayDuration;
    private Duration nightDuration;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Phase.class, cascade = CascadeType.ALL, mappedBy = "game")
    private List<Phase> phases;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Participation.class, cascade = CascadeType.ALL, mappedBy = "game")
    @JsonManagedReference
    private Set<Participation> participations;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = GameChat.class, cascade = CascadeType.ALL, mappedBy = "game")
    @JsonManagedReference
    private Set<GameChat> gameChats;
    private boolean isActive;

    private RoleType winningRole;

    public Game() {
        this.participations = new HashSet<>();
        this.gameChats = new HashSet<>();
        this.phases = new ArrayList<>();
        this.isActive = true;
    }

    public Game(String name,Duration dayDuration, Duration nightDuration, Set<Participation> participations, Set<GameChat> gameChats, boolean isActive) {
        this.name = name;
        this.dayDuration = dayDuration;
        this.nightDuration = nightDuration;
        this.participations = participations;
        this.gameChats = gameChats;
        this.phases = new ArrayList<>();
        this.isActive = isActive;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Duration getDayDuration() {
        return dayDuration;
    }

    public void setDayDuration(Duration dayDuration) {
        this.dayDuration = dayDuration;
    }

    public Duration getNightDuration() {
        return nightDuration;
    }

    public void setNightDuration(Duration nightDuration) {
        this.nightDuration = nightDuration;
    }

    public List<Phase> getPhases() {
        return phases;
    }

    public void setPhases(List<Phase> phases) {
        this.phases = phases;
    }

    public Set<Participation> getParticipations() {
        return participations;
    }

    public void setParticipations(Set<Participation> participations) {
        this.participations = participations;
    }

    public Set<GameChat> getGameChats() {
        return gameChats;
    }

    public void setGameChats(Set<GameChat> gameChats) {
        this.gameChats = gameChats;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public RoleType getWinningRole() {
        return winningRole;
    }

    public void setWinningRole(RoleType winningRole) {
        this.winningRole = winningRole;
    }
}
