package be.kdg.ssbackend.model.game;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Card {
    @Id
    @GeneratedValue
    private int cardId;
    private RoleType roleType;
    private String description;
    private String image;
    private boolean hasNightActivity;

    public Card() {

    }

    public Card(RoleType roleType, String description, String image, boolean hasNightActivity) {
        this.roleType = roleType;
        this.description = description;
        this.image = image;
        this.hasNightActivity = hasNightActivity;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isHasNightActivity() {
        return hasNightActivity;
    }

    public void setHasNightActivity(boolean hasNightActivity) {
        this.hasNightActivity = hasNightActivity;
    }
}
