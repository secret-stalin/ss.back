package be.kdg.ssbackend.model.user;

import javax.persistence.*;

@Table
@Entity
public class NotificationSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int notificationId;
    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER, optional = false)
    private User user;
    private boolean ignoreFriendRequest;
    private boolean ignoreInvite;
    private boolean ignoreEndPhase;
    private boolean ignoreYourTurn;

    public NotificationSettings() {
        this.ignoreFriendRequest = false;
        this.ignoreInvite = false;
        this.ignoreEndPhase = false;
        this.ignoreYourTurn = false;
    }

    public NotificationSettings(boolean IgnoreFriendRequest, boolean ignoreInvite, boolean ignoreEndPhase, boolean ignoreYourTurn) {
        this.ignoreFriendRequest = IgnoreFriendRequest;
        this.ignoreInvite = ignoreInvite;
        this.ignoreEndPhase = ignoreEndPhase;
        this.ignoreYourTurn = ignoreYourTurn;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isIgnoreFriendRequest() {
        return ignoreFriendRequest;
    }

    public void setIgnoreFriendRequest(boolean ignoreFriendRequest) {
        this.ignoreFriendRequest = ignoreFriendRequest;
    }

    public boolean isIgnoreInvite() {
        return ignoreInvite;
    }

    public void setIgnoreInvite(boolean ignoreInvite) {
        this.ignoreInvite = ignoreInvite;
    }

    public boolean isIgnoreEndPhase() {
        return ignoreEndPhase;
    }

    public void setIgnoreEndPhase(boolean ignoreEndPhase) {
        this.ignoreEndPhase = ignoreEndPhase;
    }

    public boolean isIgnoreYourTurn() {
        return ignoreYourTurn;
    }

    public void setIgnoreYourTurn(boolean ignoreYourTurn) {
        this.ignoreYourTurn = ignoreYourTurn;
    }
}
