package be.kdg.ssbackend.model.user;

import javax.persistence.*;

@Table
@Entity
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int notificationId;
    private int targetId;
    private String text;
    private boolean isRead;
    private NotificationType notificatoinType;
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER, optional = false)
    private User user;

    public Notification() {
        this.user = new User();
    }

    public Notification(int targetId, String text, boolean isRead, NotificationType notificatoinType, User user) {
        this.text = text;
        this.isRead = isRead;
        this.notificatoinType = notificatoinType;
        this.targetId = targetId;
        this.user = user;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isRead() {
        return isRead;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public NotificationType getNotificatoinType() {
        return notificatoinType;
    }

    public void setNotificatoinType(NotificationType notificatoinType) {
        this.notificatoinType = notificatoinType;
    }
}
