package be.kdg.ssbackend.model.user;

import be.kdg.ssbackend.model.chat.Message;
import be.kdg.ssbackend.model.chat.PrivateChat;
import be.kdg.ssbackend.model.game.Invitation;
import be.kdg.ssbackend.model.game.Lobby;
import be.kdg.ssbackend.model.game.Participation;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Entity
@Table
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;
    @Column(name = "enabled")
    private boolean enabled;
    private String email;
    private String username;
    @JsonIgnore
    private String password;
    @Lob
    private String profilePicture;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "USER_FRIEND",
            joinColumns = @JoinColumn(name = "USER1_ID"),
            inverseJoinColumns = @JoinColumn(name = "USER2_ID"))
    private List<User> friends;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(mappedBy = "users", cascade = CascadeType.PERSIST)
    private List<PrivateChat> chats;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Participation.class, cascade = CascadeType.ALL, mappedBy = "user")
    private List<Participation> participations;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Message.class, cascade = CascadeType.ALL, mappedBy = "sender")
    private List<Message> messages;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Lobby.class, cascade = CascadeType.ALL, mappedBy = "createdBy")
    @JsonManagedReference
    private List<Lobby> createdLobies;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Invitation.class, cascade = CascadeType.ALL, mappedBy = "invitee")
    private List<Invitation> invitations;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(mappedBy = "joinedUsers")
    private List<Lobby> joinedLobbies;
    @MapKey(name = "notificationId")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Notification.class, cascade = CascadeType.ALL, mappedBy = "user")
    private Map<Integer, Notification> notifications;
    private String deviceFireBaseToken;

    public User() {
        this.friends = new ArrayList<>();
        this.chats = new ArrayList<>();
        this.participations = new ArrayList<>();
        this.messages = new ArrayList<>();
        enabled = false;
        this.notifications = new ConcurrentHashMap<>();
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", enabled=" + enabled +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                '}';
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
        this.friends = new ArrayList<>();
        this.chats = new ArrayList<>();
        this.participations = new ArrayList<>();
        this.messages = new ArrayList<>();
        enabled = false;
        this.notifications = new ConcurrentHashMap<>();
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String hashedPassword) {
        this.password = hashedPassword;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public List<PrivateChat> getChats() {
        return chats;
    }

    public void setChats(List<PrivateChat> chats) {
        this.chats = chats;
    }

    public List<Participation> getParticipations() {
        return participations;
    }

    public void setParticipations(List<Participation> participations) {
        this.participations = participations;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<Lobby> getCreatedLobies() {
        return createdLobies;
    }

    public void setCreatedLobies(List<Lobby> createdLobies) {
        this.createdLobies = createdLobies;
    }

    public List<Invitation> getInvitations() {
        return invitations;
    }

    public void setInvitations(List<Invitation> invitations) {
        this.invitations = invitations;
    }

    public List<Lobby> getJoinedLobbies() {
        return joinedLobbies;
    }

    public void setJoinedLobbies(List<Lobby> joinedLobbies) {
        this.joinedLobbies = joinedLobbies;
    }

    public Map<Integer, Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(Map<Integer, Notification> notifications) {
        this.notifications = notifications;
    }

    public String getDeviceFireBaseToken() {
        return deviceFireBaseToken;
    }

    public void setDeviceFireBaseToken(String deviceFireBaseToken) {
        this.deviceFireBaseToken = deviceFireBaseToken;
    }
}
