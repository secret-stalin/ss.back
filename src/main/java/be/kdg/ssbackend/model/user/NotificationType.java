package be.kdg.ssbackend.model.user;

public enum NotificationType {
    FRIEND_REQUEST, INVITE, ENDPHASE, YOUR_TURN
}
