package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.chat.PrivateChat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrivateChatRepository extends JpaRepository<PrivateChat, Integer> {
    PrivateChat getPrivateChatByChatId(int chatId);
}
