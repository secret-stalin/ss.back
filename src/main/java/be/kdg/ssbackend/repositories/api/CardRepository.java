package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.game.Card;
import be.kdg.ssbackend.model.game.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardRepository extends JpaRepository<Card, Integer> {
    Card findByRoleType(RoleType rt);
    List<Card> findAllByRoleTypeIn(List<RoleType> rts);

}
