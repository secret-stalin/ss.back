package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.user.NotificationSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationSettingsRepository extends JpaRepository<NotificationSettings, Integer> {
    NotificationSettings findByUserUserId(int userId);

}
