package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.game.Invitation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvitationRepository extends JpaRepository<Invitation,Integer> {
    Invitation findByInvitationId(int invitationId);
}
