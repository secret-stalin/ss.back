package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.chat.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
    List<Message> findAllByChat_ChatId(int chatId);
}
