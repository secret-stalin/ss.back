package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.game.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends JpaRepository<Game,Integer> {
    Game findGameByGameId(int id);
}
