package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.game.Game;
import be.kdg.ssbackend.model.game.Phase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhaseRepository extends JpaRepository<Phase, Integer> {
    //aka findCurrentPhaseByGame
    Phase findTopByGameOrderByPhaseNumberDesc(Game game);
    List<Phase> findAllByGame(Game game);
}
