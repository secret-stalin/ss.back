package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.game.Action;
import be.kdg.ssbackend.model.game.Phase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Erik Van Hoolst
 * 2019-02-26
 */
@Repository
public interface ActionRepository extends JpaRepository<Action, Integer> {
    List<Action> findAllByPhase(Phase phase);
}
