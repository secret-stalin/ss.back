package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.game.Participation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParticipationRepository extends JpaRepository<Participation, Integer> {
    Participation findByParticipationId(int partId);
}
