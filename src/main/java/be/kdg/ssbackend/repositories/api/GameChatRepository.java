package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.chat.GameChat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameChatRepository extends JpaRepository<GameChat, Integer> {
    List<GameChat> getGameChatsByGameGameId(int gameId);

    GameChat getGameChatByChatId(int chatId);
}
