package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.user.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Integer> {
    List<Notification> findNotificationsByUser_UserId(int userId);
    Notification findByNotificationId(int notificationId);
}
