package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.game.Lobby;
import be.kdg.ssbackend.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LobbyRepository extends JpaRepository<Lobby, Integer> {
    List<Lobby> getAllByIsPrivate(boolean isPrivate);
    List<Lobby> getAllByJoinedUsersContains(User user);
}
