package be.kdg.ssbackend.repositories.api;

import be.kdg.ssbackend.model.chat.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Integer> {
    Chat getChatByChatId(int id);
}
